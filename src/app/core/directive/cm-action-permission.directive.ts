import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

/**
 * @TungBT -Bui Thang Tung
 */
@Directive({
  selector: '[cmActionPermission]'
})
export class CmActionPermissionDirective {
  private action: string;
  private configPath: string;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.configPath = route.snapshot.data['configPath'];
  }

  @Input()
  set cmActionPermission(action: string) {
    this.action = action;
    this.updateView();
    // Tu dong thay doi view khi quyen trong accountService.userIdentity thay doi
  }

  private updateView(): void {
    // this.viewContainerRef.clear();
    // if (hasAnyAuthority) {
    //   this.viewContainerRef.createEmbeddedView(this.templateRef);
    // }
  }
}
