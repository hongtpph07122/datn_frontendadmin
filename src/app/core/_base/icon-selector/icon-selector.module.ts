import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {IconSelectorDialogComponent} from './icon-selector.dialog.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    IconSelectorDialogComponent
  ],
  entryComponents: [
    IconSelectorDialogComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    FormsModule
  ],
  exports: [
    IconSelectorDialogComponent
  ]
})
export class IconSelectorModule {
}
