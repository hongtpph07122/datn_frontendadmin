import { Options } from './models';

export const defaultOptions: Options<any> = {
  verticalSeparator: true,
	highlightRowOnHover: true,
	elevation: 0,
  customFieldConfig: false,
  customCellBuilder: false,
  showLevel: Infinity,
};
