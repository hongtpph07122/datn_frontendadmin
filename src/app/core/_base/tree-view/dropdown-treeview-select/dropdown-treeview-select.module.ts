import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {DropdownTreeviewSelectComponent} from './dropdown-treeview-select.component';
import {TreeviewModule} from '../treeview.module';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    TreeviewModule.forRoot(),
    MatIconModule
  ],
  declarations: [
    DropdownTreeviewSelectComponent,
  ],
  exports: [
    DropdownTreeviewSelectComponent
  ]
})
export class DropdownTreeviewSelectModule {
}
