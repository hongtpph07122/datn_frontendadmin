import {Injectable} from '@angular/core';
import {DefaultTreeviewI18n} from '../models/treeview-i18n';
import {TreeviewItem, TreeviewSelection} from '../models/treeview-item';

@Injectable()
export class DropdownTreeviewSelectI18n extends DefaultTreeviewI18n {
  private internalSelectedItem: TreeviewItem;

  set selectedItem(value: TreeviewItem) {
    this.internalSelectedItem = value;
  }

  get selectedItem(): TreeviewItem {
    return this.internalSelectedItem;
  }

  getText(selection: TreeviewSelection): string {
    const txt = localStorage.getItem('language') == 'vi' ? 'Chọn' : 'Please select';
    return this.internalSelectedItem ? this.internalSelectedItem.text : txt;
  }
}
