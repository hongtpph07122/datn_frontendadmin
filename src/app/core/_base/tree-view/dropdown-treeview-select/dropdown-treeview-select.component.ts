import {Component, EventEmitter, Input, OnChanges, Output, ViewChild} from '@angular/core';
import {isNil} from 'lodash';
import {DropdownTreeviewSelectI18n} from './dropdown-treeview-select-i18n';
import {TreeviewI18n} from '../models/treeview-i18n';
import {DropdownTreeviewComponent} from '../components/dropdown-treeview/dropdown-treeview.component';
import {TreeviewConfig} from '../models/treeview-config';
import {TreeviewItem} from '../models/treeview-item';
import {TreeviewHelper} from '../helpers/treeview-helper';
import {DropdownDirective} from '../directives/dropdown.directive';

@Component({
  selector: 'cm-treeview-signle-select',
  templateUrl: './dropdown-treeview-select.component.html',
  styleUrls: [
    './dropdown-treeview-select.component.scss'
  ],
  providers: [
    {provide: TreeviewI18n, useClass: DropdownTreeviewSelectI18n}
  ]
})
export class DropdownTreeviewSelectComponent implements OnChanges {
  @Input() config: TreeviewConfig;
  @Input() items: TreeviewItem[];
  @Input() value: any;
  @Input() enableSelectItemHasChild: boolean = false;
  @Output() valueChange = new EventEmitter<any>();
  @ViewChild(DropdownTreeviewComponent, {static: false}) dropdownTreeviewComponent: DropdownTreeviewComponent;
  filterText: string;
  private dropdownTreeviewSelectI18n: DropdownTreeviewSelectI18n;

  constructor(
    public i18n: TreeviewI18n
  ) {
    this.config = TreeviewConfig.create({
      hasAllCheckBox: false,
      hasCollapseExpand: false,
      hasFilter: true,
      maxHeight: 500
    });
    this.dropdownTreeviewSelectI18n = i18n as DropdownTreeviewSelectI18n;
  }

  ngOnChanges(): void {
    this.updateSelectedItem();
  }

  select(item: TreeviewItem): void {
    if (this.enableSelectItemHasChild) {
      this.selectItem(item);
    } else {
      if (!item.children) {
        this.selectItem(item);
      }
    }
  }

  private updateSelectedItem(): void {
    if (!isNil(this.items)) {
      const selectedItem = TreeviewHelper.findItemInList(this.items, this.value);
      this.selectItem(selectedItem);
    }
  }

  private selectItem(item: TreeviewItem): void {
    if (this.dropdownTreeviewSelectI18n.selectedItem !== item) {
      this.dropdownTreeviewSelectI18n.selectedItem = item;
      if (this.dropdownTreeviewComponent) {
        this.dropdownTreeviewComponent.onSelectedChange([item]);
      }

      if (item) {
        if (this.value !== item.value) {
          this.value = item.value;
          this.valueChange.emit(item.value);
        }
      }
    }

    if (this.dropdownTreeviewComponent) {
      this.dropdownTreeviewComponent.dropdownDirective.close();
    }
  }

  clearSelect() {
    this.dropdownTreeviewSelectI18n.selectedItem = null;
    this.value = null;
    this.valueChange.emit(null);

    // Close popup
    if (this.dropdownTreeviewComponent) {
      this.dropdownTreeviewComponent.dropdownDirective.close();
    }
  }
}
