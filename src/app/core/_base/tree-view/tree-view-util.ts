import {TreeviewItem} from './models/treeview-item';

export class TreeViewUtil {
  static convertToTreeItem(res: any[]): TreeviewItem[] {
    const rs: TreeviewItem[] = [];
    if (res !== null && res !== undefined) {
      res.forEach(it => {
        rs.push(new TreeviewItem(it));
      });
    }

    return rs;
  }
}
