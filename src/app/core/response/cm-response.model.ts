export class CmResponseModel<T> {
	code: string;
	message: string;
	data: T;
}
