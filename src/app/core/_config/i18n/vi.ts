// USA
export const locale = {
  lang: 'vi',
  data: {
    TRANSLATOR: {
      SELECT: 'Lựa chọn ngôn ngữ',
    },
    MENU: {
      NEW: 'mới',
      ACTIONS: 'Hành động',
      CREATE_POST: 'Tạo bài viết mới',
      PAGES: 'Trang',
      FEATURES: 'Chức năng',
      APPS: 'Ứng dụng',
      DASHBOARD: 'Trang chủ',
    },
    AUTH: {
      GENERAL: {
        OR: 'Hoặc',
        SUBMIT_BUTTON: 'Gửi',
        NO_ACCOUNT: 'Chưa có tài khoản?',
        SIGNUP_BUTTON: 'Đăng ký',
        FORGOT_BUTTON: 'Quên mật khẩu',
        BACK_BUTTON: 'Quay lại',
        PRIVACY: 'Chính sách',
        LEGAL: 'Pháp lý',
        CONTACT: 'Liên hệ',
      },
      LOGIN: {
        TITLE: 'Đăng nhập tài khoản',
        BUTTON: 'Đăng nhập',
        HEADER: 'Hệ thống kiểm soát tập trung',
        HEADER_DESC: 'Đăng nhập bằng tài khoản email nội bộ SeABank để sử dụng các tính năng kiểm soát tuyệt vời này.'
      },
      FORGOT: {
        TITLE: 'Quên mật khẩu?',
        DESC: 'Enter your email to reset your password',
        SUCCESS: 'Your account has been successfully reset.'
      },
      REGISTER: {
        TITLE: 'Đăng ký',
        DESC: 'Enter your details to create your account',
        SUCCESS: 'Your account has been successfuly registered.'
      },
      INPUT: {
        EMAIL: 'Email',
        FULLNAME: 'Tên đầy đủ',
        PASSWORD: 'Mật khẩu',
        CONFIRM_PASSWORD: 'Xác nhận mật khẩu',
        USERNAME: 'Tên đăng nhập'
      },
      VALIDATION: {
        INVALID: '{{name}} is not valid',
        REQUIRED: '{{name}} is required',
        MIN_LENGTH: '{{name}} minimum length is {{min}}',
        AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
        NOT_FOUND: 'The requested {{name}} is not found',
        INVALID_LOGIN: 'The login detail is incorrect',
        REQUIRED_FIELD: 'Required field',
        MIN_LENGTH_FIELD: 'Minimum field length:',
        MAX_LENGTH_FIELD: 'Maximum field length:',
        INVALID_FIELD: 'Field is not valid',
      }
    },
    ECOMMERCE: {
      COMMON: {
        SELECTED_RECORDS_COUNT: 'Selected records count: ',
        ALL: 'All',
        SUSPENDED: 'Suspended',
        ACTIVE: 'Active',
        FILTER: 'Filter',
        BY_STATUS: 'by Status',
        BY_TYPE: 'by Type',
        BUSINESS: 'Business',
        INDIVIDUAL: 'Individual',
        SEARCH: 'Search',
        IN_ALL_FIELDS: 'in all fields'
      },
      ECOMMERCE: 'eCommerce',
      CUSTOMERS: {
        CUSTOMERS: 'Customers',
        CUSTOMERS_LIST: 'Customers list',
        NEW_CUSTOMER: 'New Customer',
        DELETE_CUSTOMER_SIMPLE: {
          TITLE: 'Customer Delete',
          DESCRIPTION: 'Are you sure to permanently delete this customer?',
          WAIT_DESCRIPTION: 'Customer is deleting...',
          MESSAGE: 'Customer has been deleted'
        },
        DELETE_CUSTOMER_MULTY: {
          TITLE: 'Customers Delete',
          DESCRIPTION: 'Are you sure to permanently delete selected customers?',
          WAIT_DESCRIPTION: 'Customers are deleting...',
          MESSAGE: 'Selected customers have been deleted'
        },
        UPDATE_STATUS: {
          TITLE: 'Status has been updated for selected customers',
          MESSAGE: 'Selected customers status have successfully been updated'
        },
        EDIT: {
          UPDATE_MESSAGE: 'Customer has been updated',
          ADD_MESSAGE: 'Customer has been created'
        }
      }
    }
  }
};
