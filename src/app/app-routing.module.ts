// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './views/theme/base/base.component';
// Auth

const routes: Routes = [
  // {
  //   path: 'auth',
  //   loadChildren: () =>
  //     import('./views/pages/auth/auth.module').then((m) => m.AuthModule),
  // },
  {
    path: '',
    loadChildren: () =>
      import('./views/DuAn/UserDuan/User.module').then((m) => m.UserModule),
  },
  {
    path: 'error',
    loadChildren: () =>
      import('./views/pages/error/error.module').then((m) => m.ErrorModule),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./views/DuAn/auth-login/auth-login.module').then((m) => m.AuthLoginModule),
  },
  {
    path: 'admin',
    component: BaseComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./views/pages/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'user-manager',
        loadChildren: () =>
          import('./views/DuAn/user-manager/user-manager.module').then(
            (m) => m.UserManagerModule
          ),
      },
      {
        path: 'post-metol-manager',
        loadChildren: () =>
          import('./views/DuAn/post-metol-manager/post-metol-manager.module').then(
            (m) => m.PostMetolManagerModule
          ),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('./views/DuAn/my-profile/my-profile.module').then(
            (m) => m.MyProfileModule
          ),
      },
      {
        path: 'userClient',
        loadChildren: () =>
        import('./views/DuAn/UserClientManagement/user-client-manager.module').then(
          (m) => m.UserManagerModule
          ),
      },
























    
      {
        path: 'category',
        loadChildren: () =>
          import('./views/pages/category/category.module').then(
            (m) => m.CategoryModule
          ),
      },
      {
        path: 'disbursement',
        loadChildren: () => import('./views/pages/disbursement/disbursement.module').then(m => m.DisbursementModule),
      },
    
      {
        path: 'mail',
        loadChildren: () =>
          import('./views/pages/apps/mail/mail.module').then(
            (m) => m.MailModule
          ),
      },
      {
        path: 'ecommerce',
        loadChildren: () =>
          import('./views/pages/apps/e-commerce/e-commerce.module').then(
            (m) => m.ECommerceModule
          ),
      },
      {
        path: 'control-report',
        loadChildren: () =>
          import('./views/pages/control-report/control-report.module').then(
            (m) => m.ControlReportModule
          ),
      },
      {
        path: 'monitoring-after-disbursement/directory-administration',
        loadChildren: () => import('./views/pages/category/directory-administration/directory-administration.module')
          .then(m => m.DirectoryAdministrationModule),
      },
      {
        path: 'mail',
        loadChildren: () => import('./views/pages/apps/mail/mail.module').then(m => m.MailModule),
      },
      {
        path: 'ngbootstrap',
        loadChildren: () =>
          import('./views/pages/ngbootstrap/ngbootstrap.module').then(
            (m) => m.NgbootstrapModule
          ),
      },
      {
        path: 'material',
        loadChildren: () =>
          import('./views/pages/material/material.module').then(
            (m) => m.MaterialModule
          ),
      },
  
      {
        path: 'wizard',
        loadChildren: () =>
          import('./views/pages/wizard/wizard.module').then(
            (m) => m.WizardModule
          ),
      },
      {
        path: 'builder',
        loadChildren: () =>
          import('./views/theme/content/builder/builder.module').then(
            (m) => m.BuilderModule
          ),
      },
      {
        path: 'check-document',
        loadChildren: () =>
          import('./views/pages/check-document/check-document.module').then(
            (m) => m.CheckDocumentModule
          ),
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'check-document',
        loadChildren: () => import('./views/pages/check-document/check-document.module').then(m => m.CheckDocumentModule),
      },
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: '**', redirectTo: 'dashboard', pathMatch: 'full'},
    ],
  },
  { path: '**', redirectTo: 'error/403', pathMatch: 'full' },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
