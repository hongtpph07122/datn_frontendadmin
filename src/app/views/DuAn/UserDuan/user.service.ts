import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CmResponseModel} from '../../../core/response/cm-response.model';
import {createRequestOption} from '../Util/request-util';

@Injectable({providedIn: 'root'})
export class UserService {
  private BASE_API_URL = `${environment.GATEWAY_URL}api/motel`;

  constructor(private http: HttpClient) {
  }

  public searchClient(data :any){
    return this.http.post(`${this.BASE_API_URL}/searchClient`,data)
  }

 

}
