import { Component, OnInit } from '@angular/core';
import { Province } from '../../dataSources/dataProvince'
import { District } from '../../dataSources/dataDistrict'
import { Ward } from '../../dataSources/dataWard'
import {UserService} from '../user.service'
@Component({
  selector: 'kt-show-index',
  templateUrl: './show-index.component.html',
  styleUrls: ['./show-index.component.scss']
})
export class ShowIndexComponent implements OnInit {

  keywordProvince = 'name';
  keywordDistrict = 'name';
  keywordWard = 'name';
  dataProvince = Province;
  dataDistrict = District;
  dataWard = Ward;


  allExpandState: boolean = true;
  filteredCourses: any[];
  selectedCategories: any[] = [];

  allExpandState2: boolean = true;
  filteredCourses2: any[];
  selectedCategories2: any[] = [];

  dataType = [
    { id: 1, name: 'Phòng trọ' },
    { id: 2, name: 'Chung cư' },
    { id: 3, name: 'Nhà nguyên căn' }

  ];

  courses: any = [
    {
      "name": "Hiển thị  các nhà trọ",
      "categoryId": "e39f9302-77b3-4c52-a858-adb67651ce86",
      "image": "Ảnh nhà trọ",
    },
    {
      "name": "Hiển thị các ngôi nhà nguyên căn",
      "categoryId": "40665c50-ff74-4e81-b968-e127bdf1fe28",
    },
    {
      "name": "Hiển thị chung cư",
      "categoryId": "38688c41-8fda-41d7-b0f5-c37dce3f5374",
    }
  ];
  categories: any = [
    {
      "id": "e39f9302-77b3-4c52-a858-adb67651ce86",
      "name": "Nhà trọ "
    },
    {
      "id": "38688c41-8fda-41d7-b0f5-c37dce3f5374",
      "name": "Nhà nguyên căn"
    },
    {
      "id": "ed780d15-428b-4bcd-8a91-bacae8b0b72e",
      "name": "Chung cư"
    }
  ];


  courses2: any = [
    {
      "name": "Hiển thị  các nhà trọ",
      "categoryId": "1",
      "image":"Ảnh nhà trọ",
    },
    {
      "name": "Hiển thị các ngôi nhà nguyên căn",
      "categoryId": "2",
    },
    {
      "name": "Hiển thị chung cư",
      "categoryId": "3",
    },
    {
      "name": "Hiển thị chung cư mini",
      "categoryId": "4",
    }
  ];
  categories2: any = [
    {
      "id": "1",
      "name": "Từ 10 - 20 m2"
    },
    {
      "id": "2",
      "name": "Từ 20 - 30 m2"
    },
    {
      "id": "3",
      "name": "Từ 30 40 m2"
    },
    {
      "id": "4",
      "name": "Từ 40 - 50 m2"
    }
  ];
  paramSearch = {
    province: null,
    district: null,
    wards: null,
    typeMotel : [],
    priceMin:null,
    priceMax:null
  };

  dataMotel:any=[]
  constructor(
    private userService:UserService
  ) { }

  ngOnInit(): void {
    this.paramSearch=JSON.parse(localStorage.getItem('dataSearchClient'));
    this.filteredCourses = this.courses;
    this.filteredCourses2 = this.courses2;
    this.loadData();
   // this.dataMotel=JSON.parse(localStorage.getItem('xxx'))
  }
  loadData(){
    this.userService.searchClient(this.paramSearch).subscribe(res=>{
      localStorage.setItem('xxx',JSON.stringify(res));
      this.dataMotel=res;
      console.log( this.dataMotel);
      
      
    })
  }


  selectProvice(item) {
    this.paramSearch.province = item.name;
    this.dataDistrict = District.filter((items) => {
      return items.provinceId == item.id;
    })

  }

  selectDistrict(item) {
    this.paramSearch.district = item.name;
    this.dataWard = Ward.filter((items) => {
      return items.districtId == item.id && items.provinceId == item.provinceId
    })
  }

  selectWard(item) {
    this.paramSearch.wards = item.name;

  }

  onChangeSearchProvice(search: string) {
    if (search === null || search === '' || search === undefined) {
      this.paramSearch.province = null;
    }

  }

  onChangeSearchDistrict(search: string) {
    if (search === null || search === '' || search === undefined) {
      this.paramSearch.district = null;
    }

  }
  onChangeSearchWard(search: string) {
    if (search === null || search === '' || search === undefined) {
      this.paramSearch.wards = null;

    }


  }
  onSelect2(selectedCategory: any) {
    this.selectCategory2(selectedCategory);
    this.filteredCourses2 = this.courses2.filter((course: any) => {
      return this.selectedCategories2.findIndex((cat: any) => {
        return course.categoryId === cat.id;
      }) !== -1;
    });
  }

  selectCategory2(selectedCategory2: any) {
    const index: number = this.filteredCourses2.findIndex((cat: any) => {
      return cat.id === selectedCategory2.id
    });

    if (index === -1) {
      this.selectedCategories2.push(selectedCategory2);
    } else {
      this.selectedCategories2.splice(index, 1);
    }
  }

}
