import { Router } from '@angular/router';
import { Component, OnInit,ViewChild } from '@angular/core';
import { Province } from '../../dataSources/dataProvince'
import { District } from '../../dataSources/dataDistrict'
import { Ward } from '../../dataSources/dataWard'
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
@Component({
  selector: 'kt-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  @ViewChild('select') select: MatSelect;
  keywordProvince = 'name';
  keywordDistrict = 'name';
  keywordWard = 'name';
  dataProvince = Province;
  dataDistrict = District;
  dataWard = Ward;


  allExpandState: boolean = true;
  filteredCourses: any[];
  selectedCategories: any[] = [];

  dataType = [
    { id: 1, name: 'Phòng trọ' },
    { id: 2, name: 'Chung cư' },
    { id: 3, name: 'Nhà nguyên căn' }

  ];

  allSelected=false;




  paramSearch = {
    province: null,
    district: null,
    wards: null,
    typeMotel : [],
    priceMin:null,
    priceMax:null
  };


  constructor(private router: Router,) { }

  ngOnInit(): void {
  }
  onSliderChange(e){
    console.log(e);
    this.paramSearch.priceMin= e[0]
    this.paramSearch.priceMax= e[1]

    
  }
   
  toggleAllSelection() {
    this.paramSearch.typeMotel=[]

    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => {
        item.select()
        this.paramSearch.typeMotel.push(item.value)
      });
    } else {
      this.select.options.forEach((item: MatOption) => item.deselect());
    }
  }
  optionClick() {
    this.paramSearch.typeMotel=[]
    let newStatus = true;
    this.select.options.forEach((item: MatOption) => {
 
      if (item.selected) {
        this.paramSearch.typeMotel.push(item.value)
        
      }
      if (!item.selected) {
        newStatus = false;
      }
    });
    this.allSelected = newStatus;
  }

  selectProvice(item) {
    this.paramSearch.province = item.name;
    this.dataDistrict = District.filter((items) => {
      return items.provinceId == item.id;
    })

  }

  selectDistrict(item) {
    this.paramSearch.district = item.name;
    this.dataWard = Ward.filter((items) => {
      return items.districtId == item.id && items.provinceId == item.provinceId
    })
  }

  selectWard(item) {
    this.paramSearch.wards = item.name;

  }

  onChangeSearchProvice(search: string) {
    if (search === null || search === '' || search === undefined) {
      this.paramSearch.province = null;
    }

  }

  onChangeSearchDistrict(search: string) {
    if (search === null || search === '' || search === undefined) {
      this.paramSearch.district = null;
    }

  }
  onChangeSearchWard(search: string) {
    if (search === null || search === '' || search === undefined) {
      this.paramSearch.wards = null;

    }
  }
  searchNow() {
    this.goToResultSearch();
  }

  goToResultSearch() {
 
localStorage.setItem('dataSearchClient',JSON.stringify(this.paramSearch))
    this.router.navigate(['/show-index']);
  }

  showOptions(e){
    console.log(e);
    
  }

}