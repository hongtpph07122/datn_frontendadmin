import { RoomsDetailsComponent } from './rooms-details/rooms-details.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LayoutUtilsService, TypesUtilsService} from '../../../core/_base/crud';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {PortletModule} from '../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import {CoreModule} from '../../../core/core.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DropdownTreeviewSelectModule} from '../../../core/_base/tree-view/dropdown-treeview-select';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { HomeUserComponent } from './home-user.component';
import { IndexComponent } from './index/index.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ShowIndexComponent } from './show-index/show-index.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { NpnSliderModule } from 'npn-slider';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatSelectModule } from '@angular/material/select';

const routes: Routes = [
  {
    path:'',
    component:HomeUserComponent,
    
    children:[
      {
        path:'',
        redirectTo:'index',
        pathMatch:'full'
      },
      {
        path:'index',
        component:IndexComponent,
        data:{
          configPath:'/index'
        }
      },
      {
        path:'show-index',
        component:ShowIndexComponent,
        data:{
          configPath:'/show-index'
        }
      }
      ,
      {
        path:'rooms-details',
        component:RoomsDetailsComponent,
        data:{
          configPath:'/rooms-details'
        }
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    PortletModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    NgxDatatableModule,
    MatSlideToggleModule,
    AutocompleteLibModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatCardModule,
    NpnSliderModule,
    NgSelectModule,
    MatSelectModule
    

    
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService
  ],
  entryComponents: [


  ],
  declarations: [

  HomeUserComponent,
  IndexComponent,
  ShowIndexComponent,
  RoomsDetailsComponent

],
})
export class UserModule {
}
