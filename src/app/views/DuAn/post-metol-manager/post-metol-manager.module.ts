import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LayoutUtilsService, TypesUtilsService} from '../../../core/_base/crud';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {PortletModule} from '../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import {CoreModule} from '../../../core/core.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DropdownTreeviewSelectModule} from '../../../core/_base/tree-view/dropdown-treeview-select';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { PostMetolManagerComponent } from './post-metol-manager.component';
import { IndexPostMetolComponent } from './index-post-metol/index-post-metol.component';
import { ShowDetailPostMetolComponent } from './show-detail-post-metol/show-detail-post-metol.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ShowMetolComponent } from './show-metol/show-metol.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { InlineSVGModule } from 'ng-inline-svg';

const routes: Routes = [
  {
    path:'',
    component:PostMetolManagerComponent,
    children:[
      {
        path:'',
        redirectTo:'index',
        pathMatch:'full'
      },
      {
        path:'index',
        component:IndexPostMetolComponent,
        data:{
          configPath:'admin/post-metol-manager/index'
        }
      },{
        path:'showDetail',
        component:ShowMetolComponent,
        data:{
          configPath:'admin/post-metol-manager/showDetail'
        }
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    PortletModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    NgxDatatableModule,
    AutocompleteLibModule,
    	MatSelectModule,
		MatInputModule,
    InlineSVGModule,
    NgbModule

    
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '1300px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService
  ],
  entryComponents: [
    ShowDetailPostMetolComponent
    
  ],
  declarations: [

  PostMetolManagerComponent,
IndexPostMetolComponent,
ShowDetailPostMetolComponent,
ShowMetolComponent],
})
export class PostMetolManagerModule {
}
