import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CmResponseModel} from '../../../core/response/cm-response.model';
import {createRequestOption} from '../Util/request-util';

@Injectable({providedIn: 'root'})
export class PostMetolManagerService {
  private BASE_API_URL = `${environment.GATEWAY_URL}api/`;

  constructor(private http: HttpClient) {
  }
  public doSearch(data: any, req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.post(`${this.BASE_API_URL}motel/doSearch`, data, {
      params: options,
      observe: 'response'
    })
  }
  public doSearchReport(motelId:any): Observable<any> {
    return this.http.post(`${this.BASE_API_URL}report/doSearch`,{
      motelId: motelId,
   
    })
  }

}
