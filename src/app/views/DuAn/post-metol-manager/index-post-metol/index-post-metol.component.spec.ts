import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPostMetolComponent } from './index-post-metol.component';

describe('IndexPostMetolComponent', () => {
  let component: IndexPostMetolComponent;
  let fixture: ComponentFixture<IndexPostMetolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexPostMetolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPostMetolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
