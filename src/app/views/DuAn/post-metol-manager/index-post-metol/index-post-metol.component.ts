import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {HttpHeaders} from '@angular/common/http';
import {Province} from '../../dataSources/dataProvince'
import {District} from '../../dataSources/dataDistrict'
import {Ward} from '../../dataSources/dataWard'
import {PostMetolManagerService} from '../post-metol-manager.service'
import {Page} from '../../Util/page.model';
import { ShowDetailPostMetolComponent } from '../show-detail-post-metol/show-detail-post-metol.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ShareService } from '../../Util/share.service';


@Component({
  selector: 'kt-index-post-metol',
  templateUrl: './index-post-metol.component.html',
  styleUrls: ['./index-post-metol.component.scss']
})
export class IndexPostMetolComponent implements OnInit {
  formSearch: FormGroup;
  page = new Page();
  keywordProvince = 'name';
  keywordDistrict = 'name';
  keywordWard = 'name';
  dataProvince=Province;
dataDistrict=District;
dataWard=Ward;



paramSearch = {
  username: null,
  motelType: null,
  fromDate: null,
  toDate: null,
  status: null,
  province: null,
  district: null,
  wards: null,
};


   public rows :Object[];
 
  displayedColumns: string[] = ['index', 'userName', 'motelType', 'createdDateMotel', 'statusMotel','addressMotel','countView','countReport','uptopStatus','action_btn'];
  dataSource = new MatTableDataSource<any>([]);

  isLoading: boolean = false;







  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private postMetolManagerService:PostMetolManagerService,
    private router:Router,
    private shareService:ShareService
    
    ) {
      this.page.pageNumber = 0;
      this.page.size = 1;
    }

 
  ngOnInit(): void {
    this.search(0);

  }

  onSearch(){
    this.search(0);
  }

  showDetailMetol(row){
    localStorage.setItem("itemShowDetail", JSON.stringify(row));

    this.router.navigate(['/admin/post-metol-manager/showDetail']);
    
  }


  onDelete(row){
    console.log(row);

  }

  showDetailReport(row){
  
    
    this.postMetolManagerService.doSearchReport(row.motelId).subscribe(res=>{
      console.log(res);
      
      const dialogRef = this.dialog.open(ShowDetailPostMetolComponent, {
        data: {
          action: 'CREATE',
          titel: '',
          result: res,
         
        }
      });
      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.onSearch();
        }
      });
      
    })

  }
  search(page: number) {
    console.log(this.paramSearch);
    this.isLoading = true;
    this.postMetolManagerService.doSearch(this.paramSearch, {
        page:      this.page.pageNumber,
        size:   this.page.size
      }
    ).subscribe(
      (res) => {
        console.log(res);
        
        let data = res.body
        this.page.totalElements =data.totalElements;
        this.dataSource = new MatTableDataSource<any>(data.content);
        this.ref.detectChanges();
        console.log( this.page.totalElements );
        

      },
      (error) => {
        this.isLoading = false;
      },
      () => this.isLoading = false,
    );
  }

 
  getServerData(data){
  this.page.pageNumber=data.pageIndex;
  this.page.size=data.pageSize    
  this.onSearch();
  }


  selectProvice(item) {
    this.paramSearch.province=item.name;
    this.dataDistrict = District.filter((items)=>{
      return items.provinceId== item.id;
    }) 

  }

  selectDistrict(item){
    this.paramSearch.district= item.name;
    this.dataWard=Ward.filter((items)=>{
          return items.districtId == item.id && items.provinceId== item.provinceId
    })
  }
  
  selectWard(item){
    this.paramSearch.wards= item.name;
   
  }

  onChangeSearchProvice(search: string) {
   if(search===null || search==='' || search===undefined){
    this.paramSearch.province=null;
   }
  
  }

  onChangeSearchDistrict(search: string) {
    if(search===null || search==='' || search===undefined){
      this.paramSearch.district=null;
   }
  
  }
  onChangeSearchWard(search: string) {
    if(search===null || search==='' || search===undefined){
      this.paramSearch.wards=null;

}  
  }
  onFocused(e) {
    console.log('onFocused',e);
  }






  
  











}
