import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {Page} from '../../Util/page.model';

@Component({
  selector: 'kt-show-detail-post-metol',
  templateUrl: './show-detail-post-metol.component.html',
  styleUrls: ['./show-detail-post-metol.component.scss']
})
export class ShowDetailPostMetolComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = ['stt', 'userName', 'createDate', 'reason'];

  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  page = new Page();

  constructor(
    public dialogRef: MatDialogRef<ShowDetailPostMetolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {

    this.dataSource=  new MatTableDataSource<any>(this.data.result)

  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  getServerData(data){
    console.log(this.paginator);
    
    this.page.pageNumber=data.pageIndex;
    this.page.size=data.pageSize    
    }

}
