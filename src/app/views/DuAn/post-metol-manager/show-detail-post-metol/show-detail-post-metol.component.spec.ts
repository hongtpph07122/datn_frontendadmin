import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDetailPostMetolComponent } from './show-detail-post-metol.component';

describe('ShowDetailPostMetolComponent', () => {
  let component: ShowDetailPostMetolComponent;
  let fixture: ComponentFixture<ShowDetailPostMetolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDetailPostMetolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDetailPostMetolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
