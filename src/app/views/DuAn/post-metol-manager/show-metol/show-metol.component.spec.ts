import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMetolComponent } from './show-metol.component';

describe('ShowMetolComponent', () => {
  let component: ShowMetolComponent;
  let fixture: ComponentFixture<ShowMetolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowMetolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMetolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
