import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ShareService } from '../../Util/share.service';

@Component({
  selector: 'kt-show-metol',
  templateUrl: './show-metol.component.html',
  styleUrls: ['./show-metol.component.scss']
})
export class ShowMetolComponent implements OnInit,AfterViewInit {

item :any ={
	userId :'',
	userName: '',
	email:'',
	fullName:'',
	phone:'',
	addRessUser:'',
	createdDateUser:'',
	motelId:'',
	title:'',
	description:'',
	motelType :'',
	longitude: '',
	latitude:'',
	addressMotel:'',
	priceMin:'',
	priceMax:'',
	acreageMin:'',
	acreageMax:'',
	deposits:'',
	uptopStatus:'',
	lastUptop:'',
	contactPhone:'',
	liveWithHost:'',
	privateToilet :'',
	numberBedroom: '',
	numberToilet:'',
	floor:'',
	numberFloor:'',
	direction:'',
	apartmentSituation:'',
	length:'',
	width:'',
	countView:'',
	createdDateMotel:'',
	countReport:'',
	photos:'',
	statusMotel:'',

}
images :any={};

  constructor(
    private shareService:ShareService
  ) { }

  ngOnInit(): void {
this.loadItem();
this.loadImage(this.item.photos);
	
  }
  @ViewChild('wizard', {static: true}) el: ElementRef;

loadImage(data){
let index=0;
data.split(',').forEach((pair) => {
  if(pair !== '') {
    this.images[index++] = pair;
  }
});

console.dir(this.images);
}

	model: any = {
		fname: 'John',
		lname: 'Wick',
		phone: '+61412345678',
		email: 'john.wick@reeves.com',
		address1: 'Address Line 1',
		address2: 'Address Line 2',
		postcode: '3000',
		city: 'Melbourne',
		state: 'VIC',
		country: 'AU',
		delivery: 'overnight',
		packaging: 'regular',
		preferreddelivery: 'morning',
		locaddress1: 'Address Line 1',
		locaddress2: 'Address Line 2',
		locpostcode: '3072',
		loccity: 'Preston',
		locstate: 'VIC',
		loccountry: 'AU',
		ccname: 'John Wick',
		ccnumber: '4444 3333 2222 1111',
		ccmonth: '01',
		ccyear: '21',
		cccvv: '123',
	};
	submitted = false;



	ngAfterViewInit(): void {
		// Initialize form wizard
		const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1
		});

		// Validation before going to next page
		wizard.on('beforeNext', (wizardObj) => {
			// https://angular.io/guide/forms
			// https://angular.io/guide/form-validation

			// validate the form and use below function to stop the wizard's step
			// wizardObj.stop();
		});

		// Change event
		wizard.on('change', () => {
			setTimeout(() => {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	onSubmit() {
		this.submitted = true;
	}


	loadItem(){

		this.item= JSON.parse(localStorage.getItem('itemShowDetail'));
console.log(this.item);

	}

}
