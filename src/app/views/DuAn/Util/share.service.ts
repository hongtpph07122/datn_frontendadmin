import {Injectable} from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ShareService {
    private countValue = new Subject();
    constructor() {
    }
 
    public get ValueFromChild() {
        return this.countValue;
    }
   
    public notifyCountValue(data) {
        this.countValue.next(data);
    }
}