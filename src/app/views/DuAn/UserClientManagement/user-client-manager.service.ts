import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CmResponseModel} from '../../../core/response/cm-response.model';
import {createRequestOption} from '../Util/request-util';
import { Image } from '../Util/Image';

@Injectable({providedIn: 'root'})
export class UserClientManagerService {
  private readonly IMGUR_UPLOAD_URL = 'https://api.imgur.com/3/image';
  private readonly clientId = '2fcdcd9ecca05bf';

  constructor(
    private http: HttpClient
  ) { }
  upload(b64Image: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Client-ID ${this.clientId}`
      }),
    };
    const formData = new FormData();
    formData.append('image', b64Image);
    return this.http.post<Image>(`${this.IMGUR_UPLOAD_URL}`, formData, httpOptions);
  }
  private BASE_API_URL = `${environment.GATEWAY_URL}api/`;


  public doSearch(data: any, req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.get(`${this.BASE_API_URL}motel/doSearchUser/${data}`, {
      params: options,
      observe: 'response'
    })
  }
  public doSearchReport(motelId:any): Observable<any> {
    return this.http.post(`${this.BASE_API_URL}report/doSearch`,{
      motelId: motelId,
   
    })
  }


  public save(motel:any):Observable<any> {
    return this.http.post(`${this.BASE_API_URL}motel/save`,motel, {responseType: 'text'})
  }

}
