import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { District } from '../../dataSources/dataDistrict';
import { Province } from '../../dataSources/dataProvince';
import { Ward } from '../../dataSources/dataWard';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {UserClientManagerService} from '../user-client-manager.service'


@Component({
  selector: 'kt-add-or-update',
  templateUrl: './add-or-update.component.html',
  styleUrls: ['./add-or-update.component.scss']
})
export class AddOrUpdateComponent  implements OnInit,AfterViewInit {

  itemForm: FormGroup;


  keywordProvince = 'name';
  ProvinceFake='';
  keywordDistrict = 'name';
  keywordWard = 'name';
  dataProvince=Province;
dataDistrict=District;
dataWard=Ward;

  images :any={};
  
    constructor(
      private userClientManagerService:UserClientManagerService,
      private fb: FormBuilder, private http: HttpClient,
      public dialogRef: MatDialogRef<AddOrUpdateComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      ) {
        this.itemForm = this.fb.group(
          {
            userId :'',
    motelId:'',
    title:'',
    description:'',
    motelType :1,
    longitude: '',
    latitude:'',
    priceMin:'',
    priceMax:'',
    acreageMin:'',
    acreageMax:'',
    deposits:'',
    uptopStatus:'',
    lastUptop:'',
    contactPhone:'',
    liveWithHost:'',
    privateToilet :'',
    numberBedroom: '',
    numberToilet:'',
    floor:'',
    numberFloor:'',
    direction:'',
    apartmentSituation:'',
    length:'',
    width:'',
    countView:'',
    createdDateMotel:'',
    countReport:'',
    photos:'',
    statusMotel:'',
    province: null,
    district: null,
    wards: null,
    street:null,
    contactPerson :'',
    ProvinceFake:'',
    districtFake:'',
    wardsFake:'',
    image1:'',
    image2:'',
    image3:'',
    image4:'',


          }
        )
       }
  
    ngOnInit(): void {
  this.loadItem();
  this.loadImage(this.getValue('photos'));
  this.setValue('userId',this.data.idUser)
      this.setValue('ProvinceFake',this.getValue('province'))
      this.setValue('districtFake',this.getValue('district'))
      this.setValue('wardsFake',this.getValue('wards'))

    
    }
    @ViewChild('wizard', {static: true}) el: ElementRef;
  
  loadImage(data){
  let index=0;
  data.split(',').forEach((pair) => {
    if(pair !== '') {
      this.images[index++] = pair;
      this.setValue('image'+index,pair);

    }
  });
  this.setValue('photos',this.images);
  
  }
  onChangeMotelType(data){
    

    this.setValue('motelType', parseInt(data))


  }
 
   
    submitted = false;

  
    ngAfterViewInit(): void {
      // Initialize form wizard
      const wizard = new KTWizard(this.el.nativeElement, {
        startStep: 1
      });
  
      // Validation before going to next page
      wizard.on('beforeNext', (wizardObj) => {
        // https://angular.io/guide/forms
        // https://angular.io/guide/form-validation
  
        // validate the form and use below function to stop the wizard's step
        // wizardObj.stop();
      });
  
      // Change event
      wizard.on('change', () => {
        setTimeout(() => {
          KTUtil.scrollTop();
        }, 500);
      });
    }
  
    onSubmit() {
      console.log(this.itemForm.value);
      
this.userClientManagerService.save(this.itemForm.value).subscribe(res=>{
console.log(res);

})   
    }
  
  
    loadItem(){
if(this.data.result !== 0){
  this.itemForm.patchValue(this.data.result);
}
  
  
    }

    


    selectProvice(item) {

      this.setValue('province', item.name)
      console.log('ccsaaaaaaa',     this.getValue('province') );
      this.dataDistrict = District.filter((items)=>{
        return items.provinceId== item.id;
      }) 
  
    }
  
    selectDistrict(item){

      this.setValue('district', item.name)

      this.dataWard=Ward.filter((items)=>{
            return items.districtId == item.id && items.provinceId== item.provinceId
      })
    }
    
    selectWard(item){
      this.setValue('wards', item.name)

     
    }
  
    onChangeSearchProvice(search: string) {
     
     if(search===null || search==='' || search===undefined){
      this.setValue('province',null)

     }
    
    }
  
    onChangeSearchDistrict(search: string) {
      if(search===null || search==='' || search===undefined){
        this.setValue('district',null)

     }
    
    }
    onChangeSearchWard(search: string) {
      if(search===null || search==='' || search===undefined){
        this.setValue('wards',null)
  
  }  
    }
    onFocused(e) {
      console.log('onFocused',e);
    //   this.dataWard=Ward.filter((items)=>{
    //     return items.districtId == item.id && items.provinceId== item.provinceId
    // }
    }


    onFocusedDistrict(e){
      let provicea = this.getValue('province') ;
     let idProvince;
       Province.filter((items)=>{
       if(items.name=== provicea){
        idProvince=items.id;
       }
      }) 
      this.dataDistrict = District.filter((items)=>{
        return items.provinceId== idProvince;
      }) 
  }
  
  onFocusedWards(e){
    let districta=this.getValue('district')
    let provicea = this.getValue('province') ;
    let idProvince;
    let idDistrict;
      Province.filter((items)=>{
      if(items.name=== provicea){
       idProvince=items.id;
      }
     }) 
     District.filter((items)=>{
      if(items.provinceId=== idProvince && items.name===districta){
        idDistrict=items.id;
      }
     }) 

     
    this.dataWard=Ward.filter((items)=>{
          return items.districtId == idDistrict && items.provinceId== idProvince
    })
}

   getValue(controls: string){
     return this.itemForm.controls[controls].value;
   }
   setValue(controls:string,value: any){
    return this.itemForm.controls[controls].setValue(value);
  }
 
  

}
