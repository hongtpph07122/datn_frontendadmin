import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'kt-showdetail-motel',
  templateUrl: './showdetail-motel.component.html',
  styleUrls: ['./showdetail-motel.component.scss']
})
export class ShowdetailMotelComponent implements OnInit,AfterViewInit {

  item :any ={
    userId :'',
    userName: '',
    email:'',
    fullName:'',
    phone:'',
    addRessUser:'',
    createdDateUser:'',
    motelId:'',
    title:'',
    description:'',
    motelType :'',
    longitude: '',
    latitude:'',
    addressMotel:'',
    priceMin:'',
    priceMax:'',
    acreageMin:'',
    acreageMax:'',
    deposits:'',
    uptopStatus:'',
    lastUptop:'',
    contactPhone:'',
    liveWithHost:'',
    privateToilet :'',
    numberBedroom: '',
    numberToilet:'',
    floor:'',
    numberFloor:'',
    direction:'',
    apartmentSituation:'',
    length:'',
    width:'',
    countView:'',
    createdDateMotel:'',
    countReport:'',
    photos:'',
    statusMotel:'',
    province: null,
    district: null,
    wards: null,
    street:null,
    contactPerson :'',
  
  }
  images :any={};
  
    constructor(
    ) { }
  
    ngOnInit(): void {
  this.loadItem();
  this.loadImage(this.item.photos);
    
    }
    @ViewChild('wizard', {static: true}) el: ElementRef;
  
  loadImage(data){
  let index=0;
  data.split(',').forEach((pair) => {
    if(pair !== '') {
      this.images[index++] = pair;
    }
  });
  
  console.dir(this.images);
  }
  
 
    submitted = false;
  
  
  
    ngAfterViewInit(): void {
      // Initialize form wizard
      const wizard = new KTWizard(this.el.nativeElement, {
        startStep: 1
      });
  
      // Validation before going to next page
      wizard.on('beforeNext', (wizardObj) => {
        // https://angular.io/guide/forms
        // https://angular.io/guide/form-validation
  
        // validate the form and use below function to stop the wizard's step
        // wizardObj.stop();
      });
  
      // Change event
      wizard.on('change', () => {
        setTimeout(() => {
          KTUtil.scrollTop();
        }, 500);
      });
    }
  
    onSubmit() {
      this.submitted = true;
    }
  
  
    loadItem(){
  
      this.item= JSON.parse(localStorage.getItem('itemMotelUser'));
  console.log(this.item);
  
    }
  

}
