import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowdetailMotelComponent } from './showdetail-motel.component';

describe('ShowdetailMotelComponent', () => {
  let component: ShowdetailMotelComponent;
  let fixture: ComponentFixture<ShowdetailMotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowdetailMotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowdetailMotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
