import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LayoutUtilsService, TypesUtilsService} from '../../../core/_base/crud';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {PortletModule} from '../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import {CoreModule} from '../../../core/core.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DropdownTreeviewSelectModule} from '../../../core/_base/tree-view/dropdown-treeview-select';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { UserClientManagementComponent } from './user-client-management.component';
import { ListMotelComponent } from './list-motel/list-motel.component';
import { AddOrUpdateComponent } from './add-or-update/add-or-update.component';
import { ShowdetailMotelComponent } from './showdetail-motel/showdetail-motel.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InlineSVGModule } from 'ng-inline-svg';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';


const routes: Routes = [
  {
    path:'',
    component:UserClientManagementComponent,
    children:[
      {
        path:'',
        redirectTo:'listMotel',
        pathMatch:'full'
      },
      {
        path:'listMotel',
        component:ListMotelComponent,
        data:{
          configPath:'admin/userClient/listMotel'
        }
      },
      {
        path:'showdetailMotel',
        component:ShowdetailMotelComponent,
        data:{
          configPath:'admin/userClient/showdetailMotel'
        }
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    PortletModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    NgxDatatableModule,
    AutocompleteLibModule,
    	MatSelectModule,
		MatInputModule,
    InlineSVGModule,
    NgbModule,
    
    


    
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService
  ],
  entryComponents: [
    AddOrUpdateComponent

  ],
  declarations: [

  UserClientManagementComponent,

  ListMotelComponent,

  AddOrUpdateComponent,

  ShowdetailMotelComponent],
})
export class UserManagerModule {
}
