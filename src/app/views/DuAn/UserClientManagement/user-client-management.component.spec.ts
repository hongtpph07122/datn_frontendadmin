import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserClientManagementComponent } from './user-client-management.component';

describe('UserClientManagementComponent', () => {
  let component: UserClientManagementComponent;
  let fixture: ComponentFixture<UserClientManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserClientManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserClientManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
