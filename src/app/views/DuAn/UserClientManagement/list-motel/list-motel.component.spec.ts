import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMotelComponent } from './list-motel.component';

describe('ListMotelComponent', () => {
  let component: ListMotelComponent;
  let fixture: ComponentFixture<ListMotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
