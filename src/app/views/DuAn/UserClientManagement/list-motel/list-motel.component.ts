import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PostMetolManagerService } from '../../post-metol-manager/post-metol-manager.service';
import { Page } from '../../Util/page.model';
import { ShareService } from '../../Util/share.service';
import { AddOrUpdateComponent } from '../add-or-update/add-or-update.component';
import { UserClientManagerService } from '../user-client-manager.service';

@Component({
  selector: 'kt-list-motel',
  templateUrl: './list-motel.component.html',
  styleUrls: ['./list-motel.component.scss']
})
export class ListMotelComponent implements OnInit {
  displayedColumns: string[] = ['index', 'motelType', 'createdDateMotel', 'statusMotel','street','countView','countReport','uptopStatus','action_btn'];
  dataSource = new MatTableDataSource<any>([]);
  isLoading: boolean = false;
  formSearch: FormGroup;
  page = new Page();
   user= JSON.parse(localStorage.getItem('auth-user'));

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private postMetolManagerService:PostMetolManagerService,
    private router:Router,
    private shareService:ShareService,
    private userClientManagerService :UserClientManagerService
    
    ) {
      this.page.pageNumber = 0;
      this.page.size = 5;
    }
    
paramSearch = {
  username: null,
  motelType: null,
  fromDate: null,
  toDate: null,
  status: null,
  province: null,
  district: null,
  wards: null,
};

 
  ngOnInit(): void {
 this.search(0);
  }

  loadData(){
    this.userClientManagerService.doSearch(this.user.id,{
      page:      this.page.pageNumber,
      size:   this.page.size
    }).subscribe(res=>{
      console.log(res);
      
    })
  }


  onSearch(){
    this.search(0);
  }

  showDetailMetol(row){
    localStorage.setItem("itemMotelUser", JSON.stringify(row));

    this.router.navigate(['/admin/userClient/showdetailMotel']);
    
  }


  onDelete(row){
    console.log(row);

  }
  onUpdateOrAdd(row){
    const dialogConfig = new MatDialogConfig();
dialogConfig.width = '1200px';
dialogConfig.height= '700px';
dialogConfig.autoFocus = true;
dialogConfig.data = {
  action: 'CREATE',
  titel: '',
  result: row,
  idUser:this.user.id
 
}

const dialogRef = this.dialog.open(AddOrUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.onSearch();
      }
    });

  }

  showDetailReport(row){
  
    
    

  }



  search(page: number) {
    this.isLoading = true;
   
    this.userClientManagerService.doSearch(this.user.id,{
        page:      this.page.pageNumber,
        size:   this.page.size
      }
    ).subscribe(
      (res) => {
        console.log(res);
        
        let data = res.body
        this.page.totalElements =data.totalElements;
        this.dataSource = new MatTableDataSource<any>(data.content);
        this.ref.detectChanges();
        console.log( this.page.totalElements );
        console.log(  this.dataSource );
        
        

      },
      (error) => {
        this.isLoading = false;
      },
      () => this.isLoading = false,
    );
  }

 
  getServerData(data){
  this.page.pageNumber=data.pageIndex;
  this.page.size=data.pageSize    
  this.onSearch();
  }



  onFocused(e) {
    console.log('onFocused',e);
  }





}
