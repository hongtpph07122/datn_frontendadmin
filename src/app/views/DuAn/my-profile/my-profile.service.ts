import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MyProfileService {

  private BASE_API_URL = `${environment.GATEWAY_URL}api/user`;

  constructor(private http: HttpClient) {
  }
  public showDetail(id: number) : Observable<any> {
    return this.http.get(`${this.BASE_API_URL}/details/${id}`);
  };
  public editProfile(id: number, formdata: any): Observable<any> {
    return this.http.put<any>(`${this.BASE_API_URL}/update/${id}`, formdata);
  };
}
