import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexMyProfileComponent } from './index-my-profile.component';

describe('IndexMyProfileComponent', () => {
  let component: IndexMyProfileComponent;
  let fixture: ComponentFixture<IndexMyProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexMyProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexMyProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
