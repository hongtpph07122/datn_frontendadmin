import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MyProfileService } from '../my-profile.service';
import { DataService } from '../../../_services/data.service';
import { TokenStorageService } from '../../auth-login/_service/token-storage.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'kt-index-my-profile',
  templateUrl: './index-my-profile.component.html',
  styleUrls: ['./index-my-profile.component.scss'],
  providers: [ DatePipe ]
})
export class IndexMyProfileComponent implements OnInit {
  @Input()
  userID: number;
  url : any;
  @Output()
  emitEvent = new EventEmitter();

  constructor(
    public _fb: FormBuilder,
    private MyProfileService: MyProfileService,
    private data: DataService,
    private TokenStorage: TokenStorageService,
    private datePipe: DatePipe,
  ) {
  }
  userForm: FormGroup;
  userInfo;
  ngOnInit(): void {
    this.getUser();
    this.userForm = this._fb.group({
      fullname: [''],
      phone: [''],
      address: [''],
      birthday: [],
      gender: [''],
      avatar: [''],
    })

  }
  onSelected() {

  }
  onSubmit() {
    console.log(this.userForm.value);

  }
  getUser() {
    this.MyProfileService.showDetail(this.TokenStorage.getUser().id).subscribe(
      (res) => {
        this.userInfo = res;
        this.url = this.userInfo.avatar;
        console.log(res);
        
        this.userForm.patchValue({
          fullname: this.userInfo.fullname,
          phone: this.userInfo.phone,
          address: this.userInfo.address,
          birthday: this.datePipe.transform(this.userInfo.birthday,'yyyy-MM-dd'),
          gender: this.userInfo.gender,
        });
      }
    )
  }
  getID() {

  }
  
  onFileSelected(event) {
    console.log(event);
    
    if(event.target.files.length > 0) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result
      }
      this.url = event.target.result
      const file = event.target.files[0];
      this.userForm.get('avatar').setValue(file);
    }
  }
  onClick(){
    // const formData = new FormData();
    // formData.append('file', this.userForm.get('avatar').value);
    // formData.append('fullname', this.userForm.get('fullname').value);
    // formData.append('phone', this.userForm.get('phone').value);
    // formData.append('address', this.userForm.get('address').value);
    // formData.append('gender', this.userForm.get('gender').value);
    // formData.append('birthday',  this.userForm.get('birthday').value);
    // console.log("formData Value: ",formData);

    // this.MyProfileService.editProfile(this.TokenStorage.getUser().id,formData).subscribe(
    //   (res) => console.log("Result ",res),
    //   (err) => console.log("Error" ,err)
      
    // )
    
  }
}
