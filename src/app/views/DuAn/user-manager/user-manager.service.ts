import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CmResponseModel} from '../../../core/response/cm-response.model';
import {createRequestOption} from '../Util/request-util';

@Injectable({providedIn: 'root'})
export class UserManagerService {
  private BASE_API_URL = `${environment.GATEWAY_URL}api/user`;

  constructor(private http: HttpClient) {
  }
  public doSearch(data: any, req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.post(`${this.BASE_API_URL}/search`, data, {
      params: options,
      observe: 'response'
    })
  }

  public changeStatus(id: number, dto : any): Observable<any> {
    return this.http.put(`${this.BASE_API_URL}/status/${id}`, dto);
  };
  public showDetail(id: number) : Observable<any> {
    return this.http.get(`${this.BASE_API_URL}/details/${id}`);
  };
}
