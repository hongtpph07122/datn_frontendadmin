import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {HttpHeaders} from '@angular/common/http';
import { UpdateOrSaveUserComponent } from '../update-or-save-user/update-or-save-user.component';
import { UserManagerService } from '../user-manager.service';
import { Page } from '../../Util/page.model';
import { DetailUserComponent } from '../detail-user/detail-user.component';

@Component({
  selector: 'kt-index-user',
  templateUrl: './index-user.component.html',
  styleUrls: ['./index-user.component.scss']
})
export class IndexUserComponent implements OnInit {
  dataSource = new MatTableDataSource<Object>([]);

  page = new Page();
   paramSearch = {
    username: null,
    email : null,
    roleID : null,
    status : null,
    fromDate : null,
    toDate : null,
  };

  public rows = [];
  displayedColumns: string[] = ['index', 'username', 'fullname', 'email', 'rolename','created_date','status','action_btn'];
  isLoading: boolean = false;

  changeLablesPositions(){

  }
  showDetails(){
    
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private userService:UserManagerService) {
      this.page.pageNumber = 0;
      this.page.size = 1;
    }
    ngOnInit(): void {
      this.search(0);
  
    }
  
    onSearch(){
      this.search(0);
    }
  
    showDetail(row){
      console.log("ID",row);
      
    }
    
  
    onDelete(row){
      console.log(row);
  
    }
  
   
    search(page: number) {
      console.log(this.paramSearch);
      this.isLoading = true;
      this.userService.doSearch(this.paramSearch, {
          page:      this.page.pageNumber,
          size:   this.page.size
        }
      ).subscribe(
        (res) => {
          console.log(res);
          
          let data = res.body
          this.page.totalElements =data.totalElements;
          this.dataSource = new MatTableDataSource<any>(data.content);
          this.ref.detectChanges();
          console.log( this.page.totalElements );
          
  
        },
        (error) => {
          this.isLoading = false;
        },
        () => this.isLoading = false,
      );
    }
  
   
    getServerData(data){
    this.page.pageNumber=data.pageIndex;
    this.page.size=data.pageSize    
    this.onSearch();
    }
   
    changeStatusUser(element :any){
      if(element.status == 1){
        element.status = -1;
      }else{
        element.status = 1;
      }
      this.userService.changeStatus(element.id,element).subscribe( result =>
        console.log("RESPONSE :", result)
      );
      
    }
    addNew(element: any){
      console.log("XXXXXX", element);
      
      const dialogRef = this.dialog.open(DetailUserComponent, {
        data: {
          user: element
        }
      });
      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          console.log(res);
          
        }
      });
  
    }
}
