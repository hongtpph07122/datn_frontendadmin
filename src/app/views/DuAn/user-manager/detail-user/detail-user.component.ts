import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserManagerService } from '../user-manager.service';

@Component({
  selector: 'kt-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {
  user: any;
  constructor(
    private dialogRef: MatDialogRef<DetailUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserManagerService,

  ) { }

  ngOnInit(): void {
   this.getUser();
  }
  getUser(){
    this.userService.showDetail(this.data.user.id).subscribe(
      result => {
        this.user = result;
      console.log(result);
      }
      
    )
  }

}
