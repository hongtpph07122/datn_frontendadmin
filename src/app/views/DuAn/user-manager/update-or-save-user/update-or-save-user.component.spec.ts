import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOrSaveUserComponent } from './update-or-save-user.component';

describe('UpdateOrSaveUserComponent', () => {
  let component: UpdateOrSaveUserComponent;
  let fixture: ComponentFixture<UpdateOrSaveUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateOrSaveUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOrSaveUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
