import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'kt-update-or-save-user',
  templateUrl: './update-or-save-user.component.html',
  styleUrls: ['./update-or-save-user.component.scss']
})
export class UpdateOrSaveUserComponent implements OnInit {
  areaForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  constructor(
    public dialogRef: MatDialogRef<UpdateOrSaveUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.creatForm();
  }
  creatForm(){
    this.areaForm = this.fb.group({
      id: null,
      appCode: null,
      appName: null,
      active: null
    });
  }

  getTitle(): string {
   
    return 'Thêm mới ';
  }

  
  get f() {
    return this.areaForm.controls;
  }
  onSubmit(){

  }

}
