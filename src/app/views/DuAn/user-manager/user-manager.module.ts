import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LayoutUtilsService, TypesUtilsService} from '../../../core/_base/crud';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {PortletModule} from '../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import {CoreModule} from '../../../core/core.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DropdownTreeviewSelectModule} from '../../../core/_base/tree-view/dropdown-treeview-select';
import { UserManagerComponent } from './user-manager.component';
import { IndexUserComponent } from './index-user/index-user.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { UpdateOrSaveUserComponent } from './update-or-save-user/update-or-save-user.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { DetailUserComponent } from './detail-user/detail-user.component';


const routes: Routes = [
  {
    path:'',
    component:UserManagerComponent,
    children:[
      {
        path:'',
        redirectTo:'index',
        pathMatch:'full'
      },
      {
        path:'index',
        component:IndexUserComponent,
        data:{
          configPath:'post-metol-manager/index'
        }
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    PortletModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    NgxDatatableModule,
    MatSlideToggleModule,

    
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService
  ],
  entryComponents: [
    UpdateOrSaveUserComponent,
    DetailUserComponent,

  ],
  declarations: [
  UserManagerComponent,
  IndexUserComponent,
  UpdateOrSaveUserComponent,
  DetailUserComponent
  ],
})
export class UserManagerModule {
}
