import { Component, OnInit } from '@angular/core';
import { AuthRegisterService } from '../_service/auth-register.service';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'kt-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  constructor(
    private RegisterService: AuthRegisterService,
    private _fb : FormBuilder,
    private router : Router,
    // private toastr : ToastrService,
    ) { }
  usernamePattern = "^[0-9a-zA-Z_]+$";
  passPattern = "^[0-9a-zA-Z]+$";
  emailPattern = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
  
  phonePattern = "^(\\+84|0)\\d{9,10}$"
  signupForm: FormGroup;
  ngOnInit(): void {
    this.signupForm = this._fb.group(
      {
        username: ['user1303',[Validators.required, Validators.minLength(6),Validators.maxLength(16), Validators.pattern(this.usernamePattern)]],
        password: ['123456',[Validators.required, Validators.minLength(6),Validators.maxLength(20), Validators.pattern(this.passPattern)]],
        repassword: ['',Validators.required],
        email: ['user1303@gmail.com',[Validators.required, Validators.minLength(6),Validators.maxLength(50), Validators.pattern(this.emailPattern)]],
        fullname: ['Hoang Gia Thatt',[Validators.required, Validators.minLength(4),Validators.maxLength(50)]],
        phone: ['09876543321',[Validators.required, Validators.pattern(this.phonePattern)]],
        role: [['user']],
      }, {
        validators: this.ConfirmedValidator('password','repassword')
      }
      
    )
  }
  ConfirmedValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
            return;
        }
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
  
  isLoading: boolean = false;
  onSubmit(){
    this.registerUser();
  }
  registerUser(){
    console.log("info :",this.signupForm.value);
    this.isLoading = true;
    this.RegisterService.registerUser2(this.signupForm.value)
    .subscribe(
      (res) => {
        console.log("Result: ",res);
        
        if(res.message === 'Success!'){
          // this.toastr.success(res, 'Thành công!');
          this.router.navigateByUrl('/login');
        }else{
          // this.toastr.error(res, 'Rất tiếc!');
        }
      }
     
      
    ),
    (error) => {
      this.isLoading = false;
      console.error("Error: ",error);
      
    },
    () => this.isLoading = false;
    
  }


}
