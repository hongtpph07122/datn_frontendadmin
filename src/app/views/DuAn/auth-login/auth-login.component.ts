import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthLoginService } from './auth-login.service';
import { TokenStorageService } from './_service/token-storage.service';

@Component({
  selector: 'kt-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss']
})
export class AuthLoginComponent implements OnInit {
  constructor( ) { }
  ngOnInit(): void {
  }
}
