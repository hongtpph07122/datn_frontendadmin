import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, throwError } from 'rxjs';
import { TokenStorageService } from '../auth-login/_service/token-storage.service';

const AUTH_API = `${environment.GATEWAY_URL}api/auth/`;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AuthLoginService {
  private http : HttpClient;
  constructor(  private jwtHelper : JwtHelperService, private tokenStorageService : TokenStorageService, private handler: HttpBackend) { 
    this.http = new HttpClient(handler);
  }

  public login(loginValue): Observable<any> {
    return this.http.post(`${AUTH_API}signin`, loginValue, httpOptions);
  }
  
  public isAuthenticated():boolean {
    const token = this.tokenStorageService.getToken();
    return !this.jwtHelper.isTokenExpired(token);
  }
  public isLogin() : boolean{
    return !!this.tokenStorageService.getToken();
  } 
}
