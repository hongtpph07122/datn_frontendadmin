import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthLoginService } from '../auth-login.service';
import decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate{
  tokenPayload: any;
  constructor(public auth: AuthLoginService, public router: Router) { }
  canActivate(route: ActivatedRouteSnapshot): boolean  {
   
    if(this.auth.isAuthenticated()){
      const expectedRole = route.data.expectedRole;
      const token = localStorage.getItem('auth-token');
      this.tokenPayload = decode(token);
      for(let index = 0; index < this.tokenPayload.role.length ; index++)
      {
        console.log(this.tokenPayload.role[index]);
        
        if( this.tokenPayload.role[index].authority == expectedRole){
          return true;
        }
      }
      window.alert('Tài khoản không được quyền truy cập trang này !!')
      this.router.navigate(['home']);
      return false;
    }
    return false;
  }
}
