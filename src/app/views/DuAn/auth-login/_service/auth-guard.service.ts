import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { TokenStorageService } from '../_service/token-storage.service';
import { AuthLoginService } from '../auth-login.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private auth: AuthLoginService, private router: Router,
    private token: TokenStorageService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if(!this.auth.isLogin()){
      this.router.navigate(['login']);
      return false;
    }
    if(!this.auth.isAuthenticated()){
      window.alert("Phiên đăng nhập đã hết hạn !!");
      this.token.signOut();
      window.location.replace('login');
      return false;
    }
    return true;

  }
    
}
