import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthLoginService } from '../auth-login.service';
import { TokenStorageService } from '../_service/token-storage.service';
import { DataService } from '../../../_services/data.service';
@Component({
  selector: 'kt-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage :any;
  constructor(
    private _fb : FormBuilder,
    private router : Router,
    private tokenStorage : TokenStorageService,
    private authLoginService : AuthLoginService,
    private data : DataService,
  ) { }
  usernamePattern = "^[0-9a-zA-Z_]+$";
  passPattern = "^[0-9a-zA-Z]+$";
  ngOnInit(): void {
    this.loginForm = this._fb.group(
      {
        username: ['',[Validators.required, Validators.minLength(6),Validators.maxLength(16),Validators.pattern(this.usernamePattern)]],
        password: ['',[Validators.required, Validators.minLength(6),Validators.maxLength(20),Validators.pattern(this.passPattern)]],
      }
    )
  }
  onSubmit(){
    console.log("user :",this.loginForm.value );
    
    this.login();
  }
  login(){
    
    this.authLoginService.login(this.loginForm.value)
    .subscribe(
      (res) => {
        console.log(res);
        this.tokenStorage.saveToken(res.accessToken);
        this.tokenStorage.saveUser(res);
       
        this.data.setProfileObs(res.id);
      
        this.router.navigateByUrl("/admin/dashboard");
        
      },
      (err) => {
        if(err.error.error == 'Unauthorized'){
          this.errorMessage =  'Tài khoản hoặc mật khẩu không chính xác';
        }
      }
    );
  }
}
