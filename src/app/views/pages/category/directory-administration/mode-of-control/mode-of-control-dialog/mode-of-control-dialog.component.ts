import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../../model/category';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../../category.service';
import {SUCCESS_RESPONSE} from '../../../../../../core/constants/app.constants';

@Component({
  selector: 'kt-mode-of-control-dialog',
  templateUrl: './mode-of-control-dialog.component.html',
  styleUrls: ['./mode-of-control-dialog.component.scss']
})
export class ModeOfControlDialogComponent implements OnInit {
  modeOfControlForm: FormGroup;
  categoryModelData: CategoryModel = new CategoryModel();
  hasFormErrors = false;
  viewLoading = false;

  constructor(
    public dialogRef: MatDialogRef<ModeOfControlDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryModelData = this.data.category;
    this.creatForm();
  }

  creatForm(){
    this.modeOfControlForm = this.fb.group({
      id: [this.categoryModelData.id],
      appCode: [this.categoryModelData.appCode, Validators.compose([Validators.required])],
      appName: [this.categoryModelData.appName,Validators.compose([Validators.required])],
      note: [this.categoryModelData.status]
    });
  }
  getTitle(): string {
    if (this.categoryModelData.id) {
      return 'Cập nhật phương thức kiểm soát';
    }
    return 'Thêm mới phương thức kiểm soát';
  }

  get f() {
    return this.modeOfControlForm.controls;
  }

  /**
   * On Submit
   */
  onSubmit() {
    const controls = this.modeOfControlForm.controls;
    /** check form */
    if(this.modeOfControlForm.invalid){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const categoryModel = this.prepareCustomer();
    if (categoryModel.id) {
      this.updateCategory(categoryModel);
    } else {
      this.createCategory(categoryModel);
    }
  }
  prepareCustomer(): CategoryModel {
    const category = new CategoryModel();
    category.appType = 'DM_PT_KIEM_SOAT';
    category.id = this.f.id.value;
    category.appCode = this.f.appCode.value.trim();
    category.appName = this.f.appName.value.trim();
    category.extraObj = {
      note: this.f.note.value.trim()
    }
    return category;
  }
  public updateCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.editAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Cap nhat thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  public createCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.createAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Them moi thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
}
