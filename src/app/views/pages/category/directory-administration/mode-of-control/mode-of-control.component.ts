import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../model/category';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {CategoryService} from '../../category.service';
import {CategoryAreaDialogComponent} from '../../category-area/category-area-dialog/category-area-dialog.component';
import {CategoryDeleteDialogComponent} from '../../category-delete-dialog/category-delete-dialog.component';
import {ModeOfControlDialogComponent} from './mode-of-control-dialog/mode-of-control-dialog.component';

@Component({
  selector: 'kt-mode-of-control',
  templateUrl: './mode-of-control.component.html',
  styleUrls: ['./mode-of-control.component.scss']
})
export class ModeOfControlComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'appCode', 'appName', 'note', 'action'];
  dataSource = new MatTableDataSource<CategoryModel>([]);
  isLoading: boolean = false;
  categoryForm: FormGroup;
  appType = 'DM_PT_KIEM_SOAT';
  appCode = '';
  appName = '';
  status = 'A';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private categoryService: CategoryService,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
  }

  deleteCategoryArea(category: CategoryModel) {
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: category,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });

  }
  editCategoryArea(value){
    const categoryInt = value;
    const dialogRef = this.dialog.open(ModeOfControlDialogComponent, {
      data: {
        action: 'Edit',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }

  addNew(){
    const categoryInt = new CategoryModel();
    const dialogRef = this.dialog.open(ModeOfControlDialogComponent, {
      data: {
        action: 'CREATE',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });

  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
    this.isLoading = true;
    this.categoryService.getAllAppParam(this.appType,this.appCode,this.appName,this.status).subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }

  reloadTable(data) {
    this.isLoading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.ref.detectChanges();
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

}
