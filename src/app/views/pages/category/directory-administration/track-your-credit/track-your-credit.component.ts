import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {AddEditGroupErrorComponent} from '../../category-group-error/add-edit-group-error/add-edit-group-error.component';
import {ModalEditComponent} from './modal-edit/modal-edit.component';
import {CategoryModel} from '../../model/category';
import {CategoryDeleteDialogComponent} from '../../category-delete-dialog/category-delete-dialog.component';

@Component({
  selector: 'kt-track-your-credit',
  templateUrl: './track-your-credit.component.html',
  styleUrls: ['./track-your-credit.component.scss']
})
export class TrackYourCreditComponent implements OnInit {

  displayedColumns: string[] = [ 'maCN', 'tenCN', 'maKH', 'tenKH','maMD','maTSDB','action'];
  isLoading = false;
  categoryForm: FormGroup;
  dataSource = [];
  appType = 'DM_KHU_VUC';
  appCode = '';
  appName = '';
  status = '';
  demo = {
    // maCN: '123456',
    // tenCN: 'CN Hoàng Mai',
    // maKH: 'KH12',
    // tenKH: 'Kiều Cao Long',
    // maMD: 'MD12',
    // maTSDB: 'TSDB12',
    // moTaTs: 'abcd',
    // kyDGTS: 'DGYS12',
    // TTDGTS: 'abc',
    // TTKTTS: 'abd'
  }

  public listTC: Criteria[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.dataSource.push(this.demo)
    this.createForm();
    this.getDataTable();
    this.listTC.push(new Criteria());
  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }

  editPricing(value){

  }
  filePricing(value){}

  addNew(){
    const dialogRef = this.dialog.open(ModalEditComponent, {
      data: {
        data: '',
        action: 'CREATE'
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }

  opendEdit(){
    const dialogRef = this.dialog.open(ModalEditComponent, {
      data: {
        data: '',
        action: 'Edit'
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }
  deleteGroupError(error: CategoryModel) {
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: error,
        appType: ''
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
    // console.log(deleteInitData.afterClosed().subscribe());
  }

}
export class Criteria {
}
