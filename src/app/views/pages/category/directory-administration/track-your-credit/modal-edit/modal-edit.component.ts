import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../../model/category';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../../category.service';
import {SUCCESS_RESPONSE} from '../../../../../../core/constants/app.constants';

@Component({
  selector: 'kt-modal-edit',
  templateUrl: './modal-edit.component.html',
  styleUrls: ['./modal-edit.component.scss']
})
export class ModalEditComponent implements OnInit {

  titleHeader: string; titleButton: string; modalStatus: string;

  formGroup: FormGroup; modalData: CategoryModel= new CategoryModel();
  loading = false;

  constructor(
    private dialogRef: MatDialogRef<ModalEditComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: CategoryService,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.modalData = this.data.data;
    this.modalStatus = this.data.action;
    this.createFormData();
    this.setTitle();
  }

  createFormData() {
    this.formGroup = this.formBuilder.group({
    });
  }

  setTitle() {
    if(this.modalStatus === 'CREATE') {
      this.titleHeader = 'Thêm mới điều kiện';
      this.titleButton = 'Thêm mới';
    } else {
      this.titleHeader = 'Cập nhật điều kiện';
      this.titleButton = 'Cập nhật';
    }
  }

  prepareCustoms(): CategoryModel {
    const category = new CategoryModel();
    category.id = this.formGroup.value.id;
    category.appType = 'DM_NHOM_MA_LOI';
    category.appCode = this.formGroup.value.appCode;
    category.appName = this.formGroup.value.appName;
    category.status = this.formGroup.value.status;
    category.parentCode = this.formGroup.value.parentCode;
    category.parentName = this.formGroup.value.parentName;
    category.extraObj = this.formGroup.value.extraObj;

    return category;
  }

  onSubmit() {
    const controls = this.formGroup.controls;
    if (this.formGroup.invalid) {
      // Object.keys(controls).forEach(controlName => controls[controlName].markAllAsTouched());
      return;
    }
    const data = this.prepareCustoms();
    if (this.data.data.id) {
      this.updateGroupError(data);
    } else {
      this.createGroupError(data);
    }
  }

  createGroupError(groupError: CategoryModel) {
    this.loading = true;
    this.service.createAppParam(groupError).subscribe(data => {
      this.loading = false;
      if (data.code === SUCCESS_RESPONSE) {
        this.dialogRef.close(groupError);
        alert('Thêm mới thành công');
      } else {
        alert(data.message);
        this.dialogRef.close(groupError);
      }
    }, error => {
      this.loading = false;
      alert(error);
    });
  }

  updateGroupError(groupError: CategoryModel) {
    console.log('open update')
    this.loading = true;
    this.service.editAppParam(groupError).subscribe(data => {
      this.loading = false;
      if (data.code === SUCCESS_RESPONSE) {
        alert('Chỉnh sửa thành công');
        this.dialogRef.close(groupError);
        console.log('sjdbjsbbsjbsdjbsjdbjs update 1')
      } else {
        this.dialogRef.close(groupError);
        alert(data.message);
        console.log(data.message)
        console.log('update 2')
      }
    }, error => {
      this.loading = false;
      alert(error);
      console.log(error)
      console.log('error')
    });
  }

}
