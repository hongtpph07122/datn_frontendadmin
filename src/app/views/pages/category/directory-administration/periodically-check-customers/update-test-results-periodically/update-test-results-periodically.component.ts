import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'kt-update-test-results-periodically',
  templateUrl: './update-test-results-periodically.component.html',
  styleUrls: ['./update-test-results-periodically.component.scss']
})
export class UpdateTestResultsPeriodicallyComponent implements OnInit {

  collapser = false;
  formGroup: FormGroup;

  constructor() { }

  ngOnInit(): void {

  }

  saveUpdatePeriodically() {

  }
}
