import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../../model/category';
import {MatPaginator} from '@angular/material/paginator';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';

class searchCriteriaModel {
  criteria: string;
  operator: string;
  combined: string;
}

@Component({
  selector: 'kt-approval-confirmation',
  templateUrl: './approval-confirmation.component.html',
  styleUrls: ['./approval-confirmation.component.scss']
})
export class ApprovalConfirmationComponent implements OnInit {

  displayedColumns: string[] = ['checkbox', 'stt', 'maCN', 'tenCN', 'maKH', 'tenKH', 'PK', 'maREF', 'SP', 'KV', 'NGN', 'status'];
  dataSource = new MatTableDataSource<CategoryModel>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  formGroup: FormGroup;
  loading: any;
  searchCriteria: searchCriteriaModel[] = [];
  numbersSearchBox: Array<number> = [1];
  hiddenButtonDelete = true;

  constructor(
    private dialog: MatDialog,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.searchCriteria.push(new searchCriteriaModel());

    this.formGroup = new FormGroup({
      maCN: new FormControl(''),
      maKH: new FormControl(''),
      tenKH: new FormControl(''),
      PK: new FormControl(''),
      maREF: new FormControl(''),
      SP: new FormControl(''),
      KV: new FormControl(''),
      NGN: new FormControl(''),
      status: new FormControl(''),
    })
  }

  search() {

  }

  reloadTable(data) {
    this.loading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.changeDetectorRef.detectChanges();
  }

  addSearchCriteria() {
    this.searchCriteria.push(new searchCriteriaModel());
    // this.hiddenButtonDelete = false;
  }

  deleteSearchCriteria(index) {
    this.searchCriteria.splice(index,1);
    // this.hiddenButtonDelete = true;
  }
}
