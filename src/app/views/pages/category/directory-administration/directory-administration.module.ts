import {RouterModule, Routes} from '@angular/router';
import {DirectoryAdministrationComponent} from './directory-administration.component';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {DropdownTreeviewSelectModule} from '../../../../core/_base/tree-view/dropdown-treeview-select';
import {LayoutUtilsService, TypesUtilsService} from '../../../../core/_base/crud';
import {NgModule} from '@angular/core';
import {ConditionGroupComponent} from './condition-group/condition-group.component';
import {ModeOfControlComponent} from './mode-of-control/mode-of-control.component';
import {ProductTypeComponent} from './product-type/product-type.component';
import {PortletModule} from '../../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';
import {CoreModule} from '../../../../core/core.module';
import { AddEditConditionGroupComponent } from './condition-group/add-edit-condition-group/add-edit-condition-group.component';
import {CommonModule} from '@angular/common';
import { UpdateTestResultsPeriodicallyComponent } from './periodically-check-customers/update-test-results-periodically/update-test-results-periodically.component';
import { ApprovalConfirmationComponent } from './periodically-check-customers/approval-confirmation/approval-confirmation.component';
import { ProductTypeDialogComponent } from './product-type/product-type-dialog/product-type-dialog.component';
import { ModeOfControlDialogComponent } from './mode-of-control/mode-of-control-dialog/mode-of-control-dialog.component';
import {PeriodicallyCheckCustomersComponent} from './periodically-check-customers/periodically-check-customers.component';
import { AssetRevaluationFrequencyComponent } from './asset-revaluation-frequency/asset-revaluation-frequency.component';
import { TrackRecordInputComponent } from './asset-revaluation-frequency/track-record-input/track-record-input.component';
import { TrackYourCreditComponent } from './track-your-credit/track-your-credit.component';
import { ModalEditComponent } from './track-your-credit/modal-edit/modal-edit.component';

const routes: Routes = [
  {
    path: '',
    component: DirectoryAdministrationComponent,
    children: [
      {
        path: '',
        redirectTo: 'condition-group',
        pathMatch: 'full'
      },
      {
        path: 'condition-group',
        component: ConditionGroupComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/condition-group'
        }
      },
      {
        path: 'mode-of-control',
        component: ModeOfControlComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/mode-of-control'
        }
      },
      {
        path: 'product-type',
        component: ProductTypeComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/product-type'
        }
      },
      {
        path: 'checking-customers-periodically',
        component: PeriodicallyCheckCustomersComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/checking-customers-periodically'
        }
      },
      {
        path: 'asset-revaluation-frequency',
        component: AssetRevaluationFrequencyComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/asset-revaluation-frequency'
        }
      },
      {
        path: 'update-test-results-periodically',
        component: UpdateTestResultsPeriodicallyComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/update-test-results-periodically'
        }
      },
      {
        path: 'approval-confirmation',
        component: ApprovalConfirmationComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/approval-confirmation'
        }
      },
      {
        path: 'track-record-input',
        component: ApprovalConfirmationComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/track-record-input'
        }
      },
      {
        path: 'track-your-credit',
        component: TrackYourCreditComponent,
        data: {
          configPath: '/monitoring-after-disbursement/directory-administration/track-your-credit'
        }
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    DropdownTreeviewSelectModule,
    PortletModule,
    MatButtonModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    CoreModule,
    FormsModule,
    MatIconModule,
    CommonModule,
    MatDialogModule,
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService
  ],
  entryComponents: [
    AddEditConditionGroupComponent,
    ProductTypeDialogComponent,
    ModeOfControlDialogComponent,
    ModalEditComponent
  ],
  declarations: [
    DirectoryAdministrationComponent,
    ConditionGroupComponent,
    ModeOfControlComponent,
    ProductTypeComponent,
    AddEditConditionGroupComponent,
    UpdateTestResultsPeriodicallyComponent,
    ApprovalConfirmationComponent,
    ProductTypeDialogComponent,
    ModeOfControlDialogComponent,
    PeriodicallyCheckCustomersComponent,
    AssetRevaluationFrequencyComponent,
    TrackRecordInputComponent,
    TrackYourCreditComponent,
    ModalEditComponent
  ],
})
export class DirectoryAdministrationModule {
}
