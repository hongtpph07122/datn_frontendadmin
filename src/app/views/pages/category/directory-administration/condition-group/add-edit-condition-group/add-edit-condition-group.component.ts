import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../../model/category';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../../category.service';
import {AddEditGroupErrorComponent} from '../../../category-group-error/add-edit-group-error/add-edit-group-error.component';
import {SUCCESS_RESPONSE} from '../../../../../../core/constants/app.constants';

@Component({
  selector: 'kt-add-edit-condition-group',
  templateUrl: './add-edit-condition-group.component.html',
  styleUrls: ['./add-edit-condition-group.component.scss']
})
export class AddEditConditionGroupComponent implements OnInit {

  titleHeader: string; titleButton: string; modalStatus: string;

  formGroup: FormGroup; modalData: CategoryModel= new CategoryModel(); loading = false;

  constructor(
    private dialogRef: MatDialogRef<AddEditGroupErrorComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: CategoryService,
  ) { }

  ngOnInit(): void {
    this.modalData = this.data.data;
    if(!this.modalData.id) {
      this.modalData.extraObj = {
        describe: ''
      };
      this.titleHeader = 'Thêm mới nhóm điều kiện';
      this.titleButton = 'Thêm mới';
    } else {
      this.titleHeader = 'Sửa nhóm điều kiện';
      this.titleButton = 'Cập nhật';
    }
    this.createFormData();
  }

  createFormData() {
    this.formGroup = this.formBuilder.group({
      id: [this.modalData.id],
      appType: [this.modalData.appType],
      appCode: [this.modalData.appCode, Validators.required],
      appName: [this.modalData.appName, Validators.required],
      parentCode: [this.modalData.parentCode, Validators.required],
      describe: [this.modalData.extraObj.describe]
    });
  }

  prepareCustoms(): CategoryModel {
    const category = new CategoryModel();
    category.appType = 'DM_NHOM_DIEU_KIEN';
    category.id = this.formData.id.value;
    category.appCode = this.formData.appCode.value.trim();
    category.appName = this.formData.appName.value.trim();
    category.parentCode = this.formData.parentCode.value;
    category.extraObj = { describe: this.formData.describe.value};
    return category;
  }

  get formData() {
    return this.formGroup.controls;
  }

  onSubmit() {
    const controls = this.formGroup.controls;
    if(this.formGroup.invalid){
      Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched()
      );
      return;
    }
    const categoryModel = this.prepareCustoms();
    if (categoryModel.id) {
      this.updateGroupCondition(categoryModel);
    } else {
      this.createGroupCondition(categoryModel);
    }
  }

  public createGroupCondition(category: CategoryModel){
    this.service.createAppParam(category).subscribe(response => {
        if (response.code === SUCCESS_RESPONSE) {
          alert('Thêm mới thành công');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        console.log(error);
      });
  }

  updateGroupCondition(data: CategoryModel) {
    this.service.editAppParam(data).subscribe(
      response => {
        if (response.code === SUCCESS_RESPONSE) {
          alert('Cap nhat thanh cong');
          this.dialogRef.close(data);
        } else {
          alert(response.message);
        }
      },
      error => {
        console.log(error);
      });
  }
}
