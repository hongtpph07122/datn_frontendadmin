import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CategoryService} from '../../category.service';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../model/category';
import {MatPaginator} from '@angular/material/paginator';
import {FormControl, FormGroup} from '@angular/forms';
import {AddEditConditionGroupComponent} from './add-edit-condition-group/add-edit-condition-group.component';
import {CategoryDeleteDialogComponent} from '../../category-delete-dialog/category-delete-dialog.component';

@Component({
  selector: 'kt-condition-group',
  templateUrl: './condition-group.component.html',
  styleUrls: ['./condition-group.component.scss']
})
export class ConditionGroupComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'parentCode', 'appCode', 'appName', 'describe', 'action'];
  dataSource = new MatTableDataSource<CategoryModel>([]);
  loading = false; valueAppName = ''; valueAppCode = '';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  formGroup: FormGroup;

  constructor(
    private dialog: MatDialog,
    private service: CategoryService,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.getDataTable();
    this.formGroup = new FormGroup({
      appCode: new FormControl(''),
      appName: new FormControl('')
    })
  }

  private getDataTable() {
    this.loading = true;
    this.service.getAllAppParam('DM_NHOM_DIEU_KIEN',this.valueAppCode,this.valueAppName,'A').subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
    });
  }

  reloadTable(data) {
    this.loading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.changeDetectorRef.detectChanges();
  }


  addGroupCondition() {
    const addInitData = new CategoryModel();
    const dialogRef = this.dialog.open(AddEditConditionGroupComponent, {
      data: {
        data: addInitData,
        action: 'CREATE'
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }

  searchGroupCondition() {
    this.valueAppCode = this.formGroup.get('appCode').value.replace(/(\s\s+| )/g, ' ').trim();
    this.valueAppName = this.formGroup.get('appName').value.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  editGroupCondition(dataEdit: CategoryModel) {
    const editInitData = this.dialog.open(AddEditConditionGroupComponent, {
      data: {
        data: dataEdit,
        action: 'UPDATE'
      }
    });
    editInitData.afterClosed().subscribe(dataSub => {
      if (dataSub) {
        this.getDataTable();
      }
    });
  }

  deleteGroupCondition(groupCondition: CategoryModel) {
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: groupCondition,
        appType: 'DM_NHOM_DIEU_KIEN'
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
    console.log(deleteInitData.afterClosed().subscribe());
  }
}
