import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../model/category';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {CategoryService} from '../../category.service';
import {CategoryUnitDialogComponent} from '../../category-unit/category-unit-dialog/category-unit-dialog.component';
import {CategoryDeleteDialogComponent} from '../../category-delete-dialog/category-delete-dialog.component';
import {ProductTypeDialogComponent} from './product-type-dialog/product-type-dialog.component';

@Component({
  selector: 'kt-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.scss']
})
export class ProductTypeComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'segment', 'appCode','appName','parentName', 'action'];
  isLoading = false;
  productTypeForm: FormGroup;
  dataSource = new MatTableDataSource<CategoryModel>([]);
  dataArea: CategoryModel[] = [];
  appType = 'DM_LOAI_SAN_PHAM';
  appCode = '';
  appName = '';
  status = 'A';


  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private categoryService: CategoryService,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
  }

  editCategoryUnit(value){
    const categoryInt = value;
    const dialogRef = this.dialog.open(ProductTypeDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  deleteCategoryUnit(value: CategoryModel){
    console.log(value);
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: value,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }
  addNew(){
    const categoryInt = new CategoryModel();
    const dialogRef = this.dialog.open(ProductTypeDialogComponent, {
      data: {
        action: 'CREATE',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  createForm(){
    this.productTypeForm = this.fb.group({
      codeProductType: [''],
      nameProductType: ['']
    });
  }
  find(){
    this.appCode = this.productTypeForm.value.codeProductType;
    this.appName = this.productTypeForm.value.nameProductType;
    this.getDataTable();
  }
  public getDataTable(){
    this.isLoading = true;
    this.categoryService.getAllAppParam(this.appType,this.appCode,this.appName,this.status).subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }
  reloadTable(data) {
    this.isLoading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.ref.detectChanges();
  }
}
