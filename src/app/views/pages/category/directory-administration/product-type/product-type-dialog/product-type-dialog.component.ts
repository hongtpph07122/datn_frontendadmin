import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../../model/category';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../../category.service';
import {SUCCESS_RESPONSE} from '../../../../../../core/constants/app.constants';

@Component({
  selector: 'kt-product-type-dialog',
  templateUrl: './product-type-dialog.component.html',
  styleUrls: ['./product-type-dialog.component.scss']
})
export class ProductTypeDialogComponent implements OnInit {

  productTypeForm: FormGroup;
  categoryModelData: CategoryModel = new CategoryModel();
  hasFormErrors = false;
  viewLoading = false;
  dataCondition: CategoryModel[] = [];
  titleButton = ''

  constructor(
    public dialogRef: MatDialogRef<ProductTypeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.categoryModelData = this.data.category;
    if(!this.categoryModelData.id) {
      this.categoryModelData.extraObj = {
        email: '',
        employee: ''
      };
      this.categoryModelData.parentCode = '';
      this.titleButton = 'Thêm mới';
    }else {
      this.titleButton = 'Cập nhật';
    }
    this.creatForm();
    this.loadCondition();
  }

  creatForm(){
    this.productTypeForm = this.fb.group({
      id: [this.categoryModelData.id],
      appCode: [this.categoryModelData.appCode, Validators.compose([Validators.required])],
      appName: [this.categoryModelData.appName,Validators.compose([Validators.required])],
      segment: [this.categoryModelData.extraObj.segment?this.categoryModelData.extraObj.segment:''],
      parentCode: [this.categoryModelData.parentCode],
      note: [this.categoryModelData.extraObj.note]
    });
  }
  getTitle(): string {
    if (this.categoryModelData.id) {
      return 'Thêm mới điều kiện';
    }
    return 'Cập nhật điều kiện';
  }

  get f() {
    return this.productTypeForm.controls;
  }

  /**
   * On Submit
   */
  onSubmit() {
    const controls = this.productTypeForm.controls;
    /** check form */
    if(this.productTypeForm.invalid){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const categoryModel = this.prepareCustomer();
    if (categoryModel.id) {
      this.updateCategory(categoryModel);
    } else {
      this.createCategory(categoryModel);
    }
  }
  prepareCustomer(): CategoryModel {
    const category = new CategoryModel();
    category.appType = 'DM_LOAI_SAN_PHAM';
    category.id = this.f.id.value;
    category.appCode = this.f.appCode.value.trim();
    category.appName = this.f.appName.value.trim();
    category.parentCode = this.f.parentCode.value;
    const index = this.dataCondition.findIndex(item => item.appCode === category.parentCode);
    category.parentName = this.dataCondition[index].appName;
    category.extraObj = {
      note: this.f.note.value,
      segment: this.f.segment.value
    }
    return category;
  }
  public updateCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.editAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Cap nhat thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  public createCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.createAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Them moi thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  loadCondition(){
    this.categoryService.getAllAppParam('DM_NHOM_MA_LOI','','','').subscribe(response => {
      this.dataCondition = response.data;
      this.ref.detectChanges();
    }, error => {
      console.log(error);
    });
  }
}
