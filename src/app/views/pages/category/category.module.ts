import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LayoutUtilsService, TypesUtilsService} from '../../../core/_base/crud';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {CategoryComponent} from './category.component';
import {CategoryAreaComponent} from './category-area/category-area.component';
import {CategoryUnitComponent} from './category-unit/category-unit.component';
import {CategoryErrorCodeComponent} from './category-error-code/category-error-code.component';
import {CategoryGroupErrorComponent} from './category-group-error/category-group-error.component';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {PortletModule} from '../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import {CoreModule} from '../../../core/core.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import { AddEditGroupErrorComponent } from './category-group-error/add-edit-group-error/add-edit-group-error.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CategoryAreaDialogComponent} from './category-area/category-area-dialog/category-area-dialog.component';
import { CategoryDeleteDialogComponent } from './category-delete-dialog/category-delete-dialog.component';
import {DropdownTreeviewSelectModule} from '../../../core/_base/tree-view/dropdown-treeview-select';
import { CategoryUnitDialogComponent } from './category-unit/category-unit-dialog/category-unit-dialog.component';
import { CategoryErorCodeDialogComponent } from './category-error-code/category-eror-code-dialog/category-eror-code-dialog.component';
import {DirectoryAdministrationModule} from './directory-administration/directory-administration.module';

const routes: Routes = [
  {
    path: '',
    component: CategoryComponent,
    children: [
      {
        path: '',
        redirectTo: 'area',
        pathMatch: 'full'
      },
      {
        path: 'area',
        component: CategoryAreaComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/category/area'
        }
      },
      {
        path: 'unit',
        component: CategoryUnitComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/category/unit'
        }
      },
      {
        path: 'error-code',
        component: CategoryErrorCodeComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/category/error-code'
        }
      },
      {
        path: 'group-error',
        component: CategoryGroupErrorComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/category/group-error'
        }
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    PortletModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    DirectoryAdministrationModule
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService
  ],
  entryComponents: [
    AddEditGroupErrorComponent,
    CategoryAreaDialogComponent,
    CategoryDeleteDialogComponent,
    CategoryUnitDialogComponent,
    CategoryErorCodeDialogComponent
  ],
  declarations: [
    CategoryComponent,
    CategoryAreaComponent,
    CategoryUnitComponent,
    CategoryGroupErrorComponent,
    CategoryErrorCodeComponent,
    AddEditGroupErrorComponent,
    CategoryAreaDialogComponent,
    CategoryDeleteDialogComponent,
    CategoryUnitDialogComponent,
    CategoryErorCodeDialogComponent,
  ],
})
export class CategoryModule {
}
