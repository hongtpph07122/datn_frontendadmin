export class CategoryModel {
  id: string;
  appType: string;
  appCode: string;
  appName: string;
  parentCode?: string;
  parentName?: string;
  status: string;
  extra: string;
  sort: string;
  extraObj?: any;
  createDate: string;
  createBy: string;
  updateDate: string;
  updateBy: string;
}
