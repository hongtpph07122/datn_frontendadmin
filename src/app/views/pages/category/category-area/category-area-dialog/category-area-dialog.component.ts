import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../model/category';
import {SUCCESS_RESPONSE} from '../../../../../core/constants/app.constants';
import {CategoryService} from '../../category.service';

@Component({
  selector: 'kt-category-area-dialog',
  templateUrl: './category-area-dialog.component.html',
  styleUrls: ['./category-area-dialog.component.scss']
})
export class CategoryAreaDialogComponent implements OnInit {
  areaForm: FormGroup;
  categoryModelData: CategoryModel = new CategoryModel();
  hasFormErrors = false;
  viewLoading = false;

  constructor(
    public dialogRef: MatDialogRef<CategoryAreaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryModelData = this.data.category;
    this.creatForm();
  }

  creatForm(){
    this.areaForm = this.fb.group({
      id: [this.categoryModelData.id],
      appCode: [this.categoryModelData.appCode, Validators.compose([Validators.required])],
      appName: [this.categoryModelData.appName,Validators.compose([Validators.required])],
      active: [this.categoryModelData.status,Validators.compose([Validators.required])]
    });
  }
  getTitle(): string {
    if (this.categoryModelData.id) {
      return 'Cập nhật khu vực';
    }
    return 'Thêm mới khu vực';
  }                   

  get f() {
    return this.areaForm.controls;
  }

  /**
   * On Submit
   */
  onSubmit() {
    const controls = this.areaForm.controls;
    /** check form */
    if(this.areaForm.invalid){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const categoryModel = this.prepareCustomer();
    if (categoryModel.id) {
      this.updateCategory(categoryModel);
    } else {
      this.createCategory(categoryModel);
    }
  }
  prepareCustomer(): CategoryModel {
    const category = new CategoryModel();
    category.appType = 'DM_KHU_VUC';
    category.id = this.f.id.value;
    category.appCode = this.f.appCode.value.trim();
    category.appName = this.f.appName.value.trim();
    category.status = this.f.active.value;
    return category;
  }
  public updateCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.editAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Cap nhat thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  public createCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.createAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Them moi thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }

}
