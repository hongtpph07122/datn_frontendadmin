import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {CategoryAreaDialogComponent} from './category-area-dialog/category-area-dialog.component';
import {CategoryModel} from '../model/category';
import {CategoryService} from '../category.service';
import {CategoryDeleteDialogComponent} from '../category-delete-dialog/category-delete-dialog.component';

@Component({
  selector: 'kt-category-area',
  templateUrl: './category-area.component.html',
  styleUrls: ['./category-area.component.scss']
})
export class CategoryAreaComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'appCode', 'appName', 'status', 'action'];
  dataSource = new MatTableDataSource<CategoryModel>([]);
  isLoading: boolean = false;
  categoryForm: FormGroup;
  appType = 'DM_KHU_VUC';
  appCode = '';
  appName = '';
  status = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private categoryService: CategoryService,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
  }

  deleteCategoryArea(category: CategoryModel) {
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: category,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });

  }
  editCategoryArea(value){
    const categoryInt = value;
    const dialogRef = this.dialog.open(CategoryAreaDialogComponent, {
      data: {
        action: 'Edit',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }

  addNew(){
    const categoryInt = new CategoryModel();
    categoryInt.status = 'A';
    const dialogRef = this.dialog.open(CategoryAreaDialogComponent, {
      data: {
        action: 'CREATE',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });

  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
    this.isLoading = true;
    this.categoryService.getAllAppParam(this.appType,this.appCode,this.appName,this.status).subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }

  reloadTable(data) {
    this.isLoading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.ref.detectChanges();
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

}
