import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CmResponseModel} from '../../../core/response/cm-response.model';
import {CategoryModel} from './model/category';

@Injectable({providedIn: 'root'})
export class CategoryService {
  private BASE_API_URL = `${environment.GATEWAY_URL}api/app-params/`;

  constructor(private http: HttpClient) {
  }

  getAllAppParam(appType: string, appCode: string, appName: string, status: string): Observable<CmResponseModel<CategoryModel[]>> {
    return this.http.get<CmResponseModel<CategoryModel[]>>(`${this.BASE_API_URL}get-by-type-code-name-status?appType=${appType}&appCode=${appCode}&appName=${appName}&status=${status}`);
  }

  createAppParam(param: CategoryModel): Observable<CmResponseModel<CategoryModel>> {
    return this.http.post<CmResponseModel<CategoryModel>>(`${this.BASE_API_URL}create`, param);
  }

  editAppParam(param: CategoryModel): Observable<CmResponseModel<CategoryModel>> {
    return this.http.post<CmResponseModel<CategoryModel>>(`${this.BASE_API_URL}update`, param);
  }

  switchStateAppParam(appType: string, appCode: string): Observable<CmResponseModel<CategoryModel>> {
    return this.http.delete<CmResponseModel<CategoryModel>>(`${this.BASE_API_URL}delete?appType=${appType}&appCode=${appCode}`);
  }

  getAllUnit(appCode: string, appName: string, parentCode: string, status: string, email: string): Observable<CmResponseModel<CategoryModel[]>> {
    return this.http.get<CmResponseModel<CategoryModel[]>>(`${this.BASE_API_URL}get-data-customize-for-unit?appCode=${appCode}&appName=${appName}&parentCode=${parentCode}&status=${status}&email=${email}`);
  }

  getAllErrorCode(appCode: string, appName: string, parentCode: string, status: string, riskLevel: string): Observable<CmResponseModel<CategoryModel[]>> {
    return this.http.get<CmResponseModel<CategoryModel[]>>(`${this.BASE_API_URL}get-data-customize-for-error-code?appCode=${appCode}&appName=${appName}&parentCode=${parentCode}&status=${status}&riskLevel=${riskLevel}`);
  }
}
