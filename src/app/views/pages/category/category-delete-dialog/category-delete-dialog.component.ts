import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../category.service';

@Component({
  selector: 'kt-category-delete-dialog',
  templateUrl: './category-delete-dialog.component.html',
  styleUrls: ['./category-delete-dialog.component.scss']
})
export class CategoryDeleteDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<CategoryDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: CategoryService
  ) {}

  ngOnInit(): void {
  }

  changeStatusCategory() {
    this.service.switchStateAppParam(this.data.appType,this.data.deleteData.appCode).subscribe();
    this.dialogRef.close('ok');
    alert('Xóa thành công :D')
  }
}
