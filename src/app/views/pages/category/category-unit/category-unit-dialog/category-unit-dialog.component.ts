import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../model/category';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../category.service';
import {SUCCESS_RESPONSE} from '../../../../../core/constants/app.constants';

@Component({
  selector: 'kt-category-unit-dialog',
  templateUrl: './category-unit-dialog.component.html',
  styleUrls: ['./category-unit-dialog.component.scss']
})
export class CategoryUnitDialogComponent implements OnInit {

  unitForm: FormGroup;
  categoryModelData: CategoryModel = new CategoryModel();
  hasFormErrors = false;
  viewLoading = false;
  dataArea: CategoryModel[] = [];
  titleButton = ''

  constructor(
    public dialogRef: MatDialogRef<CategoryUnitDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.categoryModelData = this.data.category;
    if(!this.categoryModelData.id) {
      this.categoryModelData.extraObj = {
        email: '',
        employee: ''
      };
      this.categoryModelData.parentCode = '';
      this.titleButton = 'Thêm mới';
    }else {
      this.titleButton = 'Cập nhật';
    }
    this.creatForm();
    this.loadArea();
  }

  creatForm(){
    this.unitForm = this.fb.group({
      id: [this.categoryModelData.id],
      appCode: [this.categoryModelData.appCode, Validators.compose([Validators.required])],
      appName: [this.categoryModelData.appName,Validators.compose([Validators.required])],
      active: [this.categoryModelData.status,Validators.compose([Validators.required])],
      nameEmployee: [this.categoryModelData.extraObj.employee],
      email: [this.categoryModelData.extraObj.email],
      area: [this.categoryModelData.parentCode],
      areaName: [this.categoryModelData.parentName]
    });
  }
  getTitle(): string {
    if (this.categoryModelData.id) {
      return 'Cập nhật khu vực';
    }
    return 'Thêm mới khu vực';
  }

  get f() {
    return this.unitForm.controls;
  }

  /**
   * On Submit
   */
  onSubmit() {
    const controls = this.unitForm.controls;
    /** check form */
    if(this.unitForm.invalid){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const categoryModel = this.prepareCustomer();
    if (categoryModel.id) {
      this.updateCategory(categoryModel);
    } else {
      this.createCategory(categoryModel);
    }
  }
  prepareCustomer(): CategoryModel {
    const category = new CategoryModel();
    category.appType = 'DM_DON_VI';
    category.id = this.f.id.value;
    category.appCode = this.f.appCode.value.trim();
    category.appName = this.f.appName.value.trim();
    category.status = this.f.active.value;
    category.parentCode = this.f.area.value;
    const index = this.dataArea.findIndex(item => item.appCode === category.parentCode);
    category.parentName = this.dataArea[index].appName;
    category.extraObj = {
      email: this.f.email.value,
      employee: this.f.nameEmployee.value
    }
    return category;
  }
  public updateCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.editAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Cap nhat thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  public createCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.createAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Them moi thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  loadArea(){
    this.categoryService.getAllAppParam('DM_KHU_VUC','','','').subscribe(response => {
      this.dataArea = response.data;
      this.ref.detectChanges();
    });
  }

}
