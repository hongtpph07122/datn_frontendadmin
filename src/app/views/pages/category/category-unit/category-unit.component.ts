import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../model/category';
import {MatPaginator} from '@angular/material/paginator';
import {CategoryService} from '../category.service';
import {CategoryAreaDialogComponent} from '../category-area/category-area-dialog/category-area-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {CategoryUnitDialogComponent} from './category-unit-dialog/category-unit-dialog.component';
import {CategoryDeleteDialogComponent} from '../category-delete-dialog/category-delete-dialog.component';
@Component({
  selector: 'kt-category-unit',
  templateUrl: './category-unit.component.html',
  styleUrls: ['./category-unit.component.scss']
})
export class CategoryUnitComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'appCode', 'appName','parentName','email', 'action'];
  isLoading = false;
  categoryForm: FormGroup;
  dataSource = new MatTableDataSource<CategoryModel>([]);
  dataArea: CategoryModel[] = [];
  appType = 'DM_DON_VI';
  appCode = '';
  appName = '';
  status = '';
  parentCode = '';
  email = '';


  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private categoryService: CategoryService,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.loadArea();
    this.getDataTable();
  }

  editCategoryUnit(value){
    const categoryInt = value;
    const dialogRef = this.dialog.open(CategoryUnitDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  deleteCategoryUnit(value: CategoryModel){
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: value,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }
  addNew(){
    const categoryInt = new CategoryModel();
    categoryInt.status = 'A';
    const dialogRef = this.dialog.open(CategoryUnitDialogComponent, {
      data: {
        action: 'CREATE',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeUnit: [''],
      nameUnit: [''],
      codeArea: [''],
      email: [''],
    });
  }
  find(){
    this.appCode = this.categoryForm.value.codeUnit.replace(/(\s\s+| )/g, ' ').trim();;
    this.appName = this.categoryForm.value.nameUnit.replace(/(\s\s+| )/g, ' ').trim();;
    this.parentCode = this.categoryForm.value.codeArea;
    this.email = this.categoryForm.value.email.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }
  loadArea(){
    this.categoryService.getAllAppParam('DM_KHU_VUC','','','').subscribe(response => {
      this.dataArea = response.data;
      this.ref.detectChanges();
    });
  }
  public getDataTable(){
    this.isLoading = true;
    this.categoryService.getAllUnit(this.appCode,this.appName,this.parentCode,this.status,this.email).subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }
  reloadTable(data) {
    this.isLoading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.ref.detectChanges();
  }
}
