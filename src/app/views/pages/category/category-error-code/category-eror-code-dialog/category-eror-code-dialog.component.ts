import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../model/category';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../category.service';
import {SUCCESS_RESPONSE} from '../../../../../core/constants/app.constants';

@Component({
  selector: 'kt-category-eror-code-dialog',
  templateUrl: './category-eror-code-dialog.component.html',
  styleUrls: ['./category-eror-code-dialog.component.scss']
})
export class CategoryErorCodeDialogComponent implements OnInit {
  errorForm: FormGroup;
  categoryModelData: CategoryModel = new CategoryModel();
  hasFormErrors = false;
  viewLoading = false;
  dataError: CategoryModel[] = [];
  titleButton = '';

  constructor(
    public dialogRef: MatDialogRef<CategoryErorCodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.categoryModelData = this.data.category;
    if(!this.categoryModelData.id) {
      this.categoryModelData.extraObj = {
        note: '',
        riskLevel: ''
      };
      this.categoryModelData.parentCode = '';
      this.titleButton = 'Thêm mới';
    } else {
      this.titleButton = 'Cập nhật';
    }
    this.creatForm();
    this.loadError();
  }

  creatForm(){
    this.errorForm = this.fb.group({
      id: [this.categoryModelData.id],
      appCode: [this.categoryModelData.appCode, Validators.compose([Validators.required])],
      appName: [this.categoryModelData.appName,Validators.compose([Validators.required])],
      parentCode:[this.categoryModelData.parentCode],
      active: [this.categoryModelData.status,Validators.compose([Validators.required])],
      note: [this.categoryModelData.extraObj.note],
      riskLevel: [this.categoryModelData.extraObj.riskLevel]
    });
  }
  getTitle(): string {
    if (this.categoryModelData.id) {
      return 'Cập nhật khu vực';
    }
    return 'Thêm mới khu vực';
  }

  get f() {
    return this.errorForm.controls;
  }

  /**
   * On Submit
   */
  onSubmit() {
    const controls = this.errorForm.controls;
    /** check form */
    if(this.errorForm.invalid){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const categoryModel = this.prepareCustomer();
    if (categoryModel.id) {
      this.updateCategory(categoryModel);
    } else {
      this.createCategory(categoryModel);
    }
  }
  prepareCustomer(): CategoryModel {
    const category = new CategoryModel();
    category.appType = 'DM_MA_LOI';
    category.id = this.f.id.value;
    category.appCode = this.f.appCode.value.trim();
    category.appName = this.f.appName.value.trim();
    category.status = this.f.active.value.trim();
    category.parentCode = this.f.parentCode.value;
    const index = this.dataError.findIndex(item => item.appCode === category.parentCode);
    if(index>=0){
    category.parentName = this.dataError[index].appName;
    }
    category.extraObj = {
      note: this.f.note.value,
      riskLevel: this.f.riskLevel.value
    }
    return category;
  }
  public updateCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.editAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Cap nhat thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  public createCategory(category: CategoryModel){
    this.viewLoading = true;
    this.categoryService.createAppParam(category).subscribe(
      response => {
        this.viewLoading = false;
        if (response.code == SUCCESS_RESPONSE) {
          alert('Them moi thanh cong');
          this.dialogRef.close(category);
        } else {
          alert(response.message);
        }
      },
      error => {
        this.viewLoading = false;
        console.log(error);
      });
  }
  loadError(){
    this.categoryService.getAllAppParam('DM_NHOM_MA_LOI','','','').subscribe(response => {
      this.dataError = response.data;
      this.ref.detectChanges();
    });
  }
}
