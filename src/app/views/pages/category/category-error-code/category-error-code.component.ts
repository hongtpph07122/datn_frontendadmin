import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CategoryService } from '../category.service';
import { MatTableDataSource } from '@angular/material/table';
import { CategoryModel } from '../model/category';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { CategoryUnitDialogComponent } from '../category-unit/category-unit-dialog/category-unit-dialog.component';
import { CategoryErorCodeDialogComponent } from './category-eror-code-dialog/category-eror-code-dialog.component';
import { CategoryDeleteDialogComponent } from '../category-delete-dialog/category-delete-dialog.component';

@Component({
  selector: 'kt-category-error-code',
  templateUrl: './category-error-code.component.html',
  styleUrls: ['./category-error-code.component.scss']
})
export class CategoryErrorCodeComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'appCode', 'appName', 'parentName', 'riskLevel', 'note', 'status', 'action'];
  isLoading = false;
  formGroup: FormGroup;
  dataSource = new MatTableDataSource<CategoryModel>([]);
  dataErrorGroup: CategoryModel[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  appType = 'DM_MA_LOI';
  appCode = '';
  appName = '';
  status = '';
  parentCode = '';
  riskLevel = '';

  constructor(
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private ref: ChangeDetectorRef,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getAllGroupError();
    this.getDataTable();
    this.createForm();
  }

  public getDataTable() {
    this.isLoading = true;
    this.categoryService.getAllErrorCode(this.appCode, this.appName, this.parentCode, this.status, this.riskLevel).subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }

  editCategoryUnit(value) {
    const categoryInt = value;
    const dialogRef = this.dialog.open(CategoryErorCodeDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  deleteCategoryUnit(value: CategoryModel) {
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: value,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }
  addNew() {
    const categoryInt = new CategoryModel();
    categoryInt.status = 'A';
    const dialogRef = this.dialog.open(CategoryErorCodeDialogComponent, {
      data: {
        action: 'CREATE',
        category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  createForm() {
    this.formGroup = this.fb.group({
      appCode: [''],
      appName: [''],
      parentCode: [''],
      rickLevel: [''],
      status: ['']
    });
  }

  searchErrorCode() {
    this.appCode = this.formGroup.get('appCode').value.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.formGroup.get('appName').value.replace(/(\s\s+| )/g, ' ').trim();
    this.parentCode = this.formGroup.get('parentCode').value.replace(/(\s\s+| )/g, ' ').trim();
    this.status = this.formGroup.get('status').value.replace(/(\s\s+| )/g, ' ').trim();
    this.riskLevel = this.formGroup.get('rickLevel').value.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }


  getAllGroupError() {
    this.categoryService.getAllAppParam('DM_NHOM_MA_LOI', '', '', '').subscribe(response => {
      this.dataErrorGroup = response.data;
    }, error => {
      console.log(error);
    });
  }

  private reloadTable(data: CategoryModel[]) {
    this.isLoading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.ref.detectChanges();
  }
}
