import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {CategoryModel} from '../model/category';
import {MatDialog} from '@angular/material/dialog';
import {AddEditGroupErrorComponent} from './add-edit-group-error/add-edit-group-error.component';
import {CategoryService} from '../category.service';
import {FormControl, FormGroup} from '@angular/forms';
import {CategoryDeleteDialogComponent} from '../category-delete-dialog/category-delete-dialog.component';

@Component({
  selector: 'kt-category-group-error',
  templateUrl: './category-group-error.component.html',
  styleUrls: ['./category-group-error.component.scss']
})
export class CategoryGroupErrorComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'appCode', 'appName', 'parentCode', 'status', 'action'];
  dataSource = new MatTableDataSource<CategoryModel>([]);
  loading = false; valueAppCode = ''; valueAppName = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  formGroup: FormGroup;

  constructor(
    private dialog: MatDialog,
    private service: CategoryService,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.getDataTable();
    this.formGroup = new FormGroup({
      appCode: new FormControl(''),
      appName: new FormControl('')
    })
  }

  addGroupError() {
    const addInitData = new CategoryModel();
    addInitData.status = 'A';
    const dialogRef = this.dialog.open(AddEditGroupErrorComponent, {
      data: {
        data: addInitData,
        action: 'CREATE'
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }

  editGroupError(error: CategoryModel) {
    const editInitData = this.dialog.open(AddEditGroupErrorComponent, {
      data: {
        data: error,
        action: 'UPDATE'
      }
    });
    editInitData.afterClosed().subscribe(dataSub => {
      if (dataSub) {
        this.getDataTable();
      }
    });
  }

  private getDataTable() {
    this.loading = true;
    this.service.getAllAppParam('DM_NHOM_MA_LOI',this.valueAppCode,this.valueAppName,'').subscribe(response => {
      this.reloadTable(response.data);
    }, error => {
      console.log(error);
    });
  }

  reloadTable(data) {
    this.loading = false;
    this.dataSource = new MatTableDataSource<CategoryModel>(data);
    this.dataSource.paginator = this.paginator;
    this.changeDetectorRef.detectChanges();
  }

  searchGroupError() {
    this.valueAppCode = this.formGroup.get('appCode').value.replace(/(\s\s+| )/g, ' ').trim();
    this.valueAppName = this.formGroup.get('appName').value.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  deleteGroupError(error: CategoryModel) {
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: error,
        appType: 'DM_NHOM_MA_LOI'
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
    console.log(deleteInitData.afterClosed().subscribe());
  }
}
