import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'kt-receipt-vouchers',
  templateUrl: './receipt-vouchers.component.html',
  styleUrls: ['./receipt-vouchers.component.scss']
})
export class ReceiptVouchersComponent implements OnInit {
  displayedColumns: string[] = ['stt','action', 'unit','dateDelivery', 'startDate','statusReceive','userReceive', 'numberDocument', 'numberDayLate','extend'];
  loading = false;
  dataArea = []
  dataSource = [
    { stt: 1, dateDelivery: 'Hydrogen', startDate: 1.0079, 
    unit: 'H', numberDocument: 1 },

  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  router: any;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  remove() {

  }

}
