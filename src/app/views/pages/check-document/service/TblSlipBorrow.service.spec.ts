/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TblSlipBorrowService } from './TblSlipBorrow.service';

describe('Service: TblSlipBorrow', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TblSlipBorrowService]
    });
  });

  it('should ...', inject([TblSlipBorrowService], (service: TblSlipBorrowService) => {
    expect(service).toBeTruthy();
  }));
});
