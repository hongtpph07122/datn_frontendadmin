import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TblSlipBorrow } from './../model/TblSlipBorrow';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckDocumentServiceService {
  api= 'http://localhost:8080/api/dataman';
constructor(
  private http: HttpClient
) { }
  public doSearchSlipBorrow(data: TblSlipBorrow): Observable<any>{
    return this.http.post<any>(this.api + '/doSearchSlipBorrow', data);
  }
  public findAllBySlipBorrowId(id: number): Observable<any>{
    return this.http.get<any>(this.api + `/findAllBySlipBorrowId/${id}`);
  }
  // phê duyệt
  public approval(data: TblSlipBorrow): Observable<TblSlipBorrow>{
    return this.http.post<TblSlipBorrow>(this.api + `/updateSlipBorrow` , data);
   }
}
