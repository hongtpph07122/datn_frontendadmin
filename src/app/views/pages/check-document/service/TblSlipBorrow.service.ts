import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TblSlipBorrow } from '../model/TblSlipBorrow';

@Injectable({
  providedIn: 'root'
})
export class TblSlipBorrowService {
  api = 'http://localhost:8080/api/dataman';
  constructor(
    private http: HttpClient
  ) { }
  public doSearchSlipBorrow(data: TblSlipBorrow): Observable<any> {
    return this.http.post<any>(this.api + '/doSearchSlipBorrow', data);
  }
  // public doSearchSlipBorrow(data: TblSlipBorrow): Observable<any> {
  //   return this.http.post<any>(this.api + '/doSearchSlipBorrow', data);
  // }
  // public doSearchSlipBorrow(data: TblSlipBorrow): Observable<any> {
  //   return this.http.post<any>(this.api + '/doSearchSlipBorrow', data);
  // }
  // public doSearchSlipBorrow(data: TblSlipBorrow): Observable<any> {
  //   return this.http.post<any>(this.api + '/doSearchSlipBorrow', data);
  // }
}
