/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CheckDocumentServiceService } from './check-document-service.service';

describe('Service: CheckDocumentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckDocumentServiceService]
    });
  });

  it('should ...', inject([CheckDocumentServiceService], (service: CheckDocumentServiceService) => {
    expect(service).toBeTruthy();
  }));
});
