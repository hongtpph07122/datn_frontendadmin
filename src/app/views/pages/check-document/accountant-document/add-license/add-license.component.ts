import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
export interface PeriodicElement {
  stt: number;
  dknq: string;
  refcode: string;
  ptks: string;
  ngaygiaingan: string;
  ngaydenhan: string;

}
const ELEMENT_DATA: PeriodicElement[] = [
  { stt: 1, dknq: '23/10/2020', refcode: '25/10/2020', ptks: '1', ngaygiaingan: 'CTKT', ngaydenhan: 'Ghi chú số 1' },
  { stt: 2, dknq: '23/10/2020', refcode: '25/10/2020', ptks: '2', ngaygiaingan: 'CTKT', ngaydenhan: 'Ghi chú số 2' },
  { stt: 3, dknq: '23/10/2020', refcode: '25/10/2020', ptks: '3', ngaygiaingan: 'CTKT', ngaydenhan: 'Ghi chú số 3'},

];
@Component({
  selector: 'kt-add-license',
  templateUrl: './add-license.component.html',
  styleUrls: ['./add-license.component.scss']
})
export class AddLicenseComponent implements OnInit {
  areaForm: FormGroup;
  displayedColumns: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan','action'];
  dataSource = ELEMENT_DATA;
  panelOpenState = false;
  constructor() { }

  ngOnInit() {
  }

}
