import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'kt-accountant-delete',
  templateUrl: './accountant-delete.component.html',
  styleUrls: ['./accountant-delete.component.scss']
})
export class AccountantDeleteComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<AccountantDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
  }
  changeStatusCategory() {
    this.dialogRef.close('ok');
    alert('Xóa thành công :D')
  }
}
