import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'kt-accountant-dialog',
  templateUrl: './accountant-dialog.component.html',
  styleUrls: ['./accountant-dialog.component.scss']
})
export class AccountantDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AccountantDialogComponent>,
    @Inject(MAT_DIALOG_DATA)  public data: any,
  ) { }

  ngOnInit() {
  }

}
