import { AccountantDeleteComponent } from './accountant-delete/accountant-delete.component';

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import { AccountantDialogComponent } from './accountant-dialog/accountant-dialog.component';
export interface PeriodicElement {
  stt: number;
  dknq: string;
  refcode: string;
  ptks: string;
  ngaygiaingan: string;
  ngaydenhan: string;
  tungay: string;
  ngayden: string;
  canbo: string;
  ngaylu: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { stt: 1, dknq: 'Trần Duy Hưng', refcode: 'Hà Nội 2', ptks: 'Miền Bắc', ngaygiaingan: '25/10/2020', ngaydenhan: '2', tungay: '23/10/2020', ngayden: '30/10/2020', canbo: 'Datlv', ngaylu: '26/10/2020' },
  { stt: 2, dknq: 'Trần Duy Hưng', refcode: 'Hà Nội 2', ptks: 'Miền Bắc', ngaygiaingan: '25/10/2020', ngaydenhan: '2', tungay: '23/10/2020', ngayden: '30/10/2020', canbo: 'Datlv', ngaylu: '26/10/2020' },
  {stt: 3, dknq: 'Trần Duy Hưng', refcode: 'Hà Nội 2', ptks: 'Miền Bắc',ngaygiaingan: '25/10/2020',ngaydenhan:'2',tungay:'23/10/2020',ngayden:'30/10/2020',canbo:'Datlv',ngaylu:'26/10/2020'},

];
@Component({
  selector: 'kt-accountant-document',
  templateUrl: './accountant-document.component.html',
  styleUrls: ['./accountant-document.component.scss']
})
export class AccountantDocumentComponent implements OnInit {
  areaForm: FormGroup;
  displayedColumns: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan','tungay','ngayden','canbo','ngaylu','action'];
  dataSource = ELEMENT_DATA;
  panelOpenState = false;

  constructor(public dialog: MatDialog) { }
  ngOnInit() {
  }
  addNew(){


    const dialogRef = this.dialog.open(AccountantDialogComponent, {
      data: {
        action: 'CREATE',
        titel: '',
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
      }
    });

  }
  deleteCategoryArea() {
    const deleteInitData = this.dialog.open(AccountantDeleteComponent, {
      data: {

      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {

      }
    });

  }
}
