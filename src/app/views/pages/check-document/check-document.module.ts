
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryDocumentComponent } from './delivery-document/delivery-document.component';
import { RouterModule, Routes } from '@angular/router';
import { CheckDocumentComponent } from './check-document.component';
import { PortletModule } from '../../partials/content/general/portlet/portlet.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { DropdownTreeviewSelectModule } from 'src/app/core/_base/tree-view/dropdown-treeview-select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CoreModule } from 'src/app/core/core.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {BorrowDocumentComponent} from './borrow-document/borrow-document.component';
import {BorrowDocumentDialogComponent} from './borrow-document/borrow-document-dialog/borrow-document-dialog.component';
import { BorrowDocumentBrowserDialogComponent } from './borrow-document/borrow-document-browser-dialog/borrow-document-browser-dialog.component';
import {ControlLicenseModule} from './control-license/control-license.module';
import {TimekeepingDetailsComponent} from './control-license/timekeeping-details/timekeeping-details.component';
import {TimekeepingDetailsDialogComponent} from './control-license/timekeeping-details/timekeeping-details-dialog/timekeeping-details-dialog.component';
import {ResultPropertyUpdateComponent} from '../disbursement/result-property-update/result-property-update.component';
import { AccountantDocumentComponent } from './accountant-document/accountant-document.component';
import { AddLicenseComponent } from './accountant-document/add-license/add-license.component';
import { AccountantDialogComponent } from './accountant-document/accountant-dialog/accountant-dialog.component';
import { CheckLicenseComponent } from './accountant-document/check-license/check-license.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { AccountantDeleteComponent } from './accountant-document/accountant-delete/accountant-delete.component';
import { DeliveryDocumentModule } from './delivery-document/delivery-document.module';

import { ManageBorrowDocumentComponent } from './manage-borrow-document/manage-borrow-document.component';
import { ManageBorrowDocumentBrowserDialogComponent } from './manage-borrow-document/manage-borrow-document-browser-dialog/manage-borrow-document-browser-dialog.component';
import { ManageBorrowDocumentDialogComponent } from './manage-borrow-document/manage-borrow-document-dialog/manage-borrow-document-dialog.component';
import { CheckPasswordComponent } from './manage-borrow-document/manage-borrow-document-browser-dialog/check-password/check-password.component';

import { ReceiptVouchersComponent } from './receipt-vouchers/receipt-vouchers.component';
import { DetailVouchersComponent } from './detail-vouchers/detail-vouchers.component';
import { UploadVoucherComponent } from './upload-voucher/upload-voucher.component';
import { PopupVoucherListComponent } from './upload-voucher/popup-voucher-list/popup-voucher-list.component';
import { PopupNumberEntriesComponent } from './upload-voucher/popup-number-entries/popup-number-entries.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PopupCreateComponent } from './upload-voucher/popup-create/popup-create.component';

const routes: Routes = [
  {
    path: '',
    component: CheckDocumentComponent,
    // canActivate: [UserRoleAccessService],
    children: [
      {
        path: '',
        redirectTo: 'delivery',
        pathMatch: 'full'
      },
      {
        path: 'delivery',
        component: DeliveryDocumentComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/delivery'
        }
      },
      {
        path: 'receipt-vouchers',
        component: ReceiptVouchersComponent,
        data: {
          configPath: '/check-document/receipt-vouchers'
        }
      },
      {
        path: 'upload-voucher',
        component: UploadVoucherComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/upload-voucher'
        }
      },
      {
        path: 'borrow',
        component: BorrowDocumentComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/borrow'
        }
      },
      {
        path: 'manage-borrow',
        component: ManageBorrowDocumentComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/borrow'
        }
      },
      {
        path: 'accountant',
        component: AccountantDocumentComponent,
        data: {
          configPath: '/check-document/accountant',
        },
      },
      {
        path: 'add-license',
        component: AddLicenseComponent,
          //  canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/add-license',
        },
      },
      {
        path: 'check-license',
        component: CheckLicenseComponent,
        //   canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/check-license',
        },
      },
      {
        path: 'detail-vouchers',
        component: DetailVouchersComponent,
        //   canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/detail-vouchers',
        },
      },
    ]
  }
];

@NgModule({
  declarations: [DeliveryDocumentComponent, BorrowDocumentComponent,
    BorrowDocumentDialogComponent,
    BorrowDocumentBrowserDialogComponent, DeliveryDocumentComponent,
    TimekeepingDetailsComponent,
    TimekeepingDetailsDialogComponent,
    DeliveryDocumentComponent,
    AccountantDocumentComponent,
    AccountantDialogComponent,
    AccountantDialogComponent,
    AddLicenseComponent,
    AccountantDeleteComponent,
    CheckLicenseComponent,
    ManageBorrowDocumentComponent,
    ManageBorrowDocumentBrowserDialogComponent,
    ManageBorrowDocumentDialogComponent,
    CheckPasswordComponent,
    ReceiptVouchersComponent,
    DetailVouchersComponent,
    UploadVoucherComponent,
    PopupVoucherListComponent,
    PopupNumberEntriesComponent,
    PopupCreateComponent
  ],

  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    PortletModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    CoreModule,
    DropdownTreeviewSelectModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    ControlLicenseModule,
    MatTableModule,
    MatExpansionModule,
    DeliveryDocumentModule,
    NgbModule
  ], entryComponents: [
    TimekeepingDetailsDialogComponent,
    BorrowDocumentDialogComponent,
    BorrowDocumentBrowserDialogComponent,
    AccountantDialogComponent,
    AccountantDeleteComponent,
    ManageBorrowDocumentDialogComponent,
    ManageBorrowDocumentBrowserDialogComponent,
    CheckPasswordComponent,

    PopupVoucherListComponent,
    PopupNumberEntriesComponent,
    PopupCreateComponent

  ]

})
export class CheckDocumentModule { }

