import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckDocumentComponent } from '../check-document.component';
import { RouterModule, Routes } from '@angular/router';
import { DeliveryAddEditComponent } from './delivery-add-edit/delivery-add-edit.component';
import { DeliveryBrowserComponent } from './delivery-browser/delivery-browser.component';
import { DeliveryRemoveComponent } from './delivery-remove/delivery-remove.component';
import { DeliveryInfoComponent } from './delivery-info/delivery-info.component';
import { PortletModule } from 'src/app/views/partials/content/general/portlet/portlet.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { CoreModule } from 'src/app/core/core.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path: '',
    component: CheckDocumentComponent,
    children: [
      {
        path: 'delivery-add-edit',
        component: DeliveryAddEditComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/delivery-add-edit'
        }
      },
      {
        path: 'delivery-browser',
        component: DeliveryBrowserComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/delivery-browser'
        }
      },
      {
        path: 'delivery-info',
        component: DeliveryInfoComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/delivery-info'
        }
      },
    ]
  }
];
@NgModule({
  declarations: [ DeliveryInfoComponent,
    DeliveryRemoveComponent,
    DeliveryBrowserComponent,
    DeliveryAddEditComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    PortletModule,
    MatButtonModule,
    MatTableModule,
    CoreModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatPaginatorModule,
    FormsModule ,
    ReactiveFormsModule,
  ],
  entryComponents: [
    DeliveryAddEditComponent
  ]
})
export class DeliveryDocumentModule { }
