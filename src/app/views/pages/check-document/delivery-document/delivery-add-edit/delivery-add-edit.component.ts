import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TblSlipTransModel } from '../../model/TblSlipTrans.model';
import { DeliveryDocumentService } from '../delivery-document.service';
@Component({
  selector: 'kt-delivery-add-edit',
  templateUrl: './delivery-add-edit.component.html',
  styleUrls: ['./delivery-add-edit.component.scss']
})
export class DeliveryAddEditComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'startDate', 'endDate', 'numberDocument', 'type', 'note', 'action'];
  loading = false;
  dataArea = [];
  dataDx = ['Có', 'Không'];
  listModeDelivery = ['Nộp trực tiếp', 'Nộp qua đơn vị vận chuyển'];
  dataTrans = ['đơn vị 1', 'đơn vị 2']
  listType=["kttk"]
  dataSource: any = [
    { stt: 1, dateDelivery: '1', slipFromDate: '12', slipToDate: 'H', slipType: 1, numberDocument: '1', note: 'a' },
  ];
  a: any
  get f() {
    return this.deliveryForm.controls;
  }
  constructor(private fb: FormBuilder, private ref: ChangeDetectorRef, private deliveryDocumentService: DeliveryDocumentService,) { }
  TblSlipTransModel: TblSlipTransModel = new TblSlipTransModel();
  ngOnInit(): void {
  }
  deliveryForm = this.fb.group({
    modeDelivery: [''],
    unitTrans: [''],
    proposalRenewal: [''],
    extendReason: [''],
    extendStatus: [''],
    proExtendDay: [''],
    slipFromDate: [''],
    slipToDate: [''],
    fileCount: [''],
    slipType: [''],
    note: ['']



  })
  save() {
    let data = {
      branchCode: "M02",
      transportType: this.f.modeDelivery.value,
      transportCo: this.f.unitTrans.value,
      proExtendDay:Number(this.f.proExtendDay.value),
      extendReason: this.f.extendReason.value,
      remark: this.f.extendStatus.value,
      createUsername: "Nghia",
      listSlipTransDtl: [
        {
          slipFromDate: new Date(this.f.slipFromDate.value),
          slipToDate: new Date(this.f.slipToDate.value),
          slipType: this.f.slipType.value,
          fileCount: Number(this.f.fileCount.value) ,
          remark: this.f.note.value,
          lateActual: 5,
          extendDay: 7,
          lateDay: 8,
          createUsername: "Nhan"
        }
      ]
    }

    console.log(data);

    this.deliveryDocumentService.createSlipTrans(data).subscribe((v) => console.log(v))

  }
  convertData() {

  }
  addToTable() {
    this.loading = true
    let covert = { stt: 1, slipFromDate: this.f.slipFromDate.value, slipToDate: this.f.slipToDate.value, slipType: this.f.slipType.value }
    this.dataSource.push(covert)
    this.ref.detectChanges();
    this.loading = false


  }
}
