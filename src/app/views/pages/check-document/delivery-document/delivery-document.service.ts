import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CmResponseModel } from 'src/app/core/response/cm-response.model';
import { environment } from 'src/environments/environment';
import { TblSlipTransModel } from '../model/TblSlipTrans.model';


@Injectable({
  providedIn: 'root'
})
export class DeliveryDocumentService {
  private BASE_API_URL = `${environment.GATEWAY_URL_ENTRY}api/dataman/`;

  constructor(private http: HttpClient) {
  }

  searchSlipTrans(param: any): Observable<CmResponseModel<TblSlipTransModel[]>> {
    return this.http.post<CmResponseModel<TblSlipTransModel[]>>(`${this.BASE_API_URL}searchSlipTrans`, param);
  }

  createSlipTrans(param: TblSlipTransModel): Observable<CmResponseModel<TblSlipTransModel>> {    
    return this.http.post<CmResponseModel<TblSlipTransModel>>(`${this.BASE_API_URL}createSlipTrans`, param);
  }

  // editAppParam(param: CategoryModel): Observable<CmResponseModel<CategoryModel>> {
  //   return this.http.post<CmResponseModel<CategoryModel>>(`${this.BASE_API_URL}update`, param);
  // }

  // switchStateAppParam(appType: string, appCode: string): Observable<CmResponseModel<CategoryModel>> {
  //   return this.http.delete<CmResponseModel<CategoryModel>>(`${this.BASE_API_URL}delete?appType=${appType}&appCode=${appCode}`);
  // }

  // getAllUnit(appCode: string, appName: string, parentCode: string, status: string, email: string): Observable<CmResponseModel<CategoryModel[]>> {
  //   return this.http.get<CmResponseModel<CategoryModel[]>>(`${this.BASE_API_URL}get-data-customize-for-unit?appCode=${appCode}&appName=${appName}&parentCode=${parentCode}&status=${status}&email=${email}`);
  // }

  // getAllErrorCode(appCode: string, appName: string, parentCode: string, status: string, riskLevel: string): Observable<CmResponseModel<CategoryModel[]>> {
  //   return this.http.get<CmResponseModel<CategoryModel[]>>(`${this.BASE_API_URL}get-data-customize-for-error-code?appCode=${appCode}&appName=${appName}&parentCode=${parentCode}&status=${status}&riskLevel=${riskLevel}`);
  // }
}
