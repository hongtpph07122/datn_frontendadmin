import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryBrowserComponent } from './delivery-browser.component';

describe('DeliveryBrowserComponent', () => {
  let component: DeliveryBrowserComponent;
  let fixture: ComponentFixture<DeliveryBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
