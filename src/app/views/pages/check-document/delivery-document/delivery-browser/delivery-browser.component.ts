import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-delivery-browser',
  templateUrl: './delivery-browser.component.html',
  styleUrls: ['./delivery-browser.component.scss']
})
export class DeliveryBrowserComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'startDate', 'endDate','numberDocument', 'type', 'note'];
  loading = false;
  dataArea = []
  dataSource = [
    { stt: 1,  startDate: 1.0079, userSubmit: 'H', numberDocument: 1 },

  ];
  constructor() { }

  ngOnInit(): void {
  }

}
