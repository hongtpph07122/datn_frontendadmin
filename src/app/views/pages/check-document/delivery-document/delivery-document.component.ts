import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { DeliveryDocumentService } from './delivery-document.service';
@Component({
  selector: 'kt-delivery-document',
  templateUrl: './delivery-document.component.html',
  styleUrls: ['./delivery-document.component.scss']
})
export class DeliveryDocumentComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'dateDelivery', 'startDate', 'endDate', 'userSubmit', 'UserBrowse', 'statusBrowse', 'statusReceive', 'numberDocument', 'numberDayLate', 'action'];
  loading = false;
  dataArea = []
  dataSource: any = [
    { stt: 1, dateDelivery: 'Hydrogen', startDate: 1.0079, userSubmit: 'H', numberDocument: 1 },

  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  router: any;
  constructor(public dialog: MatDialog, private fb: FormBuilder, private deliveryDocumentService: DeliveryDocumentService,) { }

  ngOnInit(): void {
    this.fetchAll()
  }
  remove() {

  }
  deliveryForm = this.fb.group({
    proposalRenewal: [''],
    prioritize: [''],
    modeDelivery: [''],
    apprUsername:['']
  })
  fetchAll() {
    let pageSize = {
      page: 1,
      pageSize: 2,
      totalRecord: null
    }
    this.deliveryDocumentService.searchSlipTrans(pageSize).subscribe((v) => {
      
      
      this.dataSource = v.data
    })
  }

}
