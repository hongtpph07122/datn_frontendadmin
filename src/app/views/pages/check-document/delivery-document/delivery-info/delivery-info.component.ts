import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-delivery-info',
  templateUrl: './delivery-info.component.html',
  styleUrls: ['./delivery-info.component.scss']
})
export class DeliveryInfoComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'startDate', 'endDate','numberDocument', 'type', 'note'];
  loading = false;
  dataArea = []
  dataSource = [
    { stt: 1,  startDate: 1.0079, userSubmit: 'H', numberDocument: 1 },

  ];
  constructor() { }

  ngOnInit(): void {
  }

}
