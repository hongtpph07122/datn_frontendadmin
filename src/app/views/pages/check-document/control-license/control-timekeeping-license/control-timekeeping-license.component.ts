import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {TimekeepingDetailsDialogComponent} from '../timekeeping-details/timekeeping-details-dialog/timekeeping-details-dialog.component';
import {EcceptTimekeepingComponent} from '../eccept-timekeeping/eccept-timekeeping.component';

@Component({
  selector: 'kt-control-timekeeping-license',
  templateUrl: './control-timekeeping-license.component.html',
  styleUrls: ['./control-timekeeping-license.component.scss']
})
export class ControlTimekeepingLicenseComponent implements OnInit {

  public form: FormGroup;
  displayedColumns: string[] = ['stt','cb','soBT','userC','ngayC','tLoi', 'kqTD', 'dttC', 'ngayGD','tkNo','tkCo','userHQ','userD','sotien','sophi'];
  dataSource = [];
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.form = new FormGroup({});
  }
  editTimeKeeping(value){

  }
  openDialog(){
    const dialogRef = this.dialog.open(TimekeepingDetailsDialogComponent, {
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  openCT(){
    const dialogRef = this.dialog.open(EcceptTimekeepingComponent, {
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  getDataTable(){

  }

}
