import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {TimekeepingDetailsDialogComponent} from '../timekeeping-details/timekeeping-details-dialog/timekeeping-details-dialog.component';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../../category/model/category';

@Component({
  selector: 'kt-timekeeping-add',
  templateUrl: './timekeeping-add.component.html',
  styleUrls: ['./timekeeping-add.component.scss']
})
export class TimekeepingAddComponent implements OnInit {

  public form: FormGroup;
  displayedColumns: string[] = ['stt','userC','ngayC','XNloi', 'nhomLoi', 'plLoi', 'ctLoi', 'mdRR','ttKP','ttCham','note','action'];
  datatest = [];
  dataSource= new MatTableDataSource<any>([]);
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog) {
    this.datatest.push({
      userC: 'datlv',
      ngayC: '23/10/2020',
      XNloi: 'Tài khoản 1',
      nhomLoi: 'Tài khoản a',
      plLoi: 'User 1',
      ctLoi: 'Lỗi ko đúng số tiền trên hệ thống',
      mdRR: 'Cao',
      ttKP: 'User 1',
      ttCham: '23/10/2020',
      note: 'Đây là ghi chú 1'
    });
  }

  ngOnInit(): void {
    this.form = new FormGroup({});
    this.dataSource = new MatTableDataSource<any>(this.datatest);
  }
  editTimeKeeping(value){
    this.openDialog();
  }
  openDialog(){
    const dialogRef = this.dialog.open(TimekeepingDetailsDialogComponent, {
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  getDataTable(){

  }

}
