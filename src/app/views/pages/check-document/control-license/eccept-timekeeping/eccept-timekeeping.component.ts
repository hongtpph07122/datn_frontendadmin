import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../../category/category.service';

@Component({
  selector: 'kt-eccept-timekeeping',
  templateUrl: './eccept-timekeeping.component.html',
  styleUrls: ['./eccept-timekeeping.component.scss']
})
export class EcceptTimekeepingComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<EcceptTimekeepingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: CategoryService
  ) {}

  ngOnInit(): void {
  }

  changeStatusCategory() {
    this.dialogRef.close('ok');
    alert('Xóa thành công :D')
  }

}
