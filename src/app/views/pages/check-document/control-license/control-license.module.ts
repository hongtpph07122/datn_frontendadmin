import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CheckDocumentComponent} from '../check-document.component';
import {DeliveryDocumentComponent} from '../delivery-document/delivery-document.component';
import {TimekeepingDetailsComponent} from './timekeeping-details/timekeeping-details.component';
import { TimekeepingAddComponent } from './timekeeping-add/timekeeping-add.component';
import {MatTableModule} from '@angular/material/table';
import {ReactiveFormsModule} from '@angular/forms';
import {PortletModule} from '../../../partials/content/general/portlet/portlet.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ControlTimekeepingLicenseComponent } from './control-timekeeping-license/control-timekeeping-license.component';
import { EcceptTimekeepingComponent } from './eccept-timekeeping/eccept-timekeeping.component';
import {MatDialogModule} from '@angular/material/dialog';


const routes: Routes = [
  {
    path: '',
    component: CheckDocumentComponent,
    children: [
      {
        path: 'timekeeping',
        component: TimekeepingDetailsComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/timekeeping'
        }
      },
      {
        path: 'timekeeping-add',
        component: TimekeepingAddComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/check-document/timekeeping-add'
        }
      },
      {
        path: 'controltime',
        component: ControlTimekeepingLicenseComponent,
        data: {
          configPath: '/check-document/controltime'
        }
      }
    ]
  }
];
@NgModule({
  declarations: [TimekeepingAddComponent, ControlTimekeepingLicenseComponent, EcceptTimekeepingComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTableModule,
    ReactiveFormsModule,
    PortletModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
  ],
  entryComponents: [
    EcceptTimekeepingComponent
  ]
})
export class ControlLicenseModule { }
