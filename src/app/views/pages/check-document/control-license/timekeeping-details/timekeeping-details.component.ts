import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {TimekeepingDetailsDialogComponent} from './timekeeping-details-dialog/timekeeping-details-dialog.component';


@Component({
  selector: 'kt-timekeeping-details',
  templateUrl: './timekeeping-details.component.html',
  styleUrls: ['./timekeeping-details.component.scss']
})
export class TimekeepingDetailsComponent implements OnInit {
  public form: FormGroup;
  displayedColumns: string[] = ['stt','userC','ngayC','XNloi', 'nhomLoi', 'plLoi', 'ctLoi', 'mdRR','ttKP','ttCham','note','action'];
  dataSource = [];
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.form = new FormGroup({});
  }
  editTimeKeeping(value){

  }
  openDialog(){
    const dialogRef = this.dialog.open(TimekeepingDetailsDialogComponent, {
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getDataTable();
      }
    });
  }
  getDataTable(){

  }


}
