import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {CategoryModel} from '../../../../category/model/category';

@Component({
  selector: 'kt-timekeeping-details-dialog',
  templateUrl: './timekeeping-details-dialog.component.html',
  styleUrls: ['./timekeeping-details-dialog.component.scss']
})
export class TimekeepingDetailsDialogComponent implements OnInit {
  timeForm: FormGroup;
  titleButton = '';
  dataError: CategoryModel[] = [];
  constructor() { }

  ngOnInit(): void {
    this.timeForm = new FormGroup({});
    this.getTitle();
  }
  getTitle(){
    this.titleButton = 'Cập nhật';
    return 'Cập nhật lỗi chứng từ'
  }
  get f() {
    return this.timeForm.controls;
  }
  onSubmit(){}
}
