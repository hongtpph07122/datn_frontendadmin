import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../category/model/category';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'kt-detail-vouchers',
  templateUrl: './detail-vouchers.component.html',
  styleUrls: ['./detail-vouchers.component.scss']
})
export class DetailVouchersComponent implements OnInit {

  dataSource = new MatTableDataSource<CategoryModel>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['stt', 'appCode', 'appName', 'parentName', 'riskLevel', 'note', 'status'];
  test:any [] =[{ stt: 1, appCode: 15/10/2020, appName:   15/10/2020, parentName: 2, riskLevel: 1, note: 'CTKT', status: 'ghichu1'}]

  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<CategoryModel>(this.test);
  }

}
