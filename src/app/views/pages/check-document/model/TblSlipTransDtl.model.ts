  export class TblSlipTransDtlModel{
    id?:number;
    slipTransId?:number;
    slipFromDate?:string;
    slipToDate?:string;
    slipType?:string;
    fileCount?:number;
    remark?:string;
    lateActual?:number;
    extendDay?:number;
    lateDay?:number;
    createUsername?:string;
    createDTM?:string;
    updtUsername?:string;
    updtDTM?:string;
  }