export class TblSlipBorrow {
  id: number;
  branchCode: string;
  expectFromDate: Date;
  expectToDate: Date;
  expectFromDateEnd: Date;
  expectToDateEnd: Date;
  reason: string;
  status: string;
  apprStatus: string;
  createUsername: string;
  createDtm: Date;;
  apprDvnvUsername: string;
  apprDvnvDtm: Date;;
  apprNvUsername: string;
  apprNvDtm: Date;;
  apprUsername: string;
  apprDtm: Date;;
  transUsername: string;
  borrowUsername: string;
  receiveId: string;
  actualFromDate: Date;;
  returnUsername: string;
  receiveUsername: string;
  actualToDate: Date;;
  updtUsername: Date;;
  updtDtm: string;
}


  