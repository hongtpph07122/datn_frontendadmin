export class TblSlipTransModel {
    id?: string;
    branchCode?:string;
    transportType?:string;
    transportCo?:string;
    proExtendDay?:number;
    extendReason?:string;
    extendDay?:number;
    remark?:string;
    statusReceive?:string;
    statusDelivery?:string;
    createUsername?:string;
    createDtmFrom?:string;
    createDtmTo?:string;
    slipFromDate?:string;
    slipToDate?:string;
    apprUsername?:string;
    apprDtm?:string;
    receiveUsername?:string;
    receiveDtm?:string;
    receiveRemark?:string;
    updtUsername?:string;
    updtDtm?:string;
    listSlipTransDtl?:any;

  
  }