import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CheckDocumentModel} from "../../model/check-document";


@Component({
  selector: 'kt-borrow-document-dialog',
  templateUrl: './borrow-document-dialog.component.html',
  styleUrls: ['./borrow-document-dialog.component.scss']
})
export class BorrowDocumentDialogComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'need', 'date', 'action'];
  borrowForm: FormGroup;
  loading:false;
  checkDocumentModelData: CheckDocumentModel = new CheckDocumentModel();
  // hasFormErrors = false;
  viewLoading = false;
  dataSurce1 = [
    {stt: 1, need: 'dd/mm/yy', date: 'dd/mm/yy', action: 'abc'},

  ];
  constructor() { }

  ngOnInit(): void {
    this.borrowForm = new FormGroup({
      appCode: new FormControl('')
    })
  }
  getTitle(){
    if (this.checkDocumentModelData.id){
      return 'Cập nhật mượn chứng từ';
    }
    return  'Thêm mới mượn chứng từ'
  }
  addNew(){

  }
  onSubmit(){

  }

}
