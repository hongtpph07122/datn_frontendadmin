import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowDocumentDialogComponent } from './borrow-document-dialog.component';

describe('BorrowDocumentDialogComponent', () => {
  let component: BorrowDocumentDialogComponent;
  let fixture: ComponentFixture<BorrowDocumentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowDocumentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowDocumentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
