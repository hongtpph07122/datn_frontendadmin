import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowDocumentComponent } from './borrow-document.component';

describe('BorrowDocumentComponent', () => {
  let component: BorrowDocumentComponent;
  let fixture: ComponentFixture<BorrowDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
