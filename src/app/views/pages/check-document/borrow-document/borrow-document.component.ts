
import { TblSlipBorrow } from './../model/TblSlipBorrow';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatDatepicker} from "@angular/material/datepicker";
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {BorrowDocumentDialogComponent} from "./borrow-document-dialog/borrow-document-dialog.component";
import {BorrowDocumentBrowserDialogComponent} from "./borrow-document-browser-dialog/borrow-document-browser-dialog.component";
import { CheckDocumentServiceService } from '../service/check-document-service.service';

import { MatTableDataSource } from '@angular/material/table';
import { TblSlipBorrowService } from '../service/TblSlipBorrow.service';


@Component({
  selector: 'kt-borrow-document',
  templateUrl: './borrow-document.component.html',
  styleUrls: ['./borrow-document.component.scss']
})
export class BorrowDocumentComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'expectFromDate', 'expectToDate', 'status', 'reason', 'createUsername', 'createDtm', 'borrowUsername', 'actualToDate', 'action'];
  loading = false;
  dataSource;
  // page; 
  // pageSize;
  tblSlipBorrow: TblSlipBorrow = new TblSlipBorrow();
  data: TblSlipBorrow[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  formGroup: FormGroup;
  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;

  constructor(
    public dialog: MatDialog,
    public service: TblSlipBorrowService
  ) {
  }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      parentCode: new FormControl(''),
      appCode: new FormControl('')
    })
    this.doSearchSlipBorrow()
  }
  

  doSearchSlipBorrow() {
    console.log(this.tblSlipBorrow)
    this.service.doSearchSlipBorrow(this.tblSlipBorrow).subscribe(
      data => {
        this.data = data.data.list;
        this.dataSource = new MatTableDataSource<TblSlipBorrow>(data.data.list)
      }, error => {
        console.log(error)
      }
    )

  }

  addNew() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(BorrowDocumentDialogComponent, {
      data: {
        action: 'CREATE',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });

  }
  deleteBorrow() { }

  updateBorrow() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(BorrowDocumentDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }

  browserBorrow() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(BorrowDocumentBrowserDialogComponent, {
      data: {
        action: 'BROWSER',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }
}
