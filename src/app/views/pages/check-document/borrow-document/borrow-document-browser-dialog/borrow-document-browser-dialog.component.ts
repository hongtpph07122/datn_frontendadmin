import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CheckDocumentModel} from "../../model/check-document";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'kt-borrow-document-browser-dialog',
  templateUrl: './borrow-document-browser-dialog.component.html',
  styleUrls: ['./borrow-document-browser-dialog.component.scss']
})
export class BorrowDocumentBrowserDialogComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'need', 'date', 'action'];
  browserForm: FormGroup;
  checkDocumentModelData: CheckDocumentModel = new CheckDocumentModel();
  // hasFormErrors = false;
  viewLoading = false;
  titleButton = 'Duyệt';
  title= 'GĐ ĐVNV duyệt mượn chứng từ';
  loading= false;
  dataSurce2 = [
    {stt: 1, need: 'dd/mm/yy', date: 'dd/mm/yy', action: 'abc'},

  ];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    this.browserForm = new FormGroup({
      appCode: new FormControl('')
    })
  }
  onSubmit(){

  }
  addNew(){

  }

}
