import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowDocumentBrowserDialogComponent } from './borrow-document-browser-dialog.component';

describe('BorrowDocumentBrowserDialogComponent', () => {
  let component: BorrowDocumentBrowserDialogComponent;
  let fixture: ComponentFixture<BorrowDocumentBrowserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowDocumentBrowserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowDocumentBrowserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
