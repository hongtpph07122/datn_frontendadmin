import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBorrowDocumentComponent } from './manage-borrow-document.component';

describe('ManageBorrowDocumentComponent', () => {
  let component: ManageBorrowDocumentComponent;
  let fixture: ComponentFixture<ManageBorrowDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBorrowDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBorrowDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
