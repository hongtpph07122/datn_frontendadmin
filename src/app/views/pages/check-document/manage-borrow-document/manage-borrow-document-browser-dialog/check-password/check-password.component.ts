import { CategoryDeleteDialogComponent } from './../../../../category/category-delete-dialog/category-delete-dialog.component';
import { CategoryService } from './../../../../category/category.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'kt-check-password',
  templateUrl: './check-password.component.html',
  styleUrls: ['./check-password.component.scss']
})
export class CheckPasswordComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<CategoryDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: CategoryService
  ) { }

  ngOnInit(): void {
  }

  changeStatusCategory() {
    this.service.switchStateAppParam(this.data.appType, this.data.deleteData.appCode).subscribe();
    this.dialogRef.close('ok');
    alert('Xóa thành công :D')
  }
}
