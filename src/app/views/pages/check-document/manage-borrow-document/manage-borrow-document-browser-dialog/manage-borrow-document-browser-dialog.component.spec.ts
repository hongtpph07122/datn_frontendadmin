import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBorrowDocumentBrowserDialogComponent } from './manage-borrow-document-browser-dialog.component';

describe('ManageBorrowDocumentBrowserDialogComponent', () => {
  let component: ManageBorrowDocumentBrowserDialogComponent;
  let fixture: ComponentFixture<ManageBorrowDocumentBrowserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBorrowDocumentBrowserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBorrowDocumentBrowserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
