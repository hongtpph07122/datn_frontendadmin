import { CheckPasswordComponent } from './check-password/check-password.component';
import { CheckDocumentModel } from './../../model/check-document';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'kt-manage-borrow-document-browser-dialog',
  templateUrl: './manage-borrow-document-browser-dialog.component.html',
  styleUrls: ['./manage-borrow-document-browser-dialog.component.scss']
})
export class ManageBorrowDocumentBrowserDialogComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'need', 'date'];
  borrowForm: FormGroup;
  loading: false;
  checkDocumentModelData: CheckDocumentModel = new CheckDocumentModel();
  // hasFormErrors = false;
  viewLoading = false;
  dataSurce1 = [
    { stt: 1, need: 'dd/mm/yy', date: 'dd/mm/yy', action: 'abc' },

  ];
  constructor(
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.borrowForm = new FormGroup({
      appCode: new FormControl('')
    })
  }
  addNew() {

  }
  onSubmit() {

  }
  checkpass() {

    console.log("check");
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(CheckPasswordComponent, {
      width:'350px',
      data: {
        action: 'EDIT',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }
}
