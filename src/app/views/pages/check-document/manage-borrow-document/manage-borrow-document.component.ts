import { MatTableDataSource } from '@angular/material/table';
import { CheckDocumentServiceService } from './../service/check-document-service.service';
import { TblSlipBorrow } from './../model/TblSlipBorrow';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatDatepicker } from "@angular/material/datepicker";
import { FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ManageBorrowDocumentBrowserDialogComponent } from './manage-borrow-document-browser-dialog/manage-borrow-document-browser-dialog.component';
import { ManageBorrowDocumentDialogComponent } from './manage-borrow-document-dialog/manage-borrow-document-dialog.component';

const ELEMENT_DATA: TblSlipBorrow[] = [];
@Component({
  selector: 'kt-manage-borrow-document',
  templateUrl: './manage-borrow-document.component.html',
  styleUrls: ['./manage-borrow-document.component.scss']
})
export class ManageBorrowDocumentComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'expectFromDate', 'expectToDate', 'reason','branchCode', 'createUsername', 'receiveId', 'status', 'apprStatus', 'action'];
  loading = false;
  dataSource;
  page =1;pageSize=10;totalRecord; 
  tblSlipBorrow:TblSlipBorrow = new  TblSlipBorrow;
  data: TblSlipBorrow[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  formGroup: FormGroup;
  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;
 
  constructor(
    public dialog: MatDialog,
    public service: CheckDocumentServiceService
  ) {}
  ngOnInit(): void {
    this.doSearchSlipBorrow();
  }
  doSearchSlipBorrow(){
    
console.log(this.tblSlipBorrow)
    // this.tblSlipBorrow.page = this.page;
    // this.tblSlipBorrow.pageSize = this.pageSize;
    this.service.doSearchSlipBorrow(this.tblSlipBorrow).subscribe(
      data => {
        this.data = data.data.list;
        this.dataSource = new MatTableDataSource<TblSlipBorrow>(this.data)
        console.log(this.dataSource)
      }, error => {
        console.log(error)
      }
      )
    
      }
search(){
  this.tblSlipBorrow.expectFromDate = this.tblSlipBorrow.expectFromDate ;
  this.tblSlipBorrow.expectFromDateEnd = this.tblSlipBorrow.expectFromDateEnd;
  this.doSearchSlipBorrow();
}
  addNew() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(ManageBorrowDocumentDialogComponent, {
      data: {
        action: 'CREATE',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });

  }
  deleteBorrow() { }

  updateBorrow(element:TblSlipBorrow) {

    console.log("check");
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(ManageBorrowDocumentDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        tblSlipBorrow: element
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }

  browserBorrow() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(ManageBorrowDocumentBrowserDialogComponent, {
      data: {
        action: 'BROWSER',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }
}

