import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBorrowDocumentDialogComponent } from './manage-borrow-document-dialog.component';

describe('ManageBorrowDocumentDialogComponent', () => {
  let component: ManageBorrowDocumentDialogComponent;
  let fixture: ComponentFixture<ManageBorrowDocumentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBorrowDocumentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBorrowDocumentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
