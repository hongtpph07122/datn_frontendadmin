import { TblSlipBorrowDtl } from './../../model/TblSlipBorrowDtl';
import { MatTableDataSource } from '@angular/material/table';
import { CheckDocumentServiceService } from './../../service/check-document-service.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { CheckDocumentModel } from "../../model/check-document";
import { TblSlipBorrow } from '../../model/TblSlipBorrow';

@Component({
  selector: 'kt-manage-borrow-document-dialog',
  templateUrl: './manage-borrow-document-dialog.component.html',
  styleUrls: ['./manage-borrow-document-dialog.component.scss']
})
export class ManageBorrowDocumentDialogComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'need', 'date'];
  borrowForm: FormGroup;
  loading: false;
  checkDocumentModelData: CheckDocumentModel = new CheckDocumentModel();
  // hasFormErrors = false;
  tblSlipBorrow: TblSlipBorrow;
  viewLoading = false;
  dataSurce1;
  constructor(
    public dialogRef: MatDialogRef<ManageBorrowDocumentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public service: CheckDocumentServiceService
  ) { }

  ngOnInit(): void {
    this.borrowForm = new FormGroup({
      appCode: new FormControl('')
    })
    this.tblSlipBorrow = this.data.tblSlipBorrow
    this.findAllBySlipBorrowId();
   
  }
  findAllBySlipBorrowId(){
    this.service.findAllBySlipBorrowId(this.tblSlipBorrow.id).subscribe(
      data => {
        this.dataSurce1 = new MatTableDataSource<TblSlipBorrowDtl>(data.data)
        console.log(data);
      }, error => {
        console.log(error)
      }
      )
  }
// phê duyệt
  approval(){
    this.tblSlipBorrow.status = "BORROWED"
    this.service.approval(this.tblSlipBorrow).subscribe(
      data => {
        console.log(this.tblSlipBorrow)
        console.log(data)
        alert("Phê duyệt thành công")
    },error => {
      alert("Phê duyệt thất bại")
      console.log(error)
    }
    )
  }
}
