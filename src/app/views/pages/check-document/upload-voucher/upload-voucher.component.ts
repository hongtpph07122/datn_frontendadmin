import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { PopupCreateComponent } from './popup-create/popup-create.component';
import { PopupVoucherListComponent } from './popup-voucher-list/popup-voucher-list.component';

@Component({
  selector: 'kt-upload-voucher',
  templateUrl: './upload-voucher.component.html',
  styleUrls: ['./upload-voucher.component.scss']
})
export class UploadVoucherComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'code', 'dateUpload', 'userUpload', 'totalPage',  'startDate', 'endDate','note','watching'];
  loading = false;
  dataArea = []
  dataSource = [
    { stt: 1, code: 'Hydrogen', startDate: '31/12/2020', note: 'H', totalPage: 1 },

  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openPopup(){
    this.dialog.open(PopupVoucherListComponent, {
      width: '70%',
    });
  }
  popupCreate(){
    this.dialog.open(PopupCreateComponent,{
      
    })
  }

}
