import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupNumberEntriesComponent } from './popup-number-entries.component';

describe('PopupNumberEntriesComponent', () => {
  let component: PopupNumberEntriesComponent;
  let fixture: ComponentFixture<PopupNumberEntriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupNumberEntriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupNumberEntriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
