import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'kt-popup-number-entries',
  templateUrl: './popup-number-entries.component.html',
  styleUrls: ['./popup-number-entries.component.scss']
})
export class PopupNumberEntriesComponent implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  constructor() { }

  ngOnInit(): void {
  }

}
