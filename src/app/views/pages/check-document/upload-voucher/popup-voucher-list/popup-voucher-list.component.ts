import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { PopupNumberEntriesComponent } from '../popup-number-entries/popup-number-entries.component';

@Component({
  selector: 'kt-popup-voucher-list',
  templateUrl: './popup-voucher-list.component.html',
  styleUrls: ['./popup-voucher-list.component.scss']
})
export class PopupVoucherListComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'numberEntries', 'dateUpload', 'userUpload', 'totalPage', 'watching'];
  loading = false;
  dataArea = []
  dataSource = [
    { stt: 1, numberEntries: 'Hydrogen', dateUpload: '31/12/2020', userUpload: 'H', totalPage: 1 },

  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  openPopup(){
    this.dialog.open(PopupNumberEntriesComponent, {
      width: '70%',
    });
  }

}
