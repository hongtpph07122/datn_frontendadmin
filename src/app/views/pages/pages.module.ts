// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { MailModule } from './apps/mail/mail.module';
import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { MyPageComponent } from './my-page/my-page.component';
import {RouterModule} from '@angular/router';
import {CategoryModule} from './category/category.module';

// DuAn
import {UserManagerModule} from '../DuAn/user-manager/user-manager.module';
import {PostMetolManagerModule} from '../DuAn/post-metol-manager/post-metol-manager.module';



import {DirectoryAdministrationModule} from './category/directory-administration/directory-administration.module';
import { CheckDocumentComponent } from './check-document/check-document.component';
import { CheckDocumentModule } from './check-document/check-document.module';
import { ControlReportComponent } from './control-report/control-report.component';
import {ControlReportModule} from "./control-report/control-report.module";

@NgModule({
  declarations: [MyPageComponent, CheckDocumentComponent, ControlReportComponent],
  exports: [],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CoreModule,
    PartialsModule,
    MailModule,
    ECommerceModule,
    RouterModule,
    CategoryModule,
    DirectoryAdministrationModule,
    CheckDocumentModule,
    ControlReportModule,
    UserManagerModule,
    PostMetolManagerModule
  ],
  providers: []
})
export class PagesModule {
}
