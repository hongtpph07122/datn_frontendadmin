import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'kt-infor-valuation',
  templateUrl: './infor-valuation.component.html',
  styleUrls: ['./infor-valuation.component.scss']
})
export class InforValuationComponent implements OnInit {
  assetForm: FormGroup;
  upload = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }
  addNew(){
  }
  createForm(){
    this.assetForm = this.fb.group({
      codeUnit: [''],
      nameUnit: [''],
      codeArea: [''],
      email: [''],
    });
  }
}
