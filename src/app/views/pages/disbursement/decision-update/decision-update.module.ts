import { DisbusementUpdateComponent } from './disbusement-update/disbusement-update.component';
import { DecisionListHistoryDialogComponent } from './decision-list/decision-list-history-dialog/decision-list-history-dialog.component';
import { ContractListNoDecisionDetailsComponent } from './contract-list-no-decision-details/contract-list-no-decision-details.component';
import { DecisionAssessmentComponent } from './decision-assessment/decision-assessment.component';
import { ContractListNoDecisionComponent } from './contract-list-no-decision/contract-list-no-decision.component';
import { RevenueDialogComponent } from './contract-details/revenue-dialog/revenue-dialog.component';
import { ProductDialogComponent } from './contract-details/product-dialog/product-dialog.component';
import { DecisionDialogComponent } from './contract-details/decision-dialog/decision-dialog.component';
import { ContractDetailsComponent } from './contract-details/contract-details.component';
import { ComplianceAssessmentEditDialogComponent } from './compliance-assessment/compliance-assessment-edit-dialog/compliance-assessment-edit-dialog.component';
import { ComplianceAssessmentComponent } from './compliance-assessment/compliance-assessment.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { DropdownTreeviewSelectModule } from 'src/app/core/_base/tree-view/dropdown-treeview-select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { CoreModule } from 'src/app/core/core.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortletModule } from './../../../partials/content/general/portlet/portlet.module';

import { DecisionListComponent } from './decision-list/decision-list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DecisionUpdateComponent } from './decision-update.component';
import { ContractListNeedsEvaluationComponent } from './contract-list-needs-evaluation/contract-list-needs-evaluation.component';
import { ComplianceAssessmentDecisionDialogComponent } from './compliance-assessment/compliance-assessment-decision-dialog/compliance-assessment-decision-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: DecisionUpdateComponent,
    children: [
      {
        path: '',
        redirectTo: 'update',
        pathMatch: 'full'
      },
      {
        path: 'update',
        component: DecisionListComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/update'
        }
      },
      {
        path: 'contract',
        component: ContractListNeedsEvaluationComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/contract'
        }
      },
      {
        path: 'contract-no-decision',
        component: ContractListNoDecisionComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/contract-no-decision'
        }
      },
      {
        path: 'disbusement-update',
        component: DisbusementUpdateComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/disbusement-update'
        }
      },
      {
        path: 'contract-no-decision-details',
        component: ContractListNoDecisionDetailsComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/contract-no-decision-details'
        }
      },
      {
        path: 'decision-assessment',
        component: DecisionAssessmentComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/decision-assessment'
        }
      },
      {
        path: 'contract-details',
        component: ContractDetailsComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/contract-details'
        }
      }
      ,
      {
        path: 'compliance-assessment',
        component: ComplianceAssessmentComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/decision/compliance-assessment'
        }
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    PortletModule,
    FormsModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    MatExpansionModule,
    MatTabsModule,
    MatCheckboxModule
  ],
  entryComponents: [
    ComplianceAssessmentEditDialogComponent,
    ComplianceAssessmentDecisionDialogComponent,
    DecisionDialogComponent,
    ProductDialogComponent,
    RevenueDialogComponent,
    DecisionListHistoryDialogComponent
  ],
  declarations: [DecisionUpdateComponent,
    ContractListNeedsEvaluationComponent,
    DecisionListComponent,
    ComplianceAssessmentComponent,
    ComplianceAssessmentEditDialogComponent,
    ComplianceAssessmentDecisionDialogComponent,
    ContractDetailsComponent,
    DecisionDialogComponent,
    ProductDialogComponent,
    RevenueDialogComponent,
    ContractListNoDecisionComponent,
    DecisionAssessmentComponent,
    ContractListNoDecisionDetailsComponent,
    DecisionListHistoryDialogComponent,
    DisbusementUpdateComponent]
})
export class DecisionUpdateModule { }
