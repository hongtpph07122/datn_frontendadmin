import { ComplianceAssessment } from './../../../model/compliance-assessment';
import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'need', 'date', 'action'];
  borrowForm: FormGroup;
  loading:false;hiddenRow:boolean = true;selected:any;
  checkDocumentModelData: ComplianceAssessment = new ComplianceAssessment();
  // hasFormErrors = false;
  viewLoading = false;
  dataSurce1 = [
    {stt: 1, need: 'dd/mm/yy', date: 'dd/mm/yy', action: 'abc'},

  ];
  constructor() { }

  ngOnInit(): void {
    this.borrowForm = new FormGroup({
      appCode: new FormControl('')
    })
  }
  getTitle(){
    if (this.checkDocumentModelData.id){
      return 'Cập nhật điều kiện sản phẩm';
    }
    return  'Thêm mới điều kiện sản phẩm'
  }
  addNew(){

  }
  onSubmit(){

  }
  onChange(event: any){
    this.selected = event.target.value;
    console.log(this.selected);
    if(this.selected ==1){
      if(this.hiddenRow == true){
        this.hiddenRow =false;
        }else{
          this.hiddenRow = true;
        }
    }else{
      this.hiddenRow = true
    }
    
  }

}
