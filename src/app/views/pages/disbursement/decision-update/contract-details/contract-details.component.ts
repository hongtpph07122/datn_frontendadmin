import { ProductDialogComponent } from './product-dialog/product-dialog.component';
import { RevenueDialogComponent } from './revenue-dialog/revenue-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DecisionDialogComponent } from './decision-dialog/decision-dialog.component';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
export interface PeriodicElement {
  stt: number;
  dknq: string;
  refcode: string;
  ptks: string;
  ngaygiaingan: string;
  ngaydenhan: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {stt: 1, dknq: 'Định kỳ', refcode: 'REF1834100085', ptks: 'Bổ sung chứng từ',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 2, dknq: 'MĐSDV', refcode: 'REF1932500100', ptks: 'Hệ thống',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 3, dknq: 'Bán chéo', refcode: 'REF1934300347', ptks: 'Hệ thống',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 4, dknq: 'Bảo hiểm TSBĐ', refcode: 'REF1934300246', ptks: 'Hệ thống',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 5, dknq: 'Định giá TSBĐ', refcode: 'REF1934300200', ptks: 'Bổ sung chứng từ',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 6, dknq: 'Quan hệ tín dụng', refcode: 'REF1934300210', ptks: 'Bổ sung chứng từ',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  
]; 
@Component({
  selector: 'app-contract-details',
  templateUrl: './contract-details.component.html',
  styleUrls: ['./contract-details.component.scss']
})
export class ContractDetailsComponent implements OnInit {
  areaForm: FormGroup;
  displayedColumns: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan'];
  displayedDKNQ: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','tenchungtu','ngaybosung','action'];
  displayedDKDT: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','tenchungtu','ngaybosung','noidung','ghichu','action'];
  dataSource = ELEMENT_DATA;
  panelOpenState = false;
  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }
  addNew(){
    alert("hello")
  }
  revenueDialog() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(RevenueDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }
  productDialog() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }
  decisionDialog() {
    // const categoryInt = new CategoryModel();
    // categoryInt.status = 'A';
    const dialogRef = this.dialog.open(DecisionDialogComponent, {
      data: {
        action: 'EDIT',
        titel: '',
        // category: categoryInt
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.getDataTable();
      }
    });
  }
}
