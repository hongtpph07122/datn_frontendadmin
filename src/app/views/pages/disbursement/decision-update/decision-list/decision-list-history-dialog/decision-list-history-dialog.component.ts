import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
export interface PeriodicElement {
  stt: number;
  dknq: string;
  refcode: string;
  ptks: string;
  ngaygiaingan: string;
  ngaydenhan: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {stt: 1, dknq: 'Định kỳ', refcode: 'REF1834100085', ptks: 'Bổ sung chứng từ',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 2, dknq: 'MĐSDV', refcode: 'REF1932500100', ptks: 'Hệ thống',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 3, dknq: 'Bán chéo', refcode: 'REF1934300347', ptks: 'Hệ thống',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 4, dknq: 'Bảo hiểm TSBĐ', refcode: 'REF1934300246', ptks: 'Hệ thống',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 5, dknq: 'Định giá TSBĐ', refcode: 'REF1934300200', ptks: 'Bổ sung chứng từ',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  {stt: 6, dknq: 'Quan hệ tín dụng', refcode: 'REF1934300210', ptks: 'Bổ sung chứng từ',ngaygiaingan: '13/11/2020',ngaydenhan:'16/11/2020'},
  
]; 
@Component({
  selector: 'app-decision-list-history-dialog',
  templateUrl: './decision-list-history-dialog.component.html',
  styleUrls: ['./decision-list-history-dialog.component.scss']
})
export class DecisionListHistoryDialogComponent implements OnInit {
// đánh giá tuân thủ
isLoading: boolean = false;
areaForm: FormGroup;
displayedColumns: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan','action'];
displayedDKNQ: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan','hosothieu','mucdoruiro','ngaytrenbienban','ketquadanhgia','trangthai','action'];
displayedDKDT: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan','hosothieu','mucdoruiro','ngaytrenbienban','action'];
displayedDKSP: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan','hosothieu'];

displayedColumn: string[] = ['stt', 'dknq', 'refcode', 'ptks','ngaygiaingan','ngaydenhan'];
dataSource = ELEMENT_DATA;
panelOpenState = true;
  constructor() { }

  ngOnInit() {
  }

}
