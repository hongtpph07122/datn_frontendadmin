import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
export interface PeriodicElement {
  stt: number;
  maCN: string;
  cnQL: string;
  pGD: string;
  idKH: string;
  tenKH: string;
  sanPham: string;
  maHD: string;
  ngayKT: string;
  loaiKV: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {stt: 1, maCN: 'VN001090', cnQL: 'CN Hoàng Mai',
   pGD: 'CN Hoàng Mai',idKH: '123456'
   ,tenKH:'Kiều Cao Long',
    sanPham: 'Vay kinh doanh', maHD: '500.000.000',
    ngayKT: '13/11/2020',loaiKV: 'Tín dụng'},
   
]; 
@Component({
  selector: 'app-contract-list-needs-evaluation',
  templateUrl: './contract-list-needs-evaluation.component.html',
  styleUrls: ['./contract-list-needs-evaluation.component.scss']
})
export class ContractListNeedsEvaluationComponent implements OnInit {
  // màn danh sách hợp đồng cần đánh giá
  isLoading: boolean = false;
  categoryForm: FormGroup;
  displayedColumns: string[] = ['stt', 'maCN', 'cnQL', 'pGD', 'idKH','tenKH','sanPham','maHD', 'ngayKT', 'loaiKV','status'];
 
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  appType = 'DM_KHU_VUC';
  appCode = 'helo';
  appName = 'lohe';
  public listTC: Criteria[] = []
  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.listTC.push(new Criteria());
  }

  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.stt + 1}`;
  }

}

export class Criteria {
}
