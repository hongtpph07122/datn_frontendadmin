import { ComplianceAssessment } from './../../../model/compliance-assessment';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-compliance-assessment-edit-dialog',
  templateUrl: './compliance-assessment-edit-dialog.component.html',
  styleUrls: ['./compliance-assessment-edit-dialog.component.scss']
})
export class ComplianceAssessmentEditDialogComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'need', 'date', 'action'];
  borrowForm: FormGroup;
  loading:false;
  checkDocumentModelData: ComplianceAssessment = new ComplianceAssessment();
  // hasFormErrors = false;
  viewLoading = false;
  dataSurce1 = [
    {stt: 1, need: 'dd/mm/yy', date: 'dd/mm/yy', action: 'abc'},

  ];
  constructor() { }

  ngOnInit(): void {
    this.borrowForm = new FormGroup({
      appCode: new FormControl('')
    })
  }
  getTitle(){
    if (this.checkDocumentModelData.id){
      return 'Cập nhật mượn chứng từ';
    }
    return  'Sửa điều kiện nghị quyết'
  }
  addNew(){

  }
  onSubmit(){

  }

}
