import { ComplianceAssessment } from './../../../model/compliance-assessment';
import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compliance-assessment-decision-dialog',
  templateUrl: './compliance-assessment-decision-dialog.component.html',
  styleUrls: ['./compliance-assessment-decision-dialog.component.scss']
})
export class ComplianceAssessmentDecisionDialogComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'need', 'date', 'action'];
  borrowForm: FormGroup;
  loading:false; hideForm: boolean = true;
  checkDocumentModelData: ComplianceAssessment = new ComplianceAssessment();
  // hasFormErrors = false;
  viewLoading = false;
  dataSurce1 = [
    {stt: 1, need: 'dd/mm/yy', date: 'dd/mm/yy', action: 'abc'},

  ];
  constructor() { }

  ngOnInit(): void {
    this.borrowForm = new FormGroup({
      appCode: new FormControl('')
    })
  }
  getTitle(){
    if (this.checkDocumentModelData.id){
      return 'Cập nhật mượn chứng từ';
    }
    return  'Điều kiện nghị quyết'
  }
  
  addNew(){
    if(this.hideForm == true){
      this.hideForm =false;
    }else{
      this.hideForm =true;
    }
  
  }
  onSubmit(){
  
  }

}
