/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ComplianceAssessmentDecisionDialogComponent } from './compliance-assessment-decision-dialog.component';

describe('ComplianceAssessmentDecisionDialogComponent', () => {
  let component: ComplianceAssessmentDecisionDialogComponent;
  let fixture: ComponentFixture<ComplianceAssessmentDecisionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplianceAssessmentDecisionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplianceAssessmentDecisionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
