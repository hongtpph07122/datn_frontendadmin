import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPropertyGuaranteedBrowserComponent } from './check-property-guaranteed-browser.component';

describe('CheckPropertyGuaranteedBrowserComponent', () => {
  let component: CheckPropertyGuaranteedBrowserComponent;
  let fixture: ComponentFixture<CheckPropertyGuaranteedBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckPropertyGuaranteedBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPropertyGuaranteedBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
