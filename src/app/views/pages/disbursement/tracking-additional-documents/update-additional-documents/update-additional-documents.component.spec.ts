import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdditionalDocumentsComponent } from './update-additional-documents.component';

describe('UpdateAdditionalDocumentsComponent', () => {
  let component: UpdateAdditionalDocumentsComponent;
  let fixture: ComponentFixture<UpdateAdditionalDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdditionalDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdditionalDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
