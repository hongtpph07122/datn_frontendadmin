import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingAdditionalDocumentsComponent } from './tracking-additional-documents.component';

describe('TrackingAdditionalDocumentsComponent', () => {
  let component: TrackingAdditionalDocumentsComponent;
  let fixture: ComponentFixture<TrackingAdditionalDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingAdditionalDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingAdditionalDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
