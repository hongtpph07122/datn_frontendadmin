import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalDocumentsUpdateComponent } from './additional-documents-update.component';

describe('AdditionalDocumentsUpdateComponent', () => {
  let component: AdditionalDocumentsUpdateComponent;
  let fixture: ComponentFixture<AdditionalDocumentsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalDocumentsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalDocumentsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
