import {MatFormFieldModule, MatLabel} from '@angular/material/form-field';
import {MatTabsModule} from '@angular/material/tabs';
import {CheckPropertyGuaranteedComponent} from './check-property-guaranteed/check-property-guaranteed.component';
import {CheckPropertyGuaranteedBrowserComponent} from './check-property-guaranteed-browser/check-property-guaranteed-browser.component';
import {CheckPropertyGuaranteedRefuseComponent} from './check-property-guaranteed-refuse/check-property-guaranteed-refuse.component';
import {RefuseComponent} from './Re-insurance/refuse/refuse.component';
import {ApprovalConfirmationComponent} from './approval-confirmation/approval-confirmation.component';
import {ReInsuranceComponent} from './Re-insurance/Re-insurance.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DisbursementComponent} from './disbursement.component';
import {DropdownTreeviewSelectModule} from 'src/app/core/_base/tree-view/dropdown-treeview-select';
import {CheckCustomersPeriodicallyComponent} from './check-customers-periodically/check-customers-periodically.component';
import {AssetValuationUpdatesComponent} from './asset-valuation-updates/asset-valuation-updates.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import {PortletModule} from '../../partials/content/general/portlet/portlet.module';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {CoreModule} from 'src/app/core/core.module';
import {AdditionalDocumentsUpdateComponent} from './additional-documents-update/additional-documents-update.component';
import {ResultPropertyUpdateComponent} from './result-property-update/result-property-update.component';
import {EditInsuranceComponent} from './Re-insurance/edit-insurance/edit-insurance.component';
import {ConfirmationComponent} from './Re-insurance/confirmation/confirmation.component';
import {CheckCapitalUsagePurposesComponent} from './check-capital-usage-purposes/check-capital-usage-purposes.component';
import {UpdateCapitalUsagePurposesComponent} from './check-capital-usage-purposes/update-capital-usage-purposes/update-capital-usage-purposes.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {PricingValuationComponent} from './pricing-valuation/pricing-valuation.component';
import {InforValuationComponent} from './infor-valuation/infor-valuation.component';
import {TrackingAdditionalDocumentsComponent} from './tracking-additional-documents/tracking-additional-documents.component';
import {UpdateAdditionalDocumentsComponent} from './tracking-additional-documents/update-additional-documents/update-additional-documents.component';


const routes: Routes = [
  {
    path: '',
    component: DisbursementComponent,
    children: [
      {
        path: '',
        redirectTo: 'decision',
        pathMatch: 'full'
      },
      {
        path: 'decision',
        loadChildren: () => import('src/app/views/pages/disbursement/decision-update/decision-update.module').then(m => m.DecisionUpdateModule),
      },
      {
        path: 'update-check-customer',
        component: CheckCustomersPeriodicallyComponent,
        data: {
          configPath: '/disbursement/update-check-customer',
        }
      },
      {
        path: 'asset',
        component: AssetValuationUpdatesComponent,
        data: {
          configPath: '/disbursement/asset'
        }
      },
      {
        path: 'tracks-future-assets',
        loadChildren: () => import('src/app/views/pages/disbursement/tracks-future-assets/tracks-future-assets.module').then(m => m.TracksFutureAssetsModule),
      }
      , {
        path: 'result-asset',
        component: ResultPropertyUpdateComponent,
        data: {
          configPath: '/disbursement/result-asset'
        }
      },
      {
        path: 'check-property',
        component: CheckPropertyGuaranteedComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/check-property'
        }
      },
      {
        path: 'check-property/browser',
        component: CheckPropertyGuaranteedBrowserComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/check-property/browser'
        }
      },
      {
        path: 'result-property',
        component: ResultPropertyUpdateComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/result-property'
        }
      },
      {
        path: 'approval',
        component: ApprovalConfirmationComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/approval'
        }
      },
      {
        path: 'pricing',
        component: PricingValuationComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/pricing'
        }
      },
      {
        path: 'additional-documents-update',
        component: AdditionalDocumentsUpdateComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/additional-documents-update'
        }
      },
      {
        path: 'insurance',
        component: ReInsuranceComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/insurance'
        }
      },
      {
        path: 'edit-insurance',
        component: EditInsuranceComponent,
        //        // canActivate: [UserRoleAccessService],
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/edit-insurance'
        }
      },
      {
        path: 'approval-confirmation',
        component: ApprovalConfirmationComponent,
        //        // canActivate: [UserRoleAccessService],
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/approval-confirmation'
        }
      },
      {
        path: 'confirmation',

        component: ConfirmationComponent,
        //        // canActivate: [UserRoleAccessService],
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/confirmation'
        }
      }, {
        path: 'checkcapitalusage',
        component: CheckCapitalUsagePurposesComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/checkcapitalusage'
        }
      },
      {
        path: 'checkcapitalusage/update',
        component: UpdateCapitalUsagePurposesComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/checkcapitalusage/update'
        }
      },
      {
        path: 'infor-valuation',
        component: InforValuationComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/infor-valuation'
        }
      },
      {
        path: 'tracking-additional-documents',
        component: TrackingAdditionalDocumentsComponent,
               // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/tracking-additional-documents'
        }
      },
      {
        path: 'tracking-additional-documents/update',
        component: UpdateAdditionalDocumentsComponent,
        //        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/disbursement/tracking-additional-documents/update'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    PortletModule,
    FormsModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    MatExpansionModule,
    MatTabsModule,
    MatCheckboxModule,
    MatFormFieldModule,

  ],
  entryComponents: [

    RefuseComponent,

    CheckPropertyGuaranteedRefuseComponent
  ],
  declarations: [DisbursementComponent,
    AssetValuationUpdatesComponent,
    ApprovalConfirmationComponent,

    CheckCustomersPeriodicallyComponent,
    AdditionalDocumentsUpdateComponent,
    ResultPropertyUpdateComponent,
    ResultPropertyUpdateComponent,
    CheckPropertyGuaranteedComponent,
    CheckPropertyGuaranteedBrowserComponent,
    CheckPropertyGuaranteedRefuseComponent,

    ResultPropertyUpdateComponent,
    PricingValuationComponent,
    CheckCustomersPeriodicallyComponent,
    AdditionalDocumentsUpdateComponent,
    ReInsuranceComponent,
    EditInsuranceComponent,
    ConfirmationComponent,
    InforValuationComponent,
    RefuseComponent,
    CheckCapitalUsagePurposesComponent,
    UpdateCapitalUsagePurposesComponent,
    InforValuationComponent,
    TrackingAdditionalDocumentsComponent,
    UpdateAdditionalDocumentsComponent

  ]

})
export class DisbursementModule {
}
