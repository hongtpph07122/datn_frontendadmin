import { RefuseComponent } from './refuse/refuse.component';
import { ChangeDetectorRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: string;
  symbol: string;
  symbol2: string;
  symbol3: string;
  symbol4: string;
  symbol5: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: '123456', weight: "CN Hoàng Mai", symbol: 'KH12',symbol2: 'Kiều Cao Long',symbol3: 'MD1',symbol4: 'HD456789',symbol5: '22/05/2020'},
  {position: 2, name: '123456', weight: "CN Hoàng Mai", symbol: 'KH12',symbol2: 'Kiều Cao Long',symbol3: 'MD1',symbol4: 'HD456789',symbol5: '22/05/2020'},
  {position: 3, name: '123456', weight: "CN Hoàng Mai", symbol: 'KH12',symbol2: 'Kiều Cao Long',symbol3: 'MD1',symbol4: 'HD456789',symbol5: '22/05/2020'},

];

@Component({
  selector: 'app-Re-insurance',
  templateUrl: './Re-insurance.component.html',
  styleUrls: ['./Re-insurance.component.scss']
})
export class ReInsuranceComponent implements OnInit {
  displayedColumns: string[] = ['select', 'position', 'name', 'weight', 'symbol','symbol2','symbol3','symbol4','symbol5','symbol6'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  isLoading = false;
  categoryForm: FormGroup;
  appType = 'DM_KHU_VUC';
  appCode = '';
  appName = '';
  status = '';
  areaForm: FormGroup;
  panelOpenState = false;
  public listTC: Criteria[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef,
              private router: Router) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
    this.listTC.push(new Criteria());
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
   /** Selects all rows if they are not all selected; otherwise clear selection. */
   masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }
  approval(){
    this.router.navigate(["/disbursement/confirmation"]);
  }
  deleteCategoryArea() {
    const deleteInitData = this.dialog.open(RefuseComponent, {
      data: {

      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {

      }
    });

  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }

}export class Criteria {
}
