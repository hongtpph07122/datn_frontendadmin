import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'kt-refuse',
  templateUrl: './refuse.component.html',
  styleUrls: ['./refuse.component.scss']
})
export class RefuseComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<RefuseComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
  }
  changeStatusCategory() {
    this.dialogRef.close('ok');
    alert('Từ chối công :D')
  }
}
