import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'kt-pricing-valuation',
  templateUrl: './pricing-valuation.component.html',
  styleUrls: ['./pricing-valuation.component.scss']
})
export class PricingValuationComponent implements OnInit {
  displayedColumns: string[] = [ 'maCN', 'tenCN', 'maKH', 'tenKH','maMD','maTSDB','moTaTS','kyDGTS','TTDGTS','TTKTTS','action'];
  isLoading: boolean = false;
  categoryForm: FormGroup;
  dataSource = [];
  appType = 'DM_KHU_VUC';
  appCode = '';
  appName = '';
  status = '';
  demo = {
    maCN: '123456',
    tenCN: 'CN Hoàng Mai',
    maKH: 'KH12',
    tenKH: 'Kiều Cao Long',
    maMD: 'MD12',
    maTSDB: 'TSDB12',
    moTaTs: 'abcd',
    kyDGTS: 'DGYS12',
    TTDGTS: 'abc',
    TTKTTS: 'abd'
  }

  public listTC: Criteria[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.dataSource.push(this.demo)
    this.createForm();
    this.getDataTable();
    this.listTC.push(new Criteria());
  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }

  editPricing(value){

  }
  filePricing(value){}

}
export class Criteria {
}
