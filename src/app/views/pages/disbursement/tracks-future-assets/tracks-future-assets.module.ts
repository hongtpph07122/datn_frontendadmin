import { FutureAssetsUpdateComponent } from './future-assets-update/future-assets-update.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { DropdownTreeviewSelectModule } from 'src/app/core/_base/tree-view/dropdown-treeview-select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoreModule } from 'src/app/core/core.module';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortletModule } from './../../../partials/content/general/portlet/portlet.module';
import { Routes, RouterModule } from '@angular/router';
import { ListFutureAssetsComponent } from './list-future-assets/list-future-assets.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TracksFutureAssetsComponent } from './tracks-future-assets.component';
const routes: Routes = [
  {
    path: '',
    component: TracksFutureAssetsComponent,
    children: [
      {
        path: '',
        redirectTo: 'list-future-assets',
        pathMatch: 'full'
      },
      {
        path: 'list-future-assets',
        component: ListFutureAssetsComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/tracks-future-assets/list-future-assets'
        }
      },
      {
        path: 'future-assets-update',
        component: FutureAssetsUpdateComponent,
        // canActivate: [UserRoleAccessService],
        data: {
          configPath: '/tracks-future-assets/future-assets-update'
        }
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    CommonModule,
    PortletModule,
    FormsModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    MatExpansionModule,
    MatTabsModule,
    MatCheckboxModule
  ],
  declarations: [TracksFutureAssetsComponent,
    ListFutureAssetsComponent,
    FutureAssetsUpdateComponent]
})
export class TracksFutureAssetsModule { }
