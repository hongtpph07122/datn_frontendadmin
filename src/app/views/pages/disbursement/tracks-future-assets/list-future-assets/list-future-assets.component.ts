import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
export interface PeriodicElement {
  stt: number;
  maCN: string;
  cnQL: string;
  pGD: string;
  idKH: string;
  tenKH: string;
  phanKhuc: string;
  sanPham: string;
  maHD: string;
  ngayGN: string;
  loaiKV: string;
  dkNQ: string;
  dkDT: string;
  dkSP: string;
  trangthaiNQ: string;
  trangthaiDG: string;
 
}
const ELEMENT_DATA: PeriodicElement[] = [
  {stt: 1, maCN: 'VN001090', cnQL: 'CN Hoàng Mai',
   pGD: 'CN Hoàng Mai',idKH: '123456'
   ,tenKH:'Kiều Cao Long',
   phanKhuc: 'Khách hàng DN lớn', sanPham: 'Vay kinh doanh', maHD: '500.000.000',
   ngayGN: '13/11/2020',loaiKV: 'Tín dụng'
   ,dkNQ:'16/11/2020',dkDT: 'Bổ sung chứng từ',dkSP: '13/11/2020'
   ,trangthaiNQ:'Chờ phê duyệt',trangthaiDG:'Chờ đánh giá'},
   
]; 
@Component({
  selector: 'app-list-future-assets',
  templateUrl: './list-future-assets.component.html',
  styleUrls: ['./list-future-assets.component.scss']
})
export class ListFutureAssetsComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'maCN', 'cnQL', 'pGD', 'idKH','tenKH','phanKhuc','sanPham','maHD','status'];
  isLoading: boolean = false;
  categoryForm: FormGroup;
  // dataSource = ELEMENT_DATA;
  appType = 'DM_KHU_VUC';
  appCode = 'helo';
  appName = 'lohe';
  status = 'A';
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  public listTC: Criteria[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
    this.listTC.push(new Criteria());
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.stt + 1}`;
  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }

}
export class Criteria {
}