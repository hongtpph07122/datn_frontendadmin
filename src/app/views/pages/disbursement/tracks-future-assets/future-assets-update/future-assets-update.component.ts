import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-future-assets-update',
  templateUrl: './future-assets-update.component.html',
  styleUrls: ['./future-assets-update.component.scss']
})
export class FutureAssetsUpdateComponent implements OnInit {

  panelOpenState = false;
  upload = true;
  constructor() { }

  ngOnInit() {
  }

}
