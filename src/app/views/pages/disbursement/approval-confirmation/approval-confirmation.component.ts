import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'kt-approval-confirmation',
  templateUrl: './approval-confirmation.component.html',
  styleUrls: ['./approval-confirmation.component.scss']
})
export class ApprovalConfirmationComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'maCN', 'tenCN', 'maKH', 'tenKH','maMD','maTSDB','status'];
  isLoading: boolean = false;
  categoryForm: FormGroup;
  dataSource = [];
  appType = 'DM_KHU_VUC';
  appCode = '';
  appName = '';
  status = '';

  public listTC: Criteria[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
    this.listTC.push(new Criteria());
  }
  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }

}
export class Criteria {
}
