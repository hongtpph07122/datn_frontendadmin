import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckCustomersPeriodicallyComponent } from './check-customers-periodically.component';

describe('CheckCustomersPeriodicallyComponent', () => {
  let component: CheckCustomersPeriodicallyComponent;
  let fixture: ComponentFixture<CheckCustomersPeriodicallyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckCustomersPeriodicallyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckCustomersPeriodicallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
