import { CategoryDeleteDialogComponent } from './../../category/category-delete-dialog/category-delete-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';




export interface PeriodicElement {
  stt: number;
  maCN: string;
  tenCN: string;
  maKH: string;
  tenKH: string;
  maMD: string;
  maTSD: string;
  motaTS: string;
  status: string;

}
const ELEMENT_DATA: PeriodicElement[] = [
  {
    stt: 1, maCN: "Hydrogen", tenCN: "1.0079", maKH: "H", tenKH: "", maMD: "string", maTSD: "", motaTS: "",status: ""
  }

];
@Component({
  selector: 'kt-check-property-guaranteed',
  templateUrl: './check-property-guaranteed.component.html',
  styleUrls: ['./check-property-guaranteed.component.scss']
})
export class CheckPropertyGuaranteedComponent implements OnInit {
  displayedColumns: string[] = ['select', 'stt', 'maCN', 'tenCN', 'maKH', 'tenKH', 'maMD', 'maTSDB', 'motaTS', 'status'];
  isLoading: boolean = false;
  categoryForm: FormGroup;
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  appType = 'DM_KHU_VUC';
  appCode = '';
  appName = '';
  status = '';
  public listTC: Criteria[] = []
  selection = new SelectionModel<PeriodicElement>(true, []);



  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.createForm();
    this.getDataTable();
    this.listTC.push(new Criteria());
  }

  updateProperty(){

  }
  deleteCategoryUnit(value: PeriodicElement){
    console.log(value);
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: value,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }
  createForm() {
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find() {
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria() {
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value) {
    this.listTC.splice(value, 1);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.stt + 1}`;
  }

}
export class Criteria {
}
