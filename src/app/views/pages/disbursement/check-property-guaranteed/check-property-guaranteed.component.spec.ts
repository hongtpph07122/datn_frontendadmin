import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPropertyGuaranteedComponent } from './check-property-guaranteed.component';

describe('CheckPropertyGuaranteedComponent', () => {
  let component: CheckPropertyGuaranteedComponent;
  let fixture: ComponentFixture<CheckPropertyGuaranteedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckPropertyGuaranteedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPropertyGuaranteedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
