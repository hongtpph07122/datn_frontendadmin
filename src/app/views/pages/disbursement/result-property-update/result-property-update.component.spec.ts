import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultPropertyUpdateComponent } from './result-property-update.component';

describe('ResultPropertyUpdateComponent', () => {
  let component: ResultPropertyUpdateComponent;
  let fixture: ComponentFixture<ResultPropertyUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultPropertyUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPropertyUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
