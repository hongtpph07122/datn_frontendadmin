import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'kt-result-property-update',
  templateUrl: './result-property-update.component.html',
  styleUrls: ['./result-property-update.component.scss']
})
export class ResultPropertyUpdateComponent implements OnInit {
  assetForm: FormGroup;
  upload = true;
  panelOpenState = false;
  constructor(public dialog: MatDialog,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  addNew() {
  }

  createForm() {
    this.assetForm = this.fb.group({
      codeUnit: [''],
      nameUnit: [''],
      codeArea: [''],
      email: [''],
    });
  }

}
