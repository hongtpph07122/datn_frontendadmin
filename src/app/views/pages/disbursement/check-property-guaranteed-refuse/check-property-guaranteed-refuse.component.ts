import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'kt-check-property-guaranteed-refuse',
  templateUrl: './check-property-guaranteed-refuse.component.html',
  styleUrls: ['./check-property-guaranteed-refuse.component.scss']
})
export class CheckPropertyGuaranteedRefuseComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<CheckPropertyGuaranteedRefuseComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
  changeStatusBrowser(){}
}
