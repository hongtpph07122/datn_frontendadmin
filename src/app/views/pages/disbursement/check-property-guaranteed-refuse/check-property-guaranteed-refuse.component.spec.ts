/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CheckPropertyGuaranteedRefuseComponent } from './check-property-guaranteed-refuse.component';

describe('CheckPropertyGuaranteedRefuseComponent', () => {
  let component: CheckPropertyGuaranteedRefuseComponent;
  let fixture: ComponentFixture<CheckPropertyGuaranteedRefuseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckPropertyGuaranteedRefuseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPropertyGuaranteedRefuseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
