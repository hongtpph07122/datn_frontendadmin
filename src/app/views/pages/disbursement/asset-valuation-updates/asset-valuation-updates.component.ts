import {ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../category/model/category';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from '../../category/category.service';
import {SUCCESS_RESPONSE} from '../../../../core/constants/app.constants';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {CategoryUnitDialogComponent} from '../../category/category-unit/category-unit-dialog/category-unit-dialog.component';
import {CategoryDeleteDialogComponent} from '../../category/category-delete-dialog/category-delete-dialog.component';

@Component({
  selector: 'kt-asset-valuation-updates',
  templateUrl: './asset-valuation-updates.component.html',
  styleUrls: ['./asset-valuation-updates.component.scss']
})
export class AssetValuationUpdatesComponent implements OnInit {
  assetForm: FormGroup;
  upload = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }
  addNew(){
  }
  createForm(){
    this.assetForm = this.fb.group({
      codeUnit: [''],
      nameUnit: [''],
      codeArea: [''],
      email: [''],
    });
  }
}
