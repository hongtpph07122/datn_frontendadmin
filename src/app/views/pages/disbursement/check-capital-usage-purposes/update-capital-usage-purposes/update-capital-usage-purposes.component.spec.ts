import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCapitalUsagePurposesComponent } from './update-capital-usage-purposes.component';

describe('UpdateCapitalUsagePurposesComponent', () => {
  let component: UpdateCapitalUsagePurposesComponent;
  let fixture: ComponentFixture<UpdateCapitalUsagePurposesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCapitalUsagePurposesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCapitalUsagePurposesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
