import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckCapitalUsagePurposesComponent } from './check-capital-usage-purposes.component';

describe('CheckCapitalUsagePurposesComponent', () => {
  let component: CheckCapitalUsagePurposesComponent;
  let fixture: ComponentFixture<CheckCapitalUsagePurposesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckCapitalUsagePurposesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckCapitalUsagePurposesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
