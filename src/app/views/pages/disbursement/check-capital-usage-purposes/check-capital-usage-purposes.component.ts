import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {CategoryDeleteDialogComponent} from '../../category/category-delete-dialog/category-delete-dialog.component';

export interface PeriodicElement {
  stt:number;
  maCN:number;
  tenCN:string;
  maKH:string;
  tenKH:string;
  phanKhuc:string;
  maHopDong:string;
  sanPham:string;
  khoanVay:string;
  ngayGiaiNgan:string;
  trangThai:string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { stt:1,
  maCN:1,
  tenCN:"Tên CN",
  maKH:"Mã 1",
  tenKH:"Tên KH",
  phanKhuc:"Phân khúc 1",
  maHopDong:"Mã hợp đồng 1",
  sanPham:"Sản phẩm 1",
  khoanVay:"Khoản vay 1",
  ngayGiaiNgan:"12/20/2002",
  trangThai:"Đã hoàn thành"},

  { stt:2,
  maCN:1,
  tenCN:"Tên CN",
  maKH:"Mã 1",
  tenKH:"Tên KH",
  phanKhuc:"Phân khúc 1",
  maHopDong:"Mã hợp đồng 1",
  sanPham:"Sản phẩm 1",
  khoanVay:"Khoản vay 1",
  ngayGiaiNgan:"12/20/2002",
  trangThai:"Đã hoàn thành"},
  { stt:3,
  maCN:1,
  tenCN:"Tên CN",
  maKH:"Mã 1",
  tenKH:"Tên KH",
  phanKhuc:"Phân khúc 1",
  maHopDong:"Mã hợp đồng 1",
  sanPham:"Sản phẩm 1",
  khoanVay:"Khoản vay 1",
  ngayGiaiNgan:"12/20/2002",
  trangThai:"Đã hoàn thành"},
  { stt:4,
  maCN:1,
  tenCN:"Tên CN",
  maKH:"Mã 1",
  tenKH:"Tên KH",
  phanKhuc:"Phân khúc 1",
  maHopDong:"Mã hợp đồng 1",
  sanPham:"Sản phẩm 1",
  khoanVay:"Khoản vay 1",
  ngayGiaiNgan:"12/20/2002",
  trangThai:"Đã hoàn thành"},
];
@Component({
  selector: 'kt-check-capital-usage-purposes',
  templateUrl: './check-capital-usage-purposes.component.html',
  styleUrls: ['./check-capital-usage-purposes.component.scss']
})
export class CheckCapitalUsagePurposesComponent implements OnInit {
 isLoading: boolean = false;
  categoryForm: FormGroup;
  displayedColumns: string[] = ['select','stt', 'maCN', 'tenCN', 'maKH', 'tenKH','phanKhuc','maHopDong','sanPham', 'khoanVay', 'ngayGiaiNgan','trangThai','active'];

  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  appType = 'DM_KHU_VUC';
  appCode = 'helo';
  appName = 'lohe';
  public listTC: Criteria[] = []
  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.listTC.push(new Criteria());
  }

  createForm(){
    this.categoryForm = this.fb.group({
      codeArea: [''],
      nameArea: ['']
    });
  }

  getDataTable() {
  }

  reloadTable(data) {
  }
  find(){
    this.appCode = this.categoryForm.value.codeArea.replace(/(\s\s+| )/g, ' ').trim();
    this.appName = this.categoryForm.value.nameArea.replace(/(\s\s+| )/g, ' ').trim();
    this.getDataTable();
  }

  addNewCriteria(){
    this.listTC.push(new Criteria());
  }

  deleteNewCriteria(value){
    this.listTC.splice(value,1);
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.stt + 1}`;
  }

  deleteCategoryUnit(value: PeriodicElement){
    console.log(value);
    const deleteInitData = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        deleteData: value,
        appType: this.appType
      },
      width: '40%',
    });
    deleteInitData.afterClosed().subscribe(data => {
      if (data) {
        this.getDataTable();
      }
    });
  }

}



export class Criteria {
}
