import { MatTableDataSource } from '@angular/material/table';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  stt: number;
  maCN: string;
  cnQL: string;
  pGD: string;
  idKH: string;
  tenKH: string;
  phanKhuc: string;
  sanPham: string;
  maHD: string;
  ngayGN: string;
  loaiKV: string;
  dkNQ: string;
  dkDT: string;
  dkSP: string;
  trangthaiNQ: string;
  trangthaiDG: string;
 
}
const ELEMENT_DATA: PeriodicElement[] = [
  {stt: 1, maCN: 'VN001090', cnQL: 'CN Hoàng Mai',
   pGD: 'CN Hoàng Mai',idKH: '123456'
   ,tenKH:'Kiều Cao Long',
   phanKhuc: 'Khách hàng DN lớn', sanPham: 'Vay kinh doanh', maHD: '500.000.000',
   ngayGN: '13/11/2020',loaiKV: 'Tín dụng'
   ,dkNQ:'16/11/2020',dkDT: 'Bổ sung chứng từ',dkSP: '13/11/2020'
   ,trangthaiNQ:'Chờ phê duyệt',trangthaiDG:'Chờ đánh giá'},
   
]; 

@Component({
  selector: 'kt-detailed-voucher',
  templateUrl: './detailed-voucher.component.html',
  styleUrls: ['./detailed-voucher.component.scss']
})
export class DetailedVoucherComponent implements OnInit {

  // đánh giá tuân thủ
  isLoading: boolean = false;
  areaForm: FormGroup;
  displayedColumns: string[] = ['stt', 'maCN', 'cnQL', 'pGD', 'idKH','tenKH','phanKhuc','sanPham','maHD', 'ngayGN', 'loaiKV','trangthaiNQ','trangthaiDG','status','status1','status2'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  panelOpenState = false;
  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }
  addNew(){
    
  }
  decisionDialog(){
   
  }
  update() {
   
  }
}
