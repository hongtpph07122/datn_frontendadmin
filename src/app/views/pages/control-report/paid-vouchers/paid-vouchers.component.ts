import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../category/model/category';


@Component({
  selector: 'kt-paid-vouchers',
  templateUrl: './paid-vouchers.component.html',
  styleUrls: ['./paid-vouchers.component.scss']
})
export class PaidVouchersComponent implements OnInit {
  dataSource = new MatTableDataSource<CategoryModel>([]);

  displayedColumns: string[] = ['unit', 'area', 'district', 'barrelCode', 'episodes', 'startDate', 'toDate', 'numberCabinets',
  'managers', 'storageDate','storageDate2'];

  loading: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
