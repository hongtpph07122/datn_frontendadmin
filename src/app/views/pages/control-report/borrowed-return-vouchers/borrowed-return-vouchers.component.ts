import { MatTableDataSource } from '@angular/material/table';
import { CategoryModel } from './../../category/model/category';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-borrowed-return-vouchers',
  templateUrl: './borrowed-return-vouchers.component.html',
  styleUrls: ['./borrowed-return-vouchers.component.scss']
})
export class BorrowedReturnVouchersComponent implements OnInit {
  dataSource = new MatTableDataSource<CategoryModel>([]);

  displayedColumns: string[] = ['unit', 'area', 'district', 'barrelCode', 'episodes', 'startDate', 'toDate', 'numberCabinets',
  'managers', 'storageDate'];

  loading: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
