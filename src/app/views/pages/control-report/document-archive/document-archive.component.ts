import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryModel} from '../../category/model/category';

@Component({
  selector: 'kt-document-archive',
  templateUrl: './document-archive.component.html',
  styleUrls: ['./document-archive.component.scss']
})
export class DocumentArchiveComponent implements OnInit {

  dataSource = new MatTableDataSource<CategoryModel>([]);

  displayedColumns: string[] = ['unit', 'area', 'district', 'barrelCode', 'episodes', 'startDate', 'toDate', 'numberCabinets',
  'managers', 'storageDate'];

  loading: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
