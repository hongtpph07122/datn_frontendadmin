import { CategoryModel } from './../../category/model/category';
import { DecisionListHistoryDialogComponent } from './../../disbursement/decision-update/decision-list/decision-list-history-dialog/decision-list-history-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
export interface PeriodicElement {
  stt: number;
  maCN: string;
  cnQL: string;
  pGD: string;
  idKH: string;
  tenKH: string;
  phanKhuc: string;
  sanPham: string;
  maHD: string;
  ngayGN: string;
  loaiKV: string;
  dkNQ: string;
  dkDT: string;
  dkSP: string;
  trangthaiNQ: string;
  trangthaiDG: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    stt: 1, maCN: 'VN001090', cnQL: 'CN Hoàng Mai',
    pGD: 'CN Hoàng Mai', idKH: '123456'
    , tenKH: 'Kiều Cao Long',
    phanKhuc: 'Khách hàng DN lớn', sanPham: 'Vay kinh doanh', maHD: '500.000.000',
    ngayGN: '13/11/2020', loaiKV: 'Tín dụng'
    , dkNQ: '16/11/2020', dkDT: 'Bổ sung chứng từ', dkSP: '13/11/2020'
    , trangthaiNQ: 'Chờ phê duyệt', trangthaiDG: 'Chờ đánh giá'
  },

];
@Component({
  selector: 'kt-synthetic-vouchers',
  templateUrl: './synthetic-vouchers.component.html',
  styleUrls: ['./synthetic-vouchers.component.scss']
})
export class SyntheticVouchersComponent implements OnInit {
  dataSource = new MatTableDataSource<CategoryModel>([]);

  displayedColumns: string[] = ['STT', 'Userhachtoan', 'tendonvi', 'khuvuc', 'mien', 'tongbuttoan', 'tongbuttoancham', 'tongloi',
    'tyleloi', 'soluongloihigh', 'tyleloihigh', 'soluongloimedium', 'tyleloimedium', 'soluongloilow', 'tyleloilow', 'soluongloidakhacphuc',
     'tyleloidakhacphuc', 'soluongloikhongkhacphucduoc', 'tyleloikhongkhacphucduoc', 'soluongloichuakhacphuc', 'hachtoansaithongtintrenchungtu', 'thieumatchungtu',
      'chukydaucuakhachhang', 'chukycuanganhang', 'thieusai1sothongtintrenbematchungtu', 'hachtoansaithieusovoiquydinh', 'loikhac',
       'tongsophithuthua', 'tongsophithuthieu'];

  loading: boolean;

  constructor() { }

  ngOnInit(): void {
  }
}
