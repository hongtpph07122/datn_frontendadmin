import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { DropdownTreeviewSelectModule } from './../../../core/_base/tree-view/dropdown-treeview-select/dropdown-treeview-select.module';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoreModule } from './../../../core/core.module';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortletModule } from 'src/app/views/partials/content/general/portlet/portlet.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaidVouchersComponent } from './paid-vouchers/paid-vouchers.component';
import { SyntheticVouchersComponent } from './synthetic-vouchers/synthetic-vouchers.component';
import { DetailedVoucherComponent } from './detailed-voucher/detailed-voucher.component';
import { ProductivityReportComponent } from './productivity-report/productivity-report.component';
import { BorrowedReturnVouchersComponent } from './borrowed-return-vouchers/borrowed-return-vouchers.component';
import { DocumentArchiveComponent } from './document-archive/document-archive.component';
import { StoredBcErrorComponent } from './stored-bc-error/stored-bc-error.component';
import {RouterModule, Routes} from '@angular/router';
import {ControlReportComponent} from './control-report.component';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';

const routes: Routes = [
  {
    path: '',
    component: ControlReportComponent,
    children: [
      {
        path: '',
        redirectTo: 'paid-vouchers',
        pathMatch: 'full'
      },
      {
        path: 'paid-vouchers',
        component: PaidVouchersComponent,
        data: {
          configPath: '/control-report/paid-vouchers'
        }
      },
      {
        path: 'synthetic-vouchers',
        component: SyntheticVouchersComponent,
        data: {
          configPath: '/control-report/synthetic-vouchers'
        }
      },
      {
        path: 'detailed-voucher',
        component: DetailedVoucherComponent,
        data: {
          configPath: '/control-report/detailed-voucher'
        }
      },
      {
        path: 'productivity-report',
        component: ProductivityReportComponent,
        data: {
          configPath: '/control-report/productivity-report'
        }
      },
      {
        path: 'borrowed-return-vouchers',
        component: BorrowedReturnVouchersComponent,
        data: {
          configPath: '/control-report/borrowed-return-vouchers'
        }
      },
      {
        path: 'document-archive',
        component: DocumentArchiveComponent,
        data: {
          configPath: '/control-report/document-archive'
        }
      },
      {
        path: 'stored-bc-error',
        component: StoredBcErrorComponent,
        data: {
          configPath: '/control-report/stored-bc-error'
        }
      },
    ]
  }
];


@NgModule({
  declarations: [PaidVouchersComponent,
                  SyntheticVouchersComponent,
                  DetailedVoucherComponent,
                  ProductivityReportComponent,
                  BorrowedReturnVouchersComponent,
                  DocumentArchiveComponent,
                  StoredBcErrorComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PortletModule,
    FormsModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    CoreModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDialogModule,
    DropdownTreeviewSelectModule,
    MatExpansionModule,
    MatTabsModule,
    MatCheckboxModule

  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
  ],
  entryComponents: [

  ]
})
export class ControlReportModule { }
