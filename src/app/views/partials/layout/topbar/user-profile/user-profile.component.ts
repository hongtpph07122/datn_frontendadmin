// Angular
import {Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthLoginService } from 'src/app/views/DuAn/auth-login/auth-login.service';
import { TokenStorageService } from 'src/app/views/DuAn/auth-login/_service/token-storage.service';
import { DataService } from 'src/app/views/_services/data.service';
// RxJS
// NGRX
// State
// import {AccountService} from '../../../../../core/auth/_services/account.service';
// import {LoginService} from '../../../../pages/auth/login/login.service';

@Component({
  selector: 'kt-user-profile',
  templateUrl: './user-profile.component.html',
})
export class UserProfileComponent implements OnInit {

  @Input() userDropdownStyle = 'light';
  @Input() avatar = true;
  @Input() greeting = true;
  @Input() badge: boolean;
  @Input() icon: boolean;

  // _user: Account;
  _user: any;

  /**
   * Component constructor
   *
   * @param accountService: AccountService,
   * @param loginService: LoginService
   */
  constructor(
    private tokenStorage : TokenStorageService,
    private authLoginStorage : AuthLoginService,
    private router : Router,
    private data : DataService,
  ) {
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this._user = this.tokenStorage.getUser();
    this.data.setProfileObs(this._user.id);
  }

  /**
   * Log out
   */
  logout() {
    this.tokenStorage.signOut();
    this.router.navigateByUrl("/login");
  }
}
