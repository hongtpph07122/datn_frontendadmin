import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private profileObs$: BehaviorSubject<any> = new BehaviorSubject(null);

    getProfileObs(): Observable<any> {
        return this.profileObs$.asObservable();
    }

    setProfileObs(profile: any) {
        this.profileObs$.next(profile);
    }
}
